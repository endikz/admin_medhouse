<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/modules/admin/assets';
    public $baseUrl = '@web/admin_assets';
    public $css = [
        'css/theme-5/bootstrap.css',
        'css/theme-5/materialadmin.css',
        'css/theme-5/font-awesome.min.css',
        'css/theme-5/material-design-iconic-font.min.css',
        'css/theme-5/libs/nestable/nestable.css',
		'css/theme-5/libs/bootstrap-tagsinput/bootstrap-tagsinput.css',
        'css/theme-5/libs/multi-select/multi-select.css',
        'css/theme-5/libs/select2/select2.css',        
        'css/theme-5/jquery.fancybox.min.css',        
    ];
    public $js = [
		//'js/libs/jquery/jquery-1.11.2.min.js',
		//'js/libs/jquery/jquery-migrate-1.2.1.min.js',
		'js/libs/bootstrap/bootstrap.min.js',
		'js/libs/dropzone/dropzone.min.js',
		'js/libs/spin.js/spin.min.js',
		'js/libs/autosize/jquery.autosize.min.js',
		'js/libs/nestable/jquery.nestable.js',
		'js/libs/nanoscroller/jquery.nanoscroller.min.js',
		'js/libs/inputmask/jquery.inputmask.bundle.min.js',
		'js/libs/select2/select2.min.js',
		'js/libs/multi-select/jquery.multi-select.js',
		'js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js',		
		'js/core/source/App.js',
		'js/core/source/AppNavigation.js',
		'js/core/source/AppOffcanvas.js',
		'js/core/source/AppCard.js',
		'js/core/source/AppForm.js',
		'js/core/source/AppNavSearch.js',
		'js/core/source/AppVendor.js',
		'js/core/source/AppOffcanvas.js',
		'js/core/source/all.js',
		'js/core/demo/Demo.js',
		'js/core/demo/DemoUILists.js',
		'js/core/demo/DemoFormComponents.js',
		'js/core/source/jquery.fancybox.min.js',
    ];
	public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',	
    ];
}