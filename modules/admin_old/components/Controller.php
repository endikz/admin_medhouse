<?php

namespace app\modules\admin\components;

use app\modules\admin\models\Settings;
use Yii;

class Controller extends \yii\web\Controller
{
	public $coreSettings;

	public function init() {

		$this->coreSettings = Settings::find()
			->where('id = 1')
			->multilingual()
			->one();
		
		parent::init();
	}
}