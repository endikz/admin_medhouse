<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Geka */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
			</ul>
		</div>
		<div class="card-body tab-content">
		
			<h4>Примеры возможных ссылок страниц:</h4>
			<hr />
			<p>1. <b>modules-gms/administrative-units</b> - "/" в начале и конце значения не имеют и удалятся при сохранении автоматически. Точная ссылка для применения Title и Description</p>
			<p>2. <b>modules-gms/*</b> - "*" - любое значение в конце</p>
			<p>3. <b>*</b> - ссылка по умолчанию. Применяется в случае, если не найдена более подходящая запись.</p>
			<h4>Записи запрашиваются страницами в порядке их приоритетности: 1 - 2 - 3. Т.е. запрашивается точное совпадение URL. Если такого нет, то относительные пути, если их нет - то значение по умолчанию. Если его нет - то значения будут пустыми.</h4>
		
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'tpl', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'title', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true, 'onkeyup' => 'count(this.value, 0, "");']) ?>
							<p id="type0"><b style="<?= (strlen($model->title) <= 10)?'color:#ff0000':'' ?>">Всего:</b> <?= strlen($model->title) ?> символов<?= (!empty($model->title))?', <b>Пример от 10 до 70-ти символов:</b> '.mb_substr($model->title, 0, 70, 'utf-8'):'' ?></p>

							<?= $form->field($model, 'description', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true, 'onkeyup' => 'count(this.value, 1, "");']) ?>
							<p id="type1"><b style="<?= (strlen($model->description) <= 50)?'color:#ff0000':'' ?>">Всего:</b> <?= strlen($model->description) ?> символов<?= (!empty($model->description))?', <b>Пример от 50 до 160-ти символов:</b> '.mb_substr($model->description, 0, 160, 'utf-8'):'' ?></p>							

							<?= $form->field($model, 'canonical', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true, 'onkeyup' => 'tag(this.value, "");']) ?>
							<p id="type2"><?= (!empty($model->canonical))?'<b>Пример тега:</b> &lt;link rel=&quot;canonical&quot; href=&quot;'.$model->canonical.'&quot;&gt;':'<b style="color:#ff0000">Тег выводиться не будет!</b>' ?></p>	
						
							<?= $form->field($model, 'r1')->dropdownList([2 => 'noindex', 1 => 'index']) ?>

							<?= $form->field($model, 'r2')->dropdownList([2 => 'nofollow', 1 => 'follow']) ?>
							
							<?= $form->field($model, 'sort_order', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'seo_text')->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_dynamic_abs',
									],
								]
							) ?>
						</div>
					</div>
					<div class="form-group">
					
					</div>
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'title_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true, 'onkeyup' => 'count(this.value, 0, "'.$k.'");'])->label($model->getAttributeLabel('title').' '.$v) ?>

								<?php $title_lang = 'title_'.$k; ?>
								<p id="type0<?= $k ?>"><b style="<?= (strlen($model->$title_lang) <= 10)?'color:#ff0000':'' ?>">Всего:</b> <?= strlen($model->$title_lang) ?> символов<?= (!empty($model->$title_lang))?', <b>Пример от 10 до 70-ти символов:</b> '.mb_substr($model->$title_lang, 0, 70, 'utf-8'):'' ?></p>

								<?= $form->field($model, 'description_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true, 'onkeyup' => 'count(this.value, 1, "'.$k.'");'])->label($model->getAttributeLabel('description').' '.$v) ?>

								<?php $description_lang = 'description_'.$k; ?>
								<p id="type1<?= $k ?>"><b style="<?= (strlen($model->$description_lang) <= 50)?'color:#ff0000':'' ?>">Всего:</b> <?= strlen($model->$description_lang) ?> символов<?= (!empty($model->$description_lang))?', <b>Пример от 50 до 160-ти символов:</b> '.mb_substr($model->$description_lang, 0, 160, 'utf-8'):'' ?></p>								
							</div>	
							<div class="col-sm-6">
								<?= $form->field($model, 'canonical_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true, 'onkeyup' => 'tag(this.value, "'.$k.'");'])->label($model->getAttributeLabel('canonical').' '.$v) ?>

								<?php $canonical_lang = 'canonical_'.$k; ?>
								<p id="type2<?= $k ?>"><?= (!empty($model->$canonical_lang))?'<b>Пример тега:</b> &lt;link rel=&quot;canonical&quot; href=&quot;'.$model->$canonical_lang.'&quot;&gt;':'<b style="color:#ff0000">Тег выводиться не будет!</b>' ?></p>
							</div>													
						</div>					
						<div class="form-group">
							<?= $form->field($model, 'seo_text_'.$k)->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_dynamic_abs',
									],
								]
							)->label($model->getAttributeLabel('seo_text')) ?>						
						</div>
					</div>
				</div>
			<?php } ?>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>

<script type="text/javascript">
function count(text, type, lang) {
	if (type == 0) {
		ntext = text.substr(0, 70);
		st = (text.length <= 10)?'color:#ff0000':'';
		if (text.length) {
			ntext = ', <b>Пример от 10 до 70-ти символов:</b> ' + ntext;
		} else {
			ntext = '';
		}
		console.log(lang);
		$('#type0' + lang).empty().append('<b style="' + st + '">Всего:</b> ' + text.length + ' символов' + ntext);
	} else if (type == 1) {
		ntext = text.substr(0, 160);
		st = (text.length <= 160)?'color:#ff0000':'';
		if (text.length) {
			ntext = ', <b>Пример от 50 до 160-ти символов:</b> ' + ntext;
		} else {
			ntext = '';
		}		
		$('#type1' + lang).empty().append('<b style="' + st + '">Всего:</b> ' + text.length + ' символов' + ntext);
	}
}

function tag(text, lang) {
	if (text == '') {
 		tpl = '<b style="color:#ff0000">Тег выводиться не будет!</b>';
	} else {
		tpl = '<b>Пример тега:</b> &lt;link rel=&quot;canonical&quot; href=&quot;' + text + '&quot;&gt;';
	}
	$('#type2' + lang).empty().append(tpl);
}
</script>