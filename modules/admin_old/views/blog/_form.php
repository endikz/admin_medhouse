<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;
use app\modules\admin\models\Blogc;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Blog */
/* @var $form yii\widgets\ActiveForm */

?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<li><a href="#gal">Галерея фотографий</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<div>
								<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
								<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image,'140x') ?>
							
								<?= $form->field($model, 'image')->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector_dynamic'
									]
								) ?>
							</div>

							<?= $form->field($model, 'title', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-11">
									<?= $form->field($model, 'url', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
								</div>
								<div class="col-sm-1">
									<?= Html::a('<i class="fa fa-link"></i>', 'javascript:void(0);', ['class' => 'btn btn-floating-action btn-default-light', 'onclick' => "v=$('#blog-title').val();if(v!=''){tr=translit(v);$('#blog-url').val($('#parurl').val()+tr).addClass('dirty');}"]) ?>
									<input type="hidden" id="parurl" value="<?= (!empty($model->cat->url))?trim($model->cat->url, '/').'/':'' ?>">
								</div>
							</div>

							<?= $form->field($model, 'event_date', ['template' => '{input}{label}{error}{hint}'])->widget(\yii\jui\DatePicker::classname(), [
								'language' => 'ru',
								'options' => [
									'class' => 'form-control'							
								],
								'clientOptions' => [
									'changeMonth' => true,
									'changeYear'=> true,
									'showButtonPanel' => true,				
								],
								'dateFormat' => 'yyyy-MM-dd',
							]) ?>

							<?= $form->field($model, 'category_id')->dropdownList(ArrayHelper::merge(['' => 'Нет'], Blogc::setTree())) ?>	

							<?= $form->field($model, 'link', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'sort_order')->textInput(['type' => 'number', 'min' => 0, 'max' => 1000]) ?>
						</div>					
					</div>
					<div class="form-group">
						<?= $form->field($model, 'short_text')->widget(
							TinyMce::className(),
							[
								'fileManager' => [
									'class' => TinyMceElFinder::className(),
									'connectorRoute' => 'el-finder/connector_dynamic_abs',
								],
							]
						) ?>					
					</div>
					<div class="form-group">
						<?= $form->field($model, 'text')->widget(
							TinyMce::className(),
							[						
								'fileManager' => [
									'class' => TinyMceElFinder::className(),
									'connectorRoute' => 'el-finder/connector_dynamic_abs',
								],
							]
						) ?>					
					</div>
				</div>
			</div>
			<div class="tab-pane" id="gal">
				<div class="card-body floating-label">

					<ul class="nav nav-tabs nav-justified" data-toggle="tabs">
						<li class="active"><a href="#gal1">Загрузить фото с компьютера</a></li>
						<li><a href="#gal2">Выбрать фото с сервера</a></li>
					</ul>

					<div class="row">
						<div class="card-body tab-content">
							<div class="tab-pane active" id="gal1">
								<div class="col-sm-6">
									<?= $form->field($model, 'upload[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' => '']) ?>
									<?= Html::submitButton('Загрузить фото', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
								</div>		
							</div>
							<div class="tab-pane" id="gal2">
								<div class="col-sm-6">
									<?= $form->field($model, 'upload2')->widget(
										ElFinderInput::className(),
										[
											'connectorRoute' => 'el-finder/connector_dynamic',
										]
									) ?>
									<?= Html::submitButton('Загрузить фото', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
								</div>
							</div>							
						</div>
					</div>
					<hr>
			        <div class="row">
			        	<?php foreach ($model->gal as $item) { ?>
				            <div class="col-lg-2 col-md-2 col-xs-2 thumb" id="foto<?= $item->id ?>">
				            	<?= Html::a('<i class="fa fa-trash-o"></i>', '#offcanvas-demo-left', ['class' => 'btn btn-floating-action btn-default-light delgalbtn', 'data-toggle' => 'offcanvas', 'data-backdrop' => 'false', 'onclick' => '$("#dfid").val('.$item->id.');']) ?>
				                <a class="thumbnail" href="<?= $item->image ?>" rel="lightbox[group]">
				                    <?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image,'300x300') ?>
				                </a>
				            </div>
				        <?php } ?>
				        <?= (!count($model->gal))?'<h3 class="text-light">Нет фотографий.</h3>':'' ?>
			        </div>

				</div>				
			</div>				
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'title_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('title').' '.$v) ?>
							</div>			
						</div>
						<div class="form-group">
							<?= $form->field($model, 'short_text_'.$k)->widget(
								TinyMce::className(),
								[								
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_dynamic_abs',
									],
								]
							)->label($model->getAttributeLabel('short_text')) ?>						
						</div>
						<div class="form-group">
							<?= $form->field($model, 'text_'.$k)->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_dynamic_abs',
									],
								]
							)->label($model->getAttributeLabel('text')) ?>						
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

	<?php if (!$model->isNewRecord) { ?>
	<div class="offcanvas">
		<div id="offcanvas-demo-left" class="offcanvas-pane style-primary width-10">
			<input type="hidden" id="dfid" value="0">
			<div class="offcanvas-head">
				<header>Вы уверены, что хотите удалить фотографию?</header>
				<div class="offcanvas-tools">
					<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
						<i class="md md-close"></i>
					</a>
				</div>
			</div>
			<div class="offcanvas-body">
				<?= Html::a('Удалить', 'javascript:void(0);', ['class' => 'btn btn-block ink-reaction btn-default-bright', 'data-dismiss' => 'offcanvas', 'onclick' => 'delfoto();']) ?>
			</div>
		</div>
	</div>
	<?php } ?>	

<?php ActiveForm::end(); ?>

<script>
function delfoto() {
	id = $('#dfid').val();
	$.post('/admin/blog/delfoto/', {id:id}).done(function (data) {
		data = JSON.parse(data);
		if (data.status == 1) {
			$('#foto' + id).remove();
		}
	}).fail(function() {
		console.log('Server error.');
	});
}
</script>