<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Orders;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Информация о заказе</a></li>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'status')->dropdownList([0 => 'В обработке', 1 => 'Передан в доставку', 2 => 'Доставлен', 3 => 'Отказ']);  ?>
						</div>
						<div class="col-sm-6">

						</div>						
					</div>
					<div class="form-group">
					
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-printable style-default-light">
							<div class="card-head">
								<div class="tools">
									<div class="btn-group">
										<a class="btn btn-floating-action btn-primary" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a>
									</div>
								</div>
							</div><!--end .card-head -->
							<div class="card-body style-default-bright">

								<!-- BEGIN INVOICE HEADER -->
								<div class="row">
									<div class="col-xs-8">
										<h1 class="text-light"><i class="fa fa-cart-arrow-down fa-fw fa-2x text-accent-dark"> </i><strong class="text-accent-dark">Информация о заказе</strong></h1>
									</div>
									<div class="col-xs-4 text-right">
										<h1 class="text-light text-default-light">Заказ #<?= $model->id ?></h1>
									</div>
								</div><!--end .row -->
								<!-- END INVOICE HEADER -->

								<br/>

								<!-- BEGIN INVOICE DESCRIPTION -->
								<div class="row">
									<div class="col-xs-4">
										<h4 class="text-light">Информация о клиенте:</h4>
										<address>
											<strong>Имя:</strong> <?= $model->fname ?><br>
											<strong>Телефон:</strong> <?= $model->phone ?><br>
											<strong>E-mail:</strong> <?= $model->email ?><br>
											<strong>Город:</strong> <?= $model->city ?><br>
											<strong>Адрес:</strong> <?= $model->addr ?><br>
											<strong>Гость / авторизованный:</strong> <?= (!empty($model->uid))?'Авторизованный пользователь (ID '.$model->uid.')':'Гость' ?><br>
										</address>
									</div><!--end .col -->
									<div class="col-xs-4">
										<h4 class="text-light">Доставка и оплата:</h4>
										<address>
											<strong>Доставка:</strong> <?= (!empty(Yii::$app->params['deliveryType'][$model->delivery_id]))?Yii::$app->params['deliveryType'][$model->delivery_id]:'' ?><br>
											<strong>Оплата:</strong> <?= (!empty(Yii::$app->params['payType'][$model->pay_id]))?Yii::$app->params['payType'][$model->pay_id]:'' ?><br>
										</address>
									</div><!--end .col -->
									<div class="col-xs-4">
										<div class="well">
											<div class="clearfix">
												<div class="pull-left"> НОМЕР ЗАКАЗА : </div>
												<div class="pull-right"> #<?= $model->id ?> </div>
											</div>
											<div class="clearfix">
												<div class="pull-left"> ДАТА ОФОРМЛЕНИЯ : </div>
												<div class="pull-right"> <?= date('d.m.Y H:i', $model->created_at) ?> </div>
											</div>
										</div>
									</div><!--end .col -->
								</div><!--end .row -->
								<!-- END INVOICE DESCRIPTION -->

								<br/>

								<!-- BEGIN INVOICE PRODUCTS -->
								<div class="row">
									<div class="col-md-12">
										<table class="table">
											<thead>
												<tr>
													<th style="width:60px" class="text-center">#</th>
													<th class="text-left">Товар</th>
													<th class="text-left">Фото</th>
													<th class="text-left"></th>
													<th class="text-left">Код товара</th>
													<th class="text-left">Количество</th>
													<th style="width:140px" class="text-right">Стоимость единицы (грн.)</th>
													<th style="width:140px" class="text-right">Общая стоимость (грн.)</th>
												</tr>
											</thead>
											<tbody>
												<?php 
													$i = 1; 
													$fin_pr = 0;
													$total_pr = 0;
													$total_disco_pr = 0;
												?>
												<?php foreach ($model->cart as $item) { ?>
													<?php
														$fin_pr = round(($item->price * $item->quant), 2);
													?>
													<tr>
														<td class="text-center"><?= $i ?></td>
														<td><?= (!empty($item->pr->title))?$item->pr->title:'-' ?></td>
														<td><?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$item->pr->image,'100x100') ?></td>
														<td></td>
														<td><?= (!empty($item->pr->sku))?$item->pr->sku:'-' ?></td>
														<td><?= $item->quant ?></td>
														<td class="text-right"><?= $item->price ?> грн.</td>
														<td class="text-right"><?= $fin_pr ?> грн.</td>
													</tr>
													<?php 
														$i++; 
														$total_pr += $fin_pr;															
													?>
												<?php } ?>
												<tr>
													<td colspan="6" rowspan="4">
														<h3 class="text-light opacity-50">Примечание к заказу</h3>
														<p><small><?= $model->descr ?></small></p>
													</td>
													<td class="text-right"><strong>Всего</strong></td>
													<td class="text-right"><?= $total_pr ?> грн.</td>
												</tr>
											</tbody>
										</table>
									</div><!--end .col -->
								</div><!--end .row -->
								<!-- END INVOICE PRODUCTS -->

							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
				</div><!--end .row -->

			</div>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton('Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>