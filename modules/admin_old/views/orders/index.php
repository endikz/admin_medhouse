<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>			
		</div>
		<div class="section-body contain-xlg">

			<div class="card">

				<div class="card-head style-primary">
					<header><i class="fa fa-table"></i> <?= Html::encode($this->title) ?></header>
				</div>

				<div class="card-body">

					<div class="row">
						<div class="col-lg-12">
							<h4></h4>
						</div>
						<div class="col-lg-12">
						
							<?= GridView::widget([
								'dataProvider' => $dataProvider,
								'id' => 'grid',
								'layout'=>"
									<div class='dataTables_info'>{summary}</div>
									<div class='card'>
										<div class='card-body no-padding'>
											<div class='table-responsive no-margin'>{items}</div>
										</div>
									</div>
									<div class='dataTables_paginate paging_simple_numbers'>{pager}</div>
								",		
								'tableOptions' => [
									'class' => 'table table-striped no-margin table-hover table-bordered',
								],	
								'rowOptions' => function ($model, $key, $index, $grid) {
									$u = Url::toRoute('/admin/orders/update/'.$model['id']);
								    return ['id' => $model['id'], 'onclick' => 'document.location.replace("'.$u.'");', 'style' => 'cursor:pointer;'];
								},								
								'filterModel' => $searchModel,
								'columns' => [
									//['class' => 'yii\grid\SerialColumn'],
									['class' => 'yii\grid\CheckboxColumn'],
									'id',
									'phone',
									'email',
									'fname',
									'city',
									'addr',
									'delivery_id' => [
										'attribute' => 'delivery_id',
										'filter' => ArrayHelper::map($delivery, 'id', 'title'),
										'value' => function($model, $index, $widget) use ($delivery) { 
											return (isset($delivery[$model->delivery_id]))?$delivery[$model->delivery_id]->title:'';
										},
									],
									'pay_id' => [
										'attribute' => 'pay_id',
										'filter' => ArrayHelper::map($payment, 'id', 'title'),
										'value' => function($model, $index, $widget) use ($payment) { 
											return (isset($payment[$model->pay_id]))?$payment[$model->pay_id]->title:'';
										},
									],								
									'descr',
									'updated_at:datetime',
									'status' => [
										'attribute' => 'status',
										'filter' => [0 => 'В обработке', 1 => 'Передан в доставку', 2 => 'Доставлен', 3 => 'Отказ'],
										'value' => function($model, $index, $widget) {
											if ($model->status == 0) {
												return 'В обработке';
											} else if ($model->status == 1) {
												return 'Передан в доставку';
											} else if ($model->status == 2) {
												return 'Доставлен';
											} else {
												return 'Отказ';
											}
										},
									],									
									[
										'class' => 'yii\grid\ActionColumn',
										'template' => '{update}',
										'buttons' => [										
											'update' => function ($url, $model) {
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-pencil"></i></button>', $url);
											},											
										]
									],
								],
							]); ?>
												
						</div>
					</div>	
				</div>

			</div>

		</div>
	</section>
</div>
