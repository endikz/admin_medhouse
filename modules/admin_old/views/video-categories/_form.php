<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\VideoCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'caption_ru', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

    						<?= $form->field($model, 'static', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

    						<?= $form->field($model, 'rate', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'text_ru')->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>	
						</div>
					</div>
					<div class="form-group">
					
					</div>
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'caption_ru_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('caption_ru').' '.$v) ?>
							</div>						
						</div>
						<div class="form-group">
							<?= $form->field($model, 'text_ru_'.$k)->widget(
								TinyMce::className(),
								[								
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							)->label($model->getAttributeLabel('text_ru')) ?>						
						</div>						
					</div>
				</div>
			<?php } ?>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn btn-flat btn-primary ink-reaction']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>