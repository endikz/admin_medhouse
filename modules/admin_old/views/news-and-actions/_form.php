<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\NewsAndActions */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'date_start', ['template' => '{input}{label}{error}{hint}'])->widget(\yii\jui\DatePicker::classname(), [
								'language' => 'ru',
								'options' => [
									'class' => 'form-control'							
								],
								'clientOptions' => [
									'changeMonth' => true,
									'changeYear'=> true,
									'showButtonPanel' => true,				
								],
								'dateFormat' => 'yyyy-MM-dd',
							]) ?>

							<?= $form->field($model, 'date_end', ['template' => '{input}{label}{error}{hint}'])->widget(\yii\jui\DatePicker::classname(), [
								'language' => 'ru',
								'options' => [
									'class' => 'form-control'							
								],
								'clientOptions' => [
									'changeMonth' => true,
									'changeYear'=> true,
									'showButtonPanel' => true,				
								],
								'dateFormat' => 'yyyy-MM-dd',
							]) ?>

							<?= $form->field($model, 'title', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

						    <?= $form->field($model, 'url', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<div>
								<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
								<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image,'140x') ?>
							
								<?= $form->field($model, 'image')->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector'
									]
								) ?>
							</div>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'type')->dropdownList([0 => 'Акция большая', 1 => 'Акция маленькая', 2 => 'Новость']) ?>

							<?= $form->field($model, 'col')->dropdownList([1 => 1, 2 => 2, 3 => 3]) ?>

							<?= $form->field($model, 'act_disco', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

							<?= $form->field($model, 'sort_order', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

							<?= $form->field($model, 'status')->dropdownList([0 => 'Скрыта', 1 => 'Отображена']) ?>
						</div>
					</div>
					<div class="form-group">
						<?= $form->field($model, 'footer_text')->widget(
							TinyMce::className(),
							[
								'fileManager' => [
									'class' => TinyMceElFinder::className(),
									'connectorRoute' => 'el-finder/connector_abs',
								],
							]
						) ?>
					</div>
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'title_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('title').' '.$v) ?>
							</div>						
						</div>					
						<div class="form-group">
							<?= $form->field($model, 'footer_text_'.$k)->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							)->label($model->getAttributeLabel('footer_text')) ?>						
						</div>
					</div>
				</div>
			<?php } ?>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn btn-flat btn-primary ink-reaction']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>