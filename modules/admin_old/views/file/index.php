<?php
use yii\widgets\Breadcrumbs;
use kartik\file\FileInput;
use yii\helpers\Url;
use zxbodya\yii2\elfinder\ElFinderWidget;

$this->title = 'Файловый менеджер';
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>			
		</div>	
		<div class="section-body contain-xlg">	
		
			<div class="row">
				<div class="col-lg-12">
					<?= ElFinderWidget::widget(
						['connectorRoute' => 'el-finder/connector']
					) ?>

					<br><p class="text-xl">Загрузка файлов в каталог /files/images/upload</p>

					<?= FileInput::widget([
					    'name' => 'file[]',
					    'language' => 'ru',
					    'options' => [
					    	'multiple' => true
					    ],
					    'pluginOptions' => [
					    	'previewFileType' => 'any', 
					    	'uploadUrl' => Url::toRoute(['file-upload']),
						    'uploadExtraData' => [
						        'category' => 'Default'
						    ],
						    'maxFileCount' => 20
					    ],  
					]) ?>
				
				</div>
			</div>
		
		</div>
	</section>
</div>