<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'category', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'message', ['template' => '{input}{label}{error}{hint}'])->textArea(['rows' => 3]) ?>	
						</div>
						<div class="col-sm-6">
							<?php foreach (ArrayHelper::map(Lang::find()->all(), 'local', 'flag') as $k => $v) { ?>	
								<div class="form-group">
									<?= Html::textInput('message['.$k.']', (isset($translations[$k]))?$translations[$k]:'', ['class' => 'form-control']) ?>
									<?= Html::label('Перевод '.Yii::$app->imageCache->img('.'.$v,'20x20', ['class'=>'img-circle']), ['class' => 'control-label']) ?>
								</div>
							<?php } ?>				
						</div>
					</div>
				</div>
			</div>	
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>