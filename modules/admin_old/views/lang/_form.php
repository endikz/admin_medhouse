<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Lang */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<!--<li><a href="#tab2">Tab 2</a></li>-->
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<div>
								<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>						
								<?= Yii::$app->imageCache->img('.'.$model->flag,'50x50', ['class'=>'img-circle']) ?>
							
								<?= $form->field($model, 'flag')->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector_flags',
									]
								) ?>
							</div>
							
							<?= $form->field($model, 'url', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'local', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'name', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'default')->dropdownList([0 => 'Нет', 1 => 'Да']) ?>
						</div>
					</div>
					<div class="form-group">
					
					</div>
				</div>
			</div>	
			<div class="tab-pane" id="tab2">

			</div>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>