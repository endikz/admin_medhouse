<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\MapAddresses;
use app\modules\admin\models\MapCities;
use app\modules\admin\models\MapCitiesSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * MapCitiesController implements the CRUD actions for MapCities model.
 */
class MapCitiesController extends Controller
{
	public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                        'actions' => ['delete', 'delete-group'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MapCities models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->request->isAjax) {
			$keys = (isset($_POST['keys']))?$_POST['keys']:[];
			if (count($keys)) {
				foreach ($keys as $k => $v) {
					if (($model = MapCities::findOne($v)) !== null) {
						$model->delete();
					}
				}
				return $this->redirect(['index']);
			}
		}
		
        $searchModel = new MapCitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MapCities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MapCities();
        $model->font_size = 7.548;
        $model->r = 2;

        $model_addr = new MapAddresses();
        $model_addr->type = 'd';

        if ($model->load(Yii::$app->request->post())) {
            $model_addr->load(Yii::$app->request->post());
            if ($model->validate() && $model_addr->validate()) {
                $model->save(false);
                $model_addr->external_parent = $model->id;
                $model_addr->save(false);
                return $this->redirect(['index']);
            }   
        }

        return $this->render('create', [
            'model' => $model,
            'model_addr' => $model_addr,
        ]);        
    }

    /**
     * Updates an existing MapCities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model_addr = $this->findAddrModel($id);
        $model_addr->type = 'd';
        $model_addr->external_parent = $id;

        if ($model->load(Yii::$app->request->post())) {
            $model_addr->load(Yii::$app->request->post());
            if ($model->validate() && $model_addr->validate()) {
                $model->save(false);
                $model_addr->save(false);
                Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
                return $this->redirect(['update', 'id' => $model->id]);   
            }              
        }

        return $this->render('update', [
            'model' => $model,
            'model_addr' => $model_addr,
        ]);
    }

    /**
     * Deletes an existing MapCities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MapCities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MapCities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id, $ml = true)
    {
        if ($ml) {
            $model = MapCities::find()->where('id = :id', [':id' => $id])->multilingual()->one();
        } else {
            $model = MapCities::findOne($id);
        }
        
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }      

    protected function findAddrModel($id)
    {
        $model = MapAddresses::find()->where('external_parent = :id', [':id' => $id])->multilingual()->one();
        
        if ($model !== null) {
            return $model;
        } else {
            return new MapAddresses();
        }
    }    
}
