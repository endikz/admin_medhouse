<?php

namespace app\modules\admin\controllers;
     
use Yii;       
use app\modules\admin\components\Controller;      
use zxbodya\yii2\elfinder\ConnectorAction;

class ElFinderController extends Controller         
{
    public function actions()         
    {        
        return [         
            'connector' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/',                    
                    'URL' => Yii::getAlias('@web') . '/files/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )
            ),
            'connector_abs' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/',                    
                    'URL' => $this->coreSettings->burl . '/files/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            ),            
            'connector_dynamic' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/images/dynamic/',                    
                    'URL' => Yii::getAlias('@web') . '/files/images/dynamic/',
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            ),
            'connector_dynamic_abs' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/images/dynamic/',                    
                    'URL' => $this->coreSettings->burl . '/files/images/dynamic/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            ),
            'connector_userpics' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/images/userpics/',                    
                    'URL' => Yii::getAlias('@web') . '/files/images/userpics/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            ), 
            'connector_flags' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/images/flags/',                    
                    'URL' => Yii::getAlias('@web') . '/files/images/flags/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            ), 
            'connector_slider' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/images/slider/',                    
                    'URL' => Yii::getAlias('@web') . '/files/images/slider/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            ),
            'connector_products' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/images/products/',                    
                    'URL' => Yii::getAlias('@web') . '/files/images/products/',
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            ),
            'connector_products_abs' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root' => Yii::getAlias('@webroot') . '/files/images/products/',                    
                    'URL' => $this->coreSettings->burl . '/files/images/products/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            )	
        ];                    
    }         
}