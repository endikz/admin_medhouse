<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\PrTabTechImages;
use app\modules\admin\models\PrTabFiles;
use app\modules\admin\models\PrAttr;
use app\modules\admin\models\PrEnt;
use app\modules\admin\models\PrValue;
use app\modules\admin\models\PrCategory;
use app\modules\admin\models\PrCategoryParams;
use app\modules\admin\models\PrCategoryParamImages;
use app\modules\admin\models\PrParams;
use app\modules\admin\models\Galery;
use app\modules\admin\models\Pr;
use app\modules\admin\models\PrTabs;
use app\modules\admin\models\PrSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use app\modules\admin\models\Lang;

/**
 * PrController implements the CRUD actions for Pr model.
 */
class PrController extends Controller
{
	public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'addtab'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                        'actions' => ['delete', 'delfoto'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDelfoto()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [];
            $answer['status'] = 0;
            $id = (isset($_POST['id']))?intval($_POST['id']):0;
            $gal = Galery::findOne($id);
            if ($gal) {
                if (!empty($gal->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$gal->image)) {
                    unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$gal->image);
                }
                $gal->delete();
                $answer['status'] = 1;
            }

            echo json_encode($answer);
        }
    }     

    public function actionAddtab()
    {
        if (Yii::$app->request->isAjax) {
            $id = isset($_POST['id'])?intval($_POST['id']):0;
            $id++;
            $item = new PrTabs();
            $item->id = $id;
            echo $this->renderAjax('_tab', ['item' => $item]);
        }
    }

    /**
     * Lists all Pr models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->request->isAjax) {
			$keys = (isset($_POST['keys']))?$_POST['keys']:[];
			if (count($keys) && Yii::$app->user->can('admin')) {
				foreach ($keys as $k => $v) {
					if (($model = Pr::findOne($v)) !== null) {
						$model->delete();
					}
				}
				return $this->redirect(['index']);
			}
		}
		
        $searchModel = new PrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Pr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pr();
        $model->discount = 0;
        $model->sort_order = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (isset($_POST['t'])) {
                foreach (Yii::$app->request->post('t') as $k => $v) {
                    $t = PrTabTechImages::findOne($v);
                    if ($t) {
                        $model->link('t', $t);
                    }
                }
            }

            if (isset($_POST['f'])) {
                foreach (Yii::$app->request->post('f') as $k => $v) {
                    $t = PrTabFiles::findOne($v);
                    if ($t) {
                        $model->link('f', $t);
                    }
                }
            }

            if (isset($_POST['fil'])) {
                foreach (Yii::$app->request->post('fil') as $k => $v) {
                    $pv = PrValue::find()->where(['id' => $v])->one();
                    if ($pv) {
                        $pe = PrEnt::find()->where(['pr_id' => $model->id, 'value_id' => $v])->one();
                        if ($pe === null) {
                            $pe = new PrEnt();
                            $pe->pr_id = $model->id;
                            $pe->attr_id = $pv->attr_id;
                            $pe->value_id = $pv->id;
                            $pe->save(false);
                        }
                    }
                }
            }

            $model->upload = UploadedFile::getInstances($model, 'upload');
            foreach ($model->upload as $file) {
                $fname = rand(1000, 9999).'_'.time();
                $attachment_name = $_SERVER['DOCUMENT_ROOT'].'/web/files/images/catalog/'.$fname.'.'.$file->extension;
                if ($file->saveAs($attachment_name)) {
                    $g = new Galery();
                    $g->image = '/files/images/catalog/'.$fname.'.'.$file->extension;
                    $g->item_type = 1;
                    $g->sort_order = 0;
                    $g->item_id = $model->id;
                    $g->item_name = $file->name;
                    $g->save();
                }
            }

            if (!empty($_POST['Pr[upload2]']) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$_POST['Pr[upload2]'])) {
                $parts = explode('/', $_POST['Pr[upload2]']);
                $fname = (isset($parts[count($parts) - 1]))?$parts[count($parts) - 1]:$parts[0];
                $g = new Galery();
                $g->image = $_POST['Pr[upload2]'];
                $g->item_type = 1;
                $g->sort_order = 0;
                $g->item_id = $model->id;
                $g->item_name = $fname;
                $g->save();                
            }  

            if (isset($_POST['tab'])) {
                $lc = Lang::getCurrent();
                foreach ($_POST['tab'] as $k => $v) {

                    $pr_tabs_model = new PrTabs();
                    $pr_tabs_model->pr_id = $model->id;

                    if (isset($v[$lc->url])) {
                        $pr_tabs_model->caption_ru = isset($v[$lc->url]['title'])?$v[$lc->url]['title']:'';
                        $pr_tabs_model->text_ru = isset($v[$lc->url]['text'])?$v[$lc->url]['text']:'';
                    }

                    foreach (Lang::getBehaviorsList() as $kl => $vl) { 
                        if (isset($v[$kl])) {
                            $caption_name = 'caption_ru_'.$kl;
                            $text_name = 'text_ru_'.$kl;
                            $pr_tabs_model->$caption_name = isset($v[$kl]['title'])?$v[$kl]['title']:'';
                            $pr_tabs_model->$text_name = isset($v[$kl]['text'])?$v[$kl]['text']:'';
                        }
                    }

                    $pr_tabs_model->save(false);
                }
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        PrCategory::setTree();
        $ftids = [];
        foreach (PrCategory::getFullTree($model->category_id) as $k => $v) {
            $ftids[] = $v['id'];
        }
        $pcp = PrCategoryParams::find()->where(['pr_category_id' => $ftids])->orderBy('rate DESC')->all();        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            foreach ($pcp as $item) {
                if (isset($_POST['value'.$item->id])) {
                    $pp = PrParams::find()->where(['param_id' => $item->id, 'pr_id' => $id])->one();
                    if ($pp) {
                        if ($item->type == 'char') {
                            $pp->value_char = $_POST['value'.$item->id];
                        } else if ($item->type == 'bool') {
                            $pp->value_bool = $_POST['value'.$item->id];
                        } else if ($item->type == 'image') {
                            $pci = PrCategoryParamImages::find()->where(['id' => $pp->value_image])->one();
                            if ($pci) {
                                $pci->image_small = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                                $pci->image = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                                $pci->save(false);
                            } else {
                                $pci = new PrCategoryParamImages();
                                $pci->image_small = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                                $pci->image = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                                $pci->rate = 0;
                                $pci->external_parent = $item->id;
                                $pci->save(false);
                                $pp->value_image = $pci->id;                                
                            }
                        }
                    } else {
                        $pp = new PrParams();
                        $pp->pr_id = $id;
                        $pp->param_id = $item->id;
                        $pp->value_char = '';
                        $pp->value_bool = 0;
                        $pp->value_image = null;
                        if ($item->type == 'char') {
                            $pp->value_char = $_POST['value'.$item->id];
                        } else if ($item->type == 'bool') {
                            $pp->value_bool = $_POST['value'.$item->id];
                        } else if ($item->type == 'image') {
                            $pci = new PrCategoryParamImages();
                            $pci->image_small = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                            $pci->image = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                            $pci->rate = 0;
                            $pci->external_parent = $item->id;
                            $pci->save(false);
                            $pp->value_image = $pci->id;
                        }
                    }
                    $pp->save(false);
                }
            }

            if (isset($_POST['fil'])) {
                PrEnt::deleteAll(['pr_id' => $id]);
                foreach (Yii::$app->request->post('fil') as $k => $v) {
                    $pv = PrValue::find()->where(['id' => $v])->one();
                    if ($pv) {
                        $pe = PrEnt::find()->where(['pr_id' => $id, 'value_id' => $v])->one();
                        if ($pe === null) {
                            $pe = new PrEnt();
                            $pe->pr_id = $id;
                            $pe->attr_id = $pv->attr_id;
                            $pe->value_id = $pv->id;
                            $pe->save(false);
                        }
                    }
                }
            }

            if (isset($_POST['t'])) {
                $model->unlinkAll('t', true);
                foreach (Yii::$app->request->post('t') as $k => $v) {
                    $t = PrTabTechImages::findOne($v);
                    if ($t) {
                        $model->link('t', $t);
                    }
                }
            }

            if (isset($_POST['f'])) {
                $model->unlinkAll('f', true);
                foreach (Yii::$app->request->post('f') as $k => $v) {
                    $t = PrTabFiles::findOne($v);
                    if ($t) {
                        $model->link('f', $t);
                    }
                }
            }

            if (isset($_POST['tab'])) {
                $lc = Lang::getCurrent();
                $new_tabs_ids = [];
                foreach ($_POST['tab'] as $k => $v) {

                    $pr_tabs_model = PrTabs::find()->where(['id' => $k, 'pr_id' => $id])->multilingual()->one();
                    if (null === $pr_tabs_model) {
                        $pr_tabs_model = new PrTabs(); 
                        $pr_tabs_model->pr_id = $id;
                    }

                    if (isset($v[$lc->url])) {
                        $pr_tabs_model->caption_ru = isset($v[$lc->url]['title'])?$v[$lc->url]['title']:'';
                        $pr_tabs_model->text_ru = isset($v[$lc->url]['text'])?$v[$lc->url]['text']:'';
                    }

                    foreach (Lang::getBehaviorsList() as $kl => $vl) { 
                        if (isset($v[$kl])) {
                            $caption_name = 'caption_ru_'.$kl;
                            $text_name = 'text_ru_'.$kl;
                            $pr_tabs_model->$caption_name = isset($v[$kl]['title'])?$v[$kl]['title']:'';
                            $pr_tabs_model->$text_name = isset($v[$kl]['text'])?$v[$kl]['text']:'';
                        }
                    }

                    $pr_tabs_model->save(false);
                    $new_tabs_ids[] = $pr_tabs_model->id;
                }

                PrTabs::deleteAll(['and', ['not in', 'id', $new_tabs_ids], ['pr_id' => $id]]);
            } else {
                PrTabs::deleteAll(['pr_id' => $id]);
            }

            $model->upload = UploadedFile::getInstances($model, 'upload');
            foreach ($model->upload as $file) {
                $fname = rand(1000, 9999).'_'.time();
                $attachment_name = $_SERVER['DOCUMENT_ROOT'].'/web/files/images/catalog/'.$fname.'.'.$file->extension;
                if ($file->saveAs($attachment_name)) {
                    $g = new Galery();
                    $g->image = '/files/images/catalog/'.$fname.'.'.$file->extension;
                    $g->item_type = 1;
                    $g->sort_order = 0;
                    $g->item_id = $model->id;
                    $g->item_name = $file->name;
                    $g->save();
                }
            }

            if (!empty($_POST['Pr']['upload2']) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$_POST['Pr']['upload2'])) {
                $parts = explode('/', $_POST['Pr']['upload2']);
                $fname = (isset($parts[count($parts) - 1]))?$parts[count($parts) - 1]:$parts[0];
                $g = new Galery();
                $g->image = $_POST['Pr']['upload2'];
                $g->item_type = 1;
                $g->sort_order = 0;
                $g->item_id = $model->id;
                $g->item_name = $fname;
                $g->save();                
            }

			Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $pv = [];
            foreach ($model->paramsValues as $item) {
                $pv[$item->param_id] = $item;
            }

            return $this->render('update', [
                'model' => $model,
                'pv' => $pv,
                'pcp' => $pcp,
            ]);
        }
    }

    /**
     * Deletes an existing Pr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id, $ml = true)
    {
        if ($ml) {
            $model = Pr::find()->where('id = :id', [':id' => $id])->multilingual()->one();
        } else {
            $model = Pr::findOne($id);
        }
        
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }       
}
