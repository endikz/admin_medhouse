<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Blog;
use app\modules\admin\models\Galery;
use app\modules\admin\models\BlogSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends Controller
{
	public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                        'actions' => ['delete', 'delfoto'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blog models.
     * @return mixed
     */
    public function actionDelfoto()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [];
            $answer['status'] = 0;
            $id = (isset($_POST['id']))?intval($_POST['id']):0;
            $gal = Galery::findOne($id);
            if ($gal) {
                if (!empty($gal->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$gal->image)) {
                    unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$gal->image);
                }
                $gal->delete();
                $answer['status'] = 1;
            }

            echo json_encode($answer);
        }
    }

    public function actionIndex()
    {
		if (Yii::$app->request->isAjax) {
			$keys = (isset($_POST['keys']))?$_POST['keys']:[];
			if (count($keys) && Yii::$app->user->can('admin')) {
				foreach ($keys as $k => $v) {
					if (($model = Blog::findOne($v)) !== null) {
						$model->delete();
					}
				}
				return $this->redirect(['index']);
			}
		}
		
        $searchModel = new BlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blog();
        $model->sort_order = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->upload = UploadedFile::getInstances($model, 'upload');
            foreach ($model->upload as $file) {
                $fname = rand(1000, 9999).'_'.time();
                $attachment_name = $_SERVER['DOCUMENT_ROOT'].'/web/files/images/dynamic/'.$fname.'.'.$file->extension;
                if ($file->saveAs($attachment_name)) {
                    $g = new Galery();
                    $g->image = '/files/images/dynamic/'.$fname.'.'.$file->extension;
                    $g->item_type = 0;
                    $g->sort_order = 0;
                    $g->item_id = $model->id;
                    $g->item_name = $file->name;
                    $g->save();
                }
            }

            if (!empty($_POST['Blog[upload2]']) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$_POST['Blog[upload2]'])) {
                $parts = explode('/', $_POST['Blog[upload2]']);
                $fname = (isset($parts[count($parts) - 1]))?$parts[count($parts) - 1]:$parts[0];
                $g = new Galery();
                $g->image = $_POST['Blog[upload2]'];
                $g->item_type = 0;
                $g->sort_order = 0;
                $g->item_id = $model->id;
                $g->item_name = $fname;
                $g->save();                
            }            

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->upload = UploadedFile::getInstances($model, 'upload');
            foreach ($model->upload as $file) {
                $fname = rand(1000, 9999).'_'.time();
                $attachment_name = $_SERVER['DOCUMENT_ROOT'].'/web/files/images/dynamic/'.$fname.'.'.$file->extension;
                if ($file->saveAs($attachment_name)) {
                    $g = new Galery();
                    $g->image = '/files/images/dynamic/'.$fname.'.'.$file->extension;
                    $g->item_type = 0;
                    $g->sort_order = 0;
                    $g->item_id = $model->id;
                    $g->item_name = $file->name;
                    $g->save();
                }
            }

            if (!empty($_POST['Blog']['upload2']) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$_POST['Blog']['upload2'])) {
                $parts = explode('/', $_POST['Blog']['upload2']);
                $fname = (isset($parts[count($parts) - 1]))?$parts[count($parts) - 1]:$parts[0];
                $g = new Galery();
                $g->image = $_POST['Blog']['upload2'];
                $g->item_type = 0;
                $g->sort_order = 0;
                $g->item_id = $model->id;
                $g->item_name = $fname;
                $g->save();                
            }

			Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id, $ml = true)
    {
        if ($ml) {
            $model = Blog::find()->where('id = :id', [':id' => $id])->multilingual()->one();
        } else {
            $model = Blog::findOne($id);
        }
        
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
}
