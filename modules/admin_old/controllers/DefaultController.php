<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\User;
use app\modules\admin\models\Cities;
use app\modules\admin\models\Countries;
use app\modules\admin\models\Regions;
use app\modules\admin\models\AuthItem;
use app\modules\admin\models\UserSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * DefaultController implements the CRUD actions for User model.
 */
class DefaultController extends Controller
{
	public $layout = "./sidebar";
	
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'autocomplete', 'webheads'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->request->isAjax) {
			$keys = (isset($_POST['keys']))?$_POST['keys']:[];
			if (count($keys) && Yii::$app->user->can('admin')) {
				foreach ($keys as $k => $v) {
					if (($model = User::findOne($v)) !== null) {
						$model->delete();
					}
				}
				return $this->redirect(['index']);
			}
		}
		
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionWebheads()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [];
            $message = (isset($_POST['message']))?strip_tags($_POST['message']):'';
            if (!empty($message)) {
                $email = (!empty(Yii::$app->user->identity->email))?Yii::$app->user->identity->email:'-';
                $server_data = '<pre>'.print_r($_SERVER, true).'</pre>';
                $app = (!empty(Yii::$app->controller->coreSettings->name))?Yii::$app->controller->coreSettings->name:'-';

                $text = '<p><b>E-mail</b>: '.$email.'</p>
                        <p><b>Название сайта из настроек</b>: '.$app.'</p>
                        <p><b>Серверная информация ($_SERVER)</b>: '.$server_data.'</p>
                        <p><b>Сообщение</b>: '.$message.'</p>'; 

                $mail = Yii::$app->mailer->compose();
                $mail->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->controller->coreSettings->name]);
                $mail->setTo(['titov292@gmail.com', 'ivaniy.s.a@gmail.com', 'vorsineugeniy@gmail.com']);
                $mail->setSubject('Все плохо. Письмо с админки.');
                $mail->setHtmlBody($text);    
                $mail->send();

                $answer['status'] = 1;
            } else {
                $answer['status'] = 0;
            }

            echo json_encode($answer);
        }
    }    

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
		$model->status = 1;   

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		
			if (isset($_POST['roles']) && Yii::$app->user->can('admin')) {
				foreach (Yii::$app->request->post('roles') as $k => $v) {
					
					/*
					// over many to many relation
					$role = AuthItem::findOne($v);
					if (!empty($role->name)) {
						$model->link('roles', $role);
					}
					*/
					
					// over rbac
					$userRole = Yii::$app->authManager->getRole($v);
					if (!empty($userRole)) {
						Yii::$app->authManager->assign($userRole, $model->id);
					}
				}
			} else if (!Yii::$app->user->can('admin') && Yii::$app->user->can('moderator')) {
                $userRole = Yii::$app->authManager->getRole('user');
                if (!empty($userRole)) {
                    Yii::$app->authManager->assign($userRole, $model->id);
                }
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		//$roles = $model->getRoles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

			if (isset($_POST['roles']) && Yii::$app->user->can('admin')) {
				$model->unlinkAll('roles', true);
				foreach (Yii::$app->request->post('roles') as $k => $v) {
					
					/*
					// over many to many relation
					$role = AuthItem::findOne($v);
					if (!empty($role->name)) {
						$model->link('roles', $role);
					}
					*/
					
					// over rbac
					$userRole = Yii::$app->authManager->getRole($v);
					if (!empty($userRole)) {
						Yii::$app->authManager->assign($userRole, $id);
					}
				}
			}
			
			Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
            
			return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }    

    public function actionAutocomplete()
    {
        if (Yii::$app->request->isAjax) {
            $sc = Yii::$app->request->get('term');

            $data = Cities::find()
                ->where(['like', 'LOWER(title_ru)', $sc])
                ->groupBy('city_id')
                ->limit(30)
                ->all();
            
            $result = [];
            foreach ($data as $i) {
                $item = [];

                $r = [];
                if (!empty($i->country->title_ru)) {
                   $r[] = $i->country->title_ru;
                }
                if (!empty($i->region_ru)) {
                   $r[] = $i->region_ru;
                }
                if (!empty($i->title_ru)) {
                   $r[] = $i->title_ru;
                }     
                $city = implode(', ', $r);                           

                $item['label'] = $city;
                $item['value'] = $city;
                $item['id'] = $i->city_id;
                $result[] = $item;
            }
            
            echo json_encode($result);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = User::findOne($id);

        if ($model !== null) {
            if (!Yii::$app->user->can('admin') && Yii::$app->user->can('moderator') && $model->id !== Yii::$app->user->identity->id) {
                $roles = Yii::$app->authManager->getRolesByUser($id);
                if (isset($roles['admin']) || isset($roles['moderator'])) {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
