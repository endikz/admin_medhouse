<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Menu;
use app\modules\admin\models\MenuSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
	public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->request->isAjax) {
			$keys = (isset($_POST['keys']))?$_POST['keys']:[];
			if (count($keys) && Yii::$app->user->can('admin')) {
				foreach ($keys as $k => $v) {
					if (($model = Menu::findOne($v)) !== null) {
						$model->delete();
					}
				}
				return $this->redirect(['index']);
			}
		}
		
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();
        $model->sort_order = 0;
		$model->menu_type = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$parent_id = (isset($model->parent_id))?$model->parent_id:0;
			$parent_model = Menu::findOne($parent_id);
			if ($parent_model === null) {
				$model->depth = 0;
			} else {
				$model->depth = $parent_model->depth + 1;
			}

			if ($model->save()) {
				return $this->redirect(['index']);
			}
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, true);

        if ($model->load(Yii::$app->request->post())) {
			$parent_id = (isset($model->parent_id))?$model->parent_id:0;
			$parent_model = Menu::findOne($parent_id);
			if ($parent_model === null) {
				$model->depth = 0;
			} else {
				$model->depth = $parent_model->depth + 1;
			}		

			Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
			if ($model->save()) {
				return $this->redirect(['update', 'id' => $model->id]);
			}
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id, $ml = true)
    {
		if ($ml) {
			$model = Menu::find()->where('id = :id', [':id' => $id])->multilingual()->one();
		} else {
			$model = Menu::findOne($id);
		}
		
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }	
}
