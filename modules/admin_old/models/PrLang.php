<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $pr_id
 * @property string $title
 * @property string $text1
 * @property string $text2
 * @property string $text3
 * @property string $text4
 */
class PrLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_id'], 'integer'],
            [['text1', 'text2', 'text3', 'text4'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'pr_id' => 'Pr ID',
            'title' => 'Title',
            'text1' => 'Text1',
            'text2' => 'Text2',
            'text3' => 'Text3',
            'text4' => 'Text4',
        ];
    }
}
