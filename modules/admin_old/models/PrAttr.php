<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_attr".
 *
 * @property integer $id
 * @property string $title
 * @property integer $sort_order
 *
 * @property PrEnt[] $prEnts
 * @property PrValue[] $prValues
 */
class PrAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort_order'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sort_order' => 'Sort Order',
        ];
    }

    public function getPrEnts()
    {
        return $this->hasMany(PrEnt::className(), ['attr_id' => 'id']);
    }

    public function getVal() {
        return $this->hasMany(PrValue::className(), ['attr_id' => 'id'])->orderBy('id ASC');
    }  
}
