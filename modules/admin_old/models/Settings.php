<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $descr
 * @property integer $maint
 * @property string $copy
 * @property string $logo
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'copy', 'email_admin'], 'required'],
            [['descr', 'email_tpl'], 'string'],
            [['maint'], 'integer'],
            [['burl'], 'url'],
            [['email_admin'], 'email'],
            [['name', 'copy', 'logo', 'maint_img', 'def_img', 'burl', 'bpath', 'pri1', 'pri2', 'pri3'], 'string', 'max' => 255],
            [['critical', 'critical2', 'ga'], 'string', 'max' => 50000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Название сайта',
            'descr' => 'Описание сайта',
            'maint' => 'Режим обслуживания',
            'copy' => 'Копирайт',
            'logo' => 'Логотип',
            'email_admin' => 'E-mail администратора',
            'email_tpl' => 'Шаблон письма',
            'maint_img' => 'Картинка заглушка режима обслуживания',
            'def_img' => 'Картинка по умолчанию',
            'burl' => 'Ссылка к файловому серверу, например: http://admin.mysite.com',
            'bpath' => 'Абсолютный путь к файловому серверу, например: /var/www/admin.mysite.com',
            'critical' => 'Critical CSS для главной',
            'critical2' => 'Critical CSS для не главной',
            'ga' => 'Код Google Analytics',
            'pri1' => 'Распродажа',
            'pri2' => 'Новинка',
            'pri3' => 'Скидка',
        ];
    }
	
	public function behaviors()
    {
        return [
			'ml' => [
				'class' => MultilingualBehavior::className(),
				'languages' => Lang::getBehaviorsList(),
				//'languageField' => 'language',
				//'localizedPrefix' => '',
				//'requireTranslations' => false',
				//'dynamicLangClass' => true',
				'defaultLanguage' => Lang::getCurrent()->local,
				'langForeignKey' => 'settings_id',
				'tableName' => "{{%settings_lang}}",
				'attributes' => [
					'name', 'copy', 'descr'
				]
			],		
        ];
    }	
	
	public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }	

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->bpath = '/'.trim($this->bpath, '/');
            $this->burl = trim($this->burl, '/');
                       
            return true;
        }
        return false;
    }      
}
