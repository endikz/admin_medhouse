<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "regions".
 *
 * @property integer $region_id
 * @property integer $country_id
 * @property string $title_ru
 * @property string $translit
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'required'],
            [['country_id'], 'integer'],
            [['title_ru'], 'string', 'max' => 150],
            [['translit'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'region_id' => 'Регион #',
            'country_id' => 'Страна #',
            'title_ru' => 'Название',
            'translit' => 'Транслит',
        ];
    }
}
