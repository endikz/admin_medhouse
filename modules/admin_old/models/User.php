<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $email_confirm_token
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_WAIT = 2;	
	
	public $reset_password;
    public $role;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => self::className(), 'message' => 'Данный E-mail уже занят'],
            [['email', 'image', 'fname', 'lname', 'phone', 'city'], 'string', 'max' => 255],
		    ['birth', 'safe'],
			['status', 'required'],
            [['status', 'sex', 'info'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
			
            ['password_hash', 'required'],
            [['password_hash', 'reset_password'], 'string', 'min' => 6],	

			['password_reset_token', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'image' => 'Фото пользователя',
            'auth_key' => 'Ключ сохранения сессии',
            'email_confirm_token' => 'Токен подтверждения аккаунта',
            'password_hash' => 'Пароль',
            'reset_password' => 'Новый пароль',
            'password_reset_token' => 'Токен сброса пароля',
            'email' => 'E-mail',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'sex' => 'Пол',
            'fname' => 'Имя',
            'lname' => 'Фамилия',
            'phone' => 'Телефон',
            'birth' => 'День рождения',
            'role' => 'Роль',
            'city' => 'Город',
            'info' => 'Получать уведомления',
        ];
    }
	
	public function getRoles() {
		return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])
		  ->viaTable('auth_assignment', ['user_id' => 'id']);
	}
	
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }	
	
    public static function getStatusesArray()
    {
        return [
            self::STATUS_BLOCKED => 'Заблокирован',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_WAIT => 'Ожидает подтверждения',
        ];
    }
	
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
	
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
 
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->generateAuthKey();
				$this->setPassword($this->password_hash);
            } elseif (!empty($this->reset_password)) {
				$this->setPassword($this->reset_password);
			}

            return true;
        }
        return false;
    }
	
	public function beforeDelete()
	{
		$this->unlinkAll('roles', true);
		if (!empty($this->image) && file_exists('.'.$this->image)) {
			unlink('.'.$this->image);
		}		
		
		return parent::beforeDelete();
	}	
}
