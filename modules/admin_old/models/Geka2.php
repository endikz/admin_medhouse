<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "geka2".
 *
 * @property integer $id
 * @property string $link_in
 * @property string $link_out
 * @property integer $created_at
 * @property integer $updated_at
 */
class Geka2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geka2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['link_in'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['link_in', 'link_out'], 'string', 'max' => 512]
        ];
    }
	
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),	
        ];
    }	

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'link_in' => 'Ссылка ошибки',
            'link_out' => 'Ссылка переадресации',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
