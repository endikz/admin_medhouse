<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $phone
 * @property string $email
 * @property string $fname
 * @property string $lname
 * @property string $city
 * @property string $addr
 * @property integer $delivery_id
 * @property integer $pay_id
 * @property string $descr
 * @property integer $uid
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'pay_id', 'uid', 'created_at', 'updated_at', 'status'], 'integer'],
            [['descr'], 'string'],
            [['phone', 'email', 'fname', 'lname', 'city', 'addr'], 'string', 'max' => 255]
        ];
    }
	
	public function getCart()
	{
		return $this->hasMany(Cart::className(), ['order_id' => 'id']);
	}	

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'fname' => 'Имя',
            'lname' => 'Фамилия',
            'city' => 'Город',
            'addr' => 'Адрес',
            'delivery_id' => 'Доставка',
            'pay_id' => 'Оплата',
            'descr' => 'Описание',
            'uid' => 'Uid',
            'created_at' => 'Добавлен',
            'updated_at' => 'Изменен',
            'status' => 'Статус',
        ];
    }
	
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),		
        ];
    }	
}
