<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property integer $city_id
 * @property integer $country_id
 * @property integer $important
 * @property integer $region_id
 * @property string $title_ru
 * @property string $area_ru
 * @property string $region_ru
 * @property string $translit
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'important'], 'required'],
            [['country_id', 'important', 'region_id'], 'integer'],
            [['title_ru', 'area_ru', 'region_ru'], 'string', 'max' => 150],
            [['translit'], 'string', 'max' => 255]
        ];
    }

    public function getCountry() {
        return $this->hasOne(Countries::className(), ['country_id' => 'country_id']);
    }    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'Город #',
            'country_id' => 'Страна #',
            'important' => 'Важный',
            'region_id' => 'Регион #',
            'title_ru' => 'Город',
            'area_ru' => 'Area Ru',
            'region_ru' => 'Регион',
            'translit' => 'Транслит',
        ];
    }
}
