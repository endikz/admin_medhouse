<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pd_lang".
 *
 * @property integer $id
 * @property integer $language
 * @property integer $pd_id
 * @property string $title
 * @property string $text
 *
 * @property Pd $pd
 */
class PdLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pd_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'pd_id'], 'integer'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'pd_id' => 'Pd ID',
            'title' => 'Title',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPd()
    {
        return $this->hasOne(Pd::className(), ['id' => 'pd_id']);
    }
}
