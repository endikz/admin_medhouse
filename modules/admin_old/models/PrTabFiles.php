<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "pr_tab_files".
 *
 * @property integer $id
 * @property integer $tab_id
 * @property string $file
 * @property string $file_filename
 * @property string $caption_ru
 * @property string $caption_ua
 * @property integer $rate
 */
class PrTabFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tab_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tab_id', 'caption_ru'], 'required'],
            [['tab_id', 'rate'], 'integer'],
            [['file'], 'string', 'max' => 255],
            [['file_filename', 'caption_ru', 'caption_ua'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tab_id' => 'ID вкладки',
            'file' => 'Файл',
            'file_filename' => 'File Filename',
            'caption_ru' => 'Название технологии',
            'caption_ua' => 'Caption Ua',
            'rate' => 'Сортировка',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pr_tab_files_id',
                'tableName' => "{{%pr_tab_files_lang}}",
                'attributes' => [
                    'caption_ru'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }    
}
