<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "news_and_actions".
 *
 * @property integer $id
 * @property string $date_start
 * @property string $date_end
 * @property integer $type
 * @property integer $col
 * @property integer $act_disco
 * @property string $title
 * @property string $url
 * @property string $image
 * @property integer $sort_order
 * @property string $footer_text
 * @property integer $status
 *
 * @property NewsAndActionsLang[] $newsAndActionsLangs
 */
class NewsAndActions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_and_actions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['date_start', 'date_end'], 'safe'],
            [['type', 'col', 'act_disco', 'sort_order', 'status'], 'integer'],
            [['footer_text'], 'string'],
            [['title', 'url', 'image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'type' => 'Тип',
            'col' => 'Колонка',
            'act_disco' => 'Скидка по акции',
            'title' => 'Заголовок',
            'url' => 'Ссылка',
            'image' => 'Изображение',
            'sort_order' => 'Сортировка',
            'footer_text' => 'Текст внизу блока',
            'status' => 'Статус',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'news_and_actions_id',
                'tableName' => "{{%news_and_actions_lang}}",
                'attributes' => [
                    'title', 'footer_text'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsAndActionsLangs()
    {
        return $this->hasMany(NewsAndActionsLang::className(), ['news_and_actions_id' => 'id']);
    }
}
