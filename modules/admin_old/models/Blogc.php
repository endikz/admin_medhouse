<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "blogc".
 *
 * @property integer $id
 * @property string $title
 * @property string $descr
 * @property integer $sort_order
 * @property integer $parent_id
 */
class Blogc extends \yii\db\ActiveRecord
{
    private static $tree = [];

    public static function tableName()
    {
        return 'blogc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            ['url', 'unique'],
            [['descr'], 'string'],
            [['sort_order', 'parent_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blogc::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Категория',
            'url' => 'Ссылка',
            'descr' => 'Описание категории',
            'sort_order' => 'Сортировка',
            'parent_id' => 'Родитель',
        ];
    }

    public function getPar() {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    } 

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'blogc_id',
                'tableName' => "{{%blogc_lang}}",
                'attributes' => [
                    'title', 'descr'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }  

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->url !== '/') {
                $this->url = str_replace(' ', '-', trim(trim($this->url, '/'))); 
            }
            $this->sort_order = !empty($this->sort_order)?intval($this->sort_order):0;
                       
            return true;
        }
        return false;
    }  

    public static function setTree()
    {
        if (!count(self::$tree)) {
            $model = self::find()->orderBy('sort_order ASC')->asArray()->all();
            $t = [];
            foreach ($model as $item) {
                $item_cat = [
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'parent_id' => !empty($item['parent_id'])?$item['parent_id']:0
                ];
                $t[$item['id']] = $item_cat;
            }

            self::$tree = self::buildTree($t);
        }

        return self::$tree;
    }

    private static function buildTree(array $elements, $parentId = 0, $depth = 0) {
        $branch = array();

        foreach ($elements as $k => $element) {
            if ($parentId == 0) {
                $depth = 0;
            }
            if ($element['parent_id'] == $parentId) {
                $children = self::buildTree($elements, $element['id'], $depth);
                $branch[$element['id']] = $element['title'];
                if ($children) {
                    $depth++;
                    $margin = str_repeat('__', $depth);
                    foreach ($children as $k => $v) {
                        $children[$k] = $margin.$v;
                    }
                    $branch = ArrayHelper::merge($branch, $children);
                }
            }
        }

        return $branch;
    }            
}
