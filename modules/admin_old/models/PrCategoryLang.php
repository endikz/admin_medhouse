<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_category_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $pr_category_id
 * @property string $title
 */
class PrCategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_category_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_category_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'pr_category_id' => 'Pr Category ID',
            'title' => 'Title',
        ];
    }
}
