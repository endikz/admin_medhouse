<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;


/**
 * This is the model class for table "geka".
 *
 * @property integer $id
 * @property string $tpl
 * @property string $title
 * @property string $description
 */
class Geka extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geka';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['tpl', 'sort_order'], 'required'],
            [['tpl', 'title', 'description', 'canonical', 'r1', 'r2'], 'string', 'max' => 512],
            [['seo_text'], 'string', 'max' => 20000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'tpl' => 'Ссылка',
            'title' => 'Title',
            'description' => 'Description',
            'canonical' => 'Canonical',
            'r1' => 'Robots index',
            'r2' => 'Robots follow',
            'sort_order' => 'Приоритет',
            'seo_text' => 'SEO текст',
        ];
    }
	
	public function behaviors()
    {
        return [
			'ml' => [
				'class' => MultilingualBehavior::className(),
				'languages' => Lang::getBehaviorsList(),
				//'languageField' => 'language',
				//'localizedPrefix' => '',
				//'requireTranslations' => false',
				//'dynamicLangClass' => true',
				'defaultLanguage' => Lang::getCurrent()->local,
				'langForeignKey' => 'geka_id',
				'tableName' => "{{%geka_lang}}",
				'attributes' => [
					'title', 'description', 'seo_text', 'canonical', 'r1', 'r2'
				]
			],		
        ];
    }	
	
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->tpl = trim($this->tpl, '/');
            return true;
        }
        return false;
    }
	
	public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }
}
