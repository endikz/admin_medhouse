<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_value".
 *
 * @property integer $id
 * @property integer $attr_id
 * @property string $value
 * @property integer $sort_order
 *
 * @property PrEnt[] $prEnts
 * @property PrAttr $attr
 */
class PrValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attr_id', 'sort_order'], 'integer'],
            [['value'], 'string'],
            [['attr_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrAttr::className(), 'targetAttribute' => ['attr_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attr_id' => 'Attr ID',
            'value' => 'Value',
            'sort_order' => 'Sort Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrEnts()
    {
        return $this->hasMany(PrEnt::className(), ['value_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(PrAttr::className(), ['id' => 'attr_id']);
    }
}
