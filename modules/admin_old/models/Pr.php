<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "pr".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $link
 * @property integer $category_id
 * @property double $price
 * @property integer $discount
 * @property integer $rating
 * @property integer $stock
 * @property string $text1
 * @property string $text2
 * @property string $text3
 */
class Pr extends \yii\db\ActiveRecord
{
    public $upload;
    public $upload2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr';
    }

    public function rules()
    {
        return [
            [['title', 'title2', 'link', 'category_id', 'price'], 'required'],
            [['category_id', 'discount', 'rating', 'stock', 'new', 'sale', 'brands_id', 'sort_order', 'bestseller', 'hidden', 'bdisco'], 'integer'],
            [['price', 'price2'], 'number'],
            [['text1', 'text2', 'text3', 'text4'], 'string'],
            [['title', 'title2', 'image', 'link', 'sku', 'sku2', 'upload2'], 'string', 'max' => 255],
            [['upload'], 'file', 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 10],
        ];
    }

    public function getCat() {
        return $this->hasOne(PrCategory::className(), ['id' => 'category_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->link !== '/') {
                $this->link = str_replace(' ', '-', trim(trim($this->link, '/'))); 
            }
            $this->discount = !empty($this->discount)?intval($this->discount):0;

            return true;
        }
        return false;
    } 

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'title2' => 'Заголовок в каталоге',
            'image' => 'Изображение',
            'link' => 'Ссылка',
            'category_id' => 'Категория',
            'price' => 'Цена, грн.',
            'price2' => 'Акционная цена, грн.',
            'discount' => 'Скидка, %',
            'stock' => 'В наличии',
            'text1' => 'Описание на странице товара',
            'text2' => 'Характеристики товара',
            'text3' => 'Детальное описание',
            'text4' => 'Видео',
            'sku' => 'Модель',
            'sku2' => 'Артикул',
            'new' => 'Новинка',
            'sale' => 'Распродажа',
            'bdisco' => 'Скидка',
            'sort_order' => 'Сортировка',
            'hidden' => 'Скрывать на сайте',
            'bestseller' => 'Лидер продаж',
            'upload' => 'Загрузить фотографии в галерею с компьютера (jpg, jpeg, png)',
            'upload2' => 'Добавить фотографии в галерею из файлов сайта (jpg, jpeg, png)',
        ];
    }    

    public function getGal()
    {
        return $this->hasMany(Galery::className(), ['item_id' => 'id'])
            ->where('item_type = 1');
    } 

    public function getParams()
    {
        return $this->hasMany(PrCategoryParams::className(), ['pr_category_id' => 'category_id'])
            ->orderBy('rate DESC');
    }     

    public function getTabs()
    {
        return $this->hasMany(PrTabs::className(), ['pr_id' => 'id'])
            ->orderBy('rate DESC')->multilingual();
    }       

    public function getParamsValues()
    {
        return $this->hasMany(PrParams::className(), ['pr_id' => 'id']);
    } 

    public function beforeDelete()
    {
        if (!empty($this->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image)) {
            unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image);
        }        
        foreach ($this->gal as $item) {
            if (!empty($item->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image)) {
                unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image);
            }
            $item->delete();
        }
        
        return parent::beforeDelete();
    }    

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pr_id',
                'tableName' => "{{%pr_lang}}",
                'attributes' => [
                    'title', 'title2', 'text1', 'text2', 'text3', 'text4'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }

    /*
    public function getF()
    {
        return $this->hasMany(PrTabFiles::className(), ['tab_id' => 'id'])
            ->orderBy('rate DESC');
    }
    */

    public function getF()
    {
        return $this->hasMany(PrTabFiles::className(), ['id' => 'file_id'])
            ->viaTable('pr_tab_files_tab', ['tab_id' => 'id'])->orderBy('caption_ru ASC');
    }       

    public function getT()
    {
        return $this->hasMany(PrTabTechImages::className(), ['id' => 'tech_id'])
            ->viaTable('pr_tab_tech_images_tab', ['tab_id' => 'id'])->orderBy('caption_ru ASC');
    }           
}
