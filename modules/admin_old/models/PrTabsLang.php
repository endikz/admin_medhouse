<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_tabs_lang".
 *
 * @property integer $id
 * @property integer $pr_tabs_id
 * @property string $language
 * @property string $caption_ru
 * @property string $text_ru
 *
 * @property PrTabs $prTabs
 */
class PrTabsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tabs_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_tabs_id'], 'integer'],
            [['text_ru'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['caption_ru'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pr_tabs_id' => 'Pr Tabs ID',
            'language' => 'Language',
            'caption_ru' => 'Caption Ru',
            'text_ru' => 'Text Ru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrTabs()
    {
        return $this->hasOne(PrTabs::className(), ['id' => 'pr_tabs_id']);
    }
}
