<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "videos_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $videos_id
 * @property string $caption_ru
 */
class VideosLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'videos_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['videos_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['caption_ru', 'video_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'videos_id' => 'Videos ID',
            'caption_ru' => 'Caption Ru',
        ];
    }
}
