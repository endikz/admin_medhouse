<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\PrCategoryParams;

/**
 * PrCategoryParamsSearch represents the model behind the search form about `app\modules\admin\models\PrCategoryParams`.
 */
class PrCategoryParamsSearch extends PrCategoryParams
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pr_category_id', 'rate'], 'integer'],
            [['caption_ru', 'caption_ua', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PrCategoryParams::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],			
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pr_category_id' => $this->pr_category_id,
            'rate' => $this->rate,
        ]);

        $query->andFilterWhere(['like', 'caption_ru', $this->caption_ru])
            ->andFilterWhere(['like', 'caption_ua', $this->caption_ua])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
