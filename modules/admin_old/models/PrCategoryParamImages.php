<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_category_param_images".
 *
 * @property integer $id
 * @property integer $external_parent
 * @property string $image_small
 * @property string $image_small_filename
 * @property string $image
 * @property string $image_filename
 * @property string $caption_ru
 * @property string $caption_ua
 * @property integer $rate
 */
class PrCategoryParamImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_category_param_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['external_parent'], 'required'],
            [['external_parent', 'rate'], 'integer'],
            [['image_small_filename', 'image_filename', 'caption_ru', 'caption_ua', 'image_small', 'image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'external_parent' => 'External Parent',
            'image_small' => 'Image Small',
            'image_small_filename' => 'Image Small Filename',
            'image' => 'Image',
            'image_filename' => 'Image Filename',
            'caption_ru' => 'Caption Ru',
            'caption_ua' => 'Caption Ua',
            'rate' => 'Rate',
        ];
    }
}
