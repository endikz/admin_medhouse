<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "video_categories_lang".
 *
 * @property integer $id
 * @property integer $video_categories_id
 * @property string $language
 * @property string $caption_ru
 * @property string $text_ru
 */
class VideoCategoriesLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_categories_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_categories_id'], 'integer'],
            [['text_ru'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['caption_ru'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video_categories_id' => 'Video Categories ID',
            'language' => 'Language',
            'caption_ru' => 'Caption Ru',
            'text_ru' => 'Text Ru',
        ];
    }
}
