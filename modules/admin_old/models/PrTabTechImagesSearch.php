<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\PrTabTechImages;

/**
 * PrTabTechImagesSearch represents the model behind the search form about `app\modules\admin\models\PrTabTechImages`.
 */
class PrTabTechImagesSearch extends PrTabTechImages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rate'], 'integer'],
            [['image_small', 'image_small_filename', 'image', 'image_filename', 'caption_ru', 'caption_ua'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PrTabTechImages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],			
			'sort'=> ['defaultOrder' => ['rate' => SORT_DESC]]			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'rate' => $this->rate,
        ]);

        $query->andFilterWhere(['like', 'image_small', $this->image_small])
            ->andFilterWhere(['like', 'image_small_filename', $this->image_small_filename])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_filename', $this->image_filename])
            ->andFilterWhere(['like', 'caption_ru', $this->caption_ru])
            ->andFilterWhere(['like', 'caption_ua', $this->caption_ua]);

        return $dataProvider;
    }
}
