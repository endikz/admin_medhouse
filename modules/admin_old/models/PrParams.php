<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_params".
 *
 * @property integer $id
 * @property integer $pr_id
 * @property integer $param_id
 * @property string $value_char
 * @property integer $value_bool
 * @property integer $value_image
 */
class PrParams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_id', 'param_id'], 'required'],
            [['pr_id', 'param_id', 'value_bool', 'value_image'], 'integer'],
            [['value_char'], 'string', 'max' => 255],
            [['pr_id', 'param_id'], 'unique', 'targetAttribute' => ['pr_id', 'param_id'], 'message' => 'The combination of Pr ID and Param ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pr_id' => 'Pr ID',
            'param_id' => 'Param ID',
            'value_char' => 'Value Char',
            'value_bool' => 'Value Bool',
            'value_image' => 'Value Image',
        ];
    }

    public function getI()
    {
        return $this->hasOne(PrCategoryParamImages::className(), ['id' => 'value_image']);
    }     

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->value_bool = empty($this->value_bool)?0:intval($this->value_bool);

            return true;
        }
        return false;
    }     
}
