<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_tab_tech_images_tab".
 *
 * @property integer $tech_id
 * @property integer $tab_id
 *
 * @property PrTabs $tab
 * @property PrTabTechImages $tech
 */
class PrTabTechImagesTab extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tab_tech_images_tab';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tech_id', 'tab_id'], 'required'],
            [['tech_id', 'tab_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tech_id' => 'Tech ID',
            'tab_id' => 'Tab ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTab()
    {
        return $this->hasOne(PrTabs::className(), ['id' => 'tab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTech()
    {
        return $this->hasOne(PrTabTechImages::className(), ['id' => 'tech_id']);
    }
}
