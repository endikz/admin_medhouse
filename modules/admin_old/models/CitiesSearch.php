<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Cities;

/**
 * CitiesSearch represents the model behind the search form about `app\modules\admin\models\Cities`.
 */
class CitiesSearch extends Cities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'country_id', 'important', 'region_id'], 'integer'],
            [['title_ru', 'area_ru', 'region_ru', 'translit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cities::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'city_id' => $this->city_id,
            'country_id' => $this->country_id,
            'important' => $this->important,
            'region_id' => $this->region_id,
        ]);

        $query->andFilterWhere(['like', 'title_ru', $this->title_ru])
            ->andFilterWhere(['like', 'area_ru', $this->area_ru])
            ->andFilterWhere(['like', 'region_ru', $this->region_ru])
            ->andFilterWhere(['like', 'translit', $this->translit]);

        return $dataProvider;
    }
}
