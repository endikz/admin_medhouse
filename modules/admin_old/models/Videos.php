<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "videos".
 *
 * @property integer $id
 * @property string $video_id
 * @property integer $good
 * @property integer $brand
 * @property integer $category
 * @property string $caption_ru
 * @property string $caption_ua
 * @property integer $rate
 * @property integer $good_rate
 * @property string $image
 */
class Videos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'videos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id'], 'required'],
            [['good', 'brand', 'category', 'rate', 'good_rate'], 'integer'],
            [['video_id', 'caption_ru', 'caption_ua'], 'string', 'max' => 255],
            [['good'], 'exist', 'skipOnError' => true, 'targetClass' => Pr::className(), 'targetAttribute' => ['good' => 'id']],
            [['image'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video_id' => 'ID Youtube видео',
            'good' => 'ID товара',
            'brand' => 'Бренд',
            'category' => 'Категория',
            'caption_ru' => 'Заголовок',
            'caption_ua' => 'Caption Ua',
            'rate' => 'Сортировка',
            'good_rate' => 'Good Rate',
            'image' => 'Image',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'videos_id',
                'tableName' => "{{%videos_lang}}",
                'attributes' => [
                    'caption_ru', 'video_id'
                ]
            ],
        ];
    }
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    } 

    public function getBrand0()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand']);
    }

    public function getGood0()
    {
        return $this->hasOne(Pr::className(), ['id' => 'good']);
    }

    public function getCategory0()
    {
        return $this->hasOne(VideoCategories::className(), ['id' => 'category']);
    }  
}
