<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "video_categories".
 *
 * @property integer $id
 * @property string $caption_ru
 * @property string $static
 * @property integer $rate
 * @property string $title_ru
 * @property string $keywords_ru
 * @property string $description_ru
 * @property string $caption_ua
 * @property string $title_ua
 * @property string $description_ua
 * @property string $keywords_ua
 * @property string $text_ru
 * @property string $text_ua
 */
class VideoCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['caption_ru', 'static'], 'required'],
            [['rate'], 'integer'],
            [['keywords_ru', 'description_ru', 'description_ua', 'keywords_ua', 'text_ru', 'text_ua'], 'string'],
            [['caption_ru', 'static', 'title_ru', 'caption_ua', 'title_ua'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'caption_ru' => 'Заголовок',
            'static' => 'Ссылка',
            'rate' => 'Сортировка',
            'title_ru' => 'Title Ru',
            'keywords_ru' => 'Keywords Ru',
            'description_ru' => 'Description Ru',
            'caption_ua' => 'Caption Ua',
            'title_ua' => 'Title Ua',
            'description_ua' => 'Description Ua',
            'keywords_ua' => 'Keywords Ua',
            'text_ru' => 'Текст',
            'text_ua' => 'Text Ua',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'video_categories_id',
                'tableName' => "{{%video_categories_lang}}",
                'attributes' => [
                    'caption_ru', 'text_ru'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }    
}
