<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "pr_tabs".
 *
 * @property integer $id
 * @property integer $pr_id
 * @property string $caption_ru
 * @property string $caption_ua
 * @property string $text_ru
 * @property string $text_ua
 * @property integer $rate
 */
class PrTabs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tabs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_id', 'caption_ru'], 'required'],
            [['pr_id', 'rate'], 'integer'],
            [['text_ru', 'text_ua'], 'string'],
            [['caption_ru', 'caption_ua'], 'string', 'max' => 255],
            [['pr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pr::className(), 'targetAttribute' => ['pr_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pr_id' => 'ID товара',
            'caption_ru' => 'Заголовок вкладки',
            'caption_ua' => 'Caption Ua',
            'text_ru' => 'Текст вкладки',
            'text_ua' => 'Text Ua',
            'rate' => 'Сортировка',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pr_tabs_id',
                'tableName' => "{{%pr_tabs_lang}}",
                'attributes' => [
                    'caption_ru', 'text_ru'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }    

    public function getF()
    {
        return $this->hasMany(PrTabFiles::className(), ['tab_id' => 'id'])
            ->orderBy('rate DESC');
    }

    public function getT()
    {
        return $this->hasMany(PrTabTechImages::className(), ['id' => 'tech_id'])
            ->viaTable('pr_tab_tech_images_tab', ['tab_id' => 'id'])->orderBy('caption_ru ASC');
    }       
}
