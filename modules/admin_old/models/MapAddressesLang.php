<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "map_addresses_lang".
 *
 * @property integer $id
 * @property integer $map_addresses_id
 * @property string $language
 * @property string $address_ru
 *
 * @property MapAddresses $mapAddresses
 */
class MapAddressesLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_addresses_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['map_addresses_id'], 'required'],
            [['map_addresses_id'], 'integer'],
            [['address_ru'], 'string'],
            [['language'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'map_addresses_id' => 'Map Addresses ID',
            'language' => 'Language',
            'address_ru' => 'Address Ru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapAddresses()
    {
        return $this->hasOne(MapAddresses::className(), ['id' => 'map_addresses_id']);
    }
}
