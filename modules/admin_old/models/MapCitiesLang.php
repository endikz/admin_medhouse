<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "map_cities_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $map_cities_id
 * @property string $caption_ru
 *
 * @property MapCities $mapCities
 */
class MapCitiesLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_cities_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['map_cities_id'], 'integer'],
            [['language', 'caption_ru'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'map_cities_id' => 'Map Cities ID',
            'caption_ru' => 'Caption Ru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapCities()
    {
        return $this->hasOne(MapCities::className(), ['id' => 'map_cities_id']);
    }
}
