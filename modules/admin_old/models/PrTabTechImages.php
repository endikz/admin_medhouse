<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "pr_tab_tech_images".
 *
 * @property integer $id
 * @property integer $tab_id
 * @property string $image_small
 * @property string $image_small_filename
 * @property string $image
 * @property string $image_filename
 * @property string $caption_ru
 * @property string $caption_ua
 * @property integer $rate
 */
class PrTabTechImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tab_tech_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['caption_ru'], 'required'],
            [['rate'], 'integer'],
            [['image_small_filename', 'image_filename', 'caption_ru', 'caption_ua', 'image_small', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_small' => 'Превью изображения технологии',
            'image_small_filename' => 'Image Small Filename',
            'image' => 'Изображение технологии',
            'image_filename' => 'Image Filename',
            'caption_ru' => 'Название технологии',
            'caption_ua' => 'Caption Ua',
            'rate' => 'Сортировка',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pr_tab_tech_images_id',
                'tableName' => "{{%pr_tab_tech_images_lang}}",
                'attributes' => [
                    'caption_ru', 'image_small', 'image'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }     
}
