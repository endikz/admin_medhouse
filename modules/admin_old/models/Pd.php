<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "pd".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $type
 * @property integer $status
 *
 * @property PdLang[] $pdLangs
 */
class Pd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type', 'status'], 'required'],
            [['text'], 'string'],
            [['type', 'status'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Описание',
            'type' => 'Группа',
            'status' => 'Статус',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pd_id',
                'tableName' => "{{%pd_lang}}",
                'attributes' => [
                    'title', 'text'
                ]
            ],
        ];
    }
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }

    public function getPdLangs()
    {
        return $this->hasMany(PdLang::className(), ['pd_id' => 'id']);
    }
}
