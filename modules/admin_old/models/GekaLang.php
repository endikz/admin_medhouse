<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "geka_lang".
 *
 * @property integer $id
 * @property integer $geka_id
 * @property string $language
 * @property string $title
 * @property string $description
 */
class GekaLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geka_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['geka_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['title', 'description', 'canonical'], 'string', 'max' => 512],
            [['seo_text'], 'string', 'max' => 20000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'geka_id' => 'Geka ID',
            'language' => 'Language',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }
}
