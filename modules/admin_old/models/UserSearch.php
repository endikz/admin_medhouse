<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\User;

/**
 * UserSearch represents the model behind the search form about `app\modules\admin\models\User`.
 */
class UserSearch extends User
{
    public $role;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'sex'], 'integer'],
            [['auth_key', 'email_confirm_token', 'password_hash', 'password_reset_token', 'email', 'fname', 'lname', 'phone', 'role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],
			'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'sex' => $this->sex,
        ]);

        if (!Yii::$app->user->can('admin') && Yii::$app->user->can('moderator')) {
            $ur = AuthAssignment::find()->select('user_id')->where(['item_name' => 'user'])->groupBy('user_id')->indexBy('user_id')->asArray()->all();
            $ids = array_keys($ur);
            $ids[] = Yii::$app->user->identity->id;
            $query->andFilterWhere(['id' => $ids]);
        } else if (!empty($this->role)) {
            $ur = AuthAssignment::find()->select('user_id')->where(['item_name' => $this->role])->groupBy('user_id')->indexBy('user_id')->asArray()->all();
            $ids = array_keys($ur);
            $ids[] = 0;
            $query->andFilterWhere(['id' => $ids]);
        }

        $query->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'email_confirm_token', $this->email_confirm_token])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
