<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "news_and_actions_lang".
 *
 * @property integer $id
 * @property integer $news_and_actions_id
 * @property string $language
 * @property string $title
 * @property string $footer_text
 *
 * @property NewsAndActions $newsAndActions
 */
class NewsAndActionsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_and_actions_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_and_actions_id'], 'integer'],
            [['footer_text'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_and_actions_id' => 'News And Actions ID',
            'language' => 'Language',
            'title' => 'Title',
            'footer_text' => 'Footer Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsAndActions()
    {
        return $this->hasOne(NewsAndActions::className(), ['id' => 'news_and_actions_id']);
    }
}
