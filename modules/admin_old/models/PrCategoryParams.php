<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "pr_category_params".
 *
 * @property integer $id
 * @property integer $pr_category_id
 * @property string $caption_ru
 * @property string $caption_ua
 * @property integer $rate
 * @property string $type
 *
 * @property PrCategoryParamImages[] $prCategoryParamImages
 * @property PrCategory $prCategory
 * @property PrCategoryParamsLang[] $prCategoryParamsLangs
 * @property PrParams[] $prParams
 */
class PrCategoryParams extends \yii\db\ActiveRecord
{
    public $image;
    public $image_small;
    public $image_rate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_category_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_category_id', 'caption_ru'], 'required'],
            [['pr_category_id', 'rate', 'image_rate'], 'integer'],
            [['type'], 'string'],
            [['caption_ru', 'caption_ua', 'image', 'image_small'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pr_category_id' => 'Категория',
            'caption_ru' => 'Заголовок',
            'caption_ua' => 'Caption Ua',
            'rate' => 'Сортировка',
            'type' => 'Тип',
            'image_rate' => 'Сортировка картинки',
            'image' => 'Картинка',
            'image_small' => 'Превью картинки',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pr_category_params_id',
                'tableName' => "{{%pr_category_params_lang}}",
                'attributes' => [
                    'caption_ru'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(PrCategoryParamImages::className(), ['external_parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(PrCategory::className(), ['id' => 'pr_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrCategoryParamsLangs()
    {
        return $this->hasMany(PrCategoryParamsLang::className(), ['pr_category_params_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrParams()
    {
        return $this->hasMany(PrParams::className(), ['param_id' => 'id']);
    }
}
