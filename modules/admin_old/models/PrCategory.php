<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "pr_category".
 *
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 */
class PrCategory extends \yii\db\ActiveRecord
{
    private static $tree = [];
    public static $tree_list = [];
    public static $child_full_tree = [];
    public static $child_tree = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'link'], 'required'],
            [['parent_id', 'sort_order', 'hidden'], 'integer'],
            [['title', 'image', 'image2', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'image' => 'Изображение',
            'image2' => 'Изображение 2',
            'image_filter' => 'Изображение фильтра',
            'image_filter_active' => 'Изображение активного фильтра',
            'sort_order' => 'Сортировка',
            'link' => 'Ссылка',
            'parent_id' => 'Родитель',
            'hidden' => 'Скрыта',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pr_category_id',
                'tableName' => "{{%pr_category_lang}}",
                'attributes' => [
                    'title'
                ]
            ],      
        ];
    }   

    public function getPar() {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }       
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }     

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->link !== '/') {
                $this->link = str_replace(' ', '-', trim(trim($this->link, '/'))); 
            }
            $this->sort_order = !empty($this->sort_order)?intval($this->sort_order):0;
                       
            return true;
        }
        return false;
    }  

    public function beforeDelete()
    {
        if (!empty($this->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image)) {
            unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image);
        }
        if (!empty($this->image2) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image2)) {
            unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image2);
        }
        
        return parent::beforeDelete();
    }    

    public static function setTree()
    {
        if (!count(self::$tree)) {
            $model = self::find()->orderBy('sort_order ASC')->asArray()->all();
            $t = [];
            foreach ($model as $item) {
                $item_cat = [
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'parent_id' => !empty($item['parent_id'])?$item['parent_id']:0
                ];
                $t[$item['id']] = $item_cat;
            }

            self::$tree_list = $t;
            self::$tree = self::buildTree($t);
        }

        return self::$tree;
    }

    public static function getFullTree($id, $url = false, &$arr = []) {
        if (isset(self::$tree_list[$id])) {
            $e = self::$tree_list[$id];
            self::$child_tree[] = $e;
            $arr[] = ($url)?$e['link']:$e;
            self::getFullTree($e['parent_id'], $url, $arr);
        }

        return ($url)?implode('/', array_reverse($arr)):array_reverse($arr);
    }    

    private static function buildTree(array $elements, $parentId = 0, $depth = 0) {
        $branch = array();

        foreach ($elements as $k => $element) {
            if ($parentId == 0) {
                $depth = 0;
            }
            if ($element['parent_id'] == $parentId) {
                $children = self::buildTree($elements, $element['id'], $depth);
                $branch[$element['id']] = $element['title'];
                if ($children) {
                    $depth++;
                    $margin = str_repeat('__', $depth);
                    foreach ($children as $k => $v) {
                        $children[$k] = $margin.$v;
                    }
                    $branch = ArrayHelper::merge($branch, $children);
                }
            }
        }

        return $branch;
    }         
}
