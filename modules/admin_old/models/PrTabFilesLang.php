<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_tab_files_lang".
 *
 * @property integer $id
 * @property integer $pr_tab_files_id
 * @property string $language
 * @property string $caption_ru
 *
 * @property PrTabFiles $prTabFiles
 */
class PrTabFilesLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tab_files_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_tab_files_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['caption_ru'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pr_tab_files_id' => 'Pr Tab Files ID',
            'language' => 'Language',
            'caption_ru' => 'Caption Ru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrTabFiles()
    {
        return $this->hasOne(PrTabFiles::className(), ['id' => 'pr_tab_files_id']);
    }
}
