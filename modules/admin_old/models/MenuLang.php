<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "menu_lang".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $language
 * @property string $name
 */
class MenuLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'language', 'name'], 'required'],
            [['menu_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'language' => 'Language',
            'name' => 'Name',
        ];
    }
}
