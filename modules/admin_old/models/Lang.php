<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lang".
 *
 * @property integer $id
 * @property string $flag
 * @property string $url
 * @property string $local
 * @property string $name
 * @property integer $default
 * @property integer $created_at
 * @property integer $updated_at
 */
class Lang extends \yii\db\ActiveRecord
{
	static $current = null;
	static $default = null;
	static $behaviors_list = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'local', 'name'], 'required'],
            [['default', 'created_at', 'updated_at'], 'integer'],
            [['flag', 'url', 'local', 'name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'flag' => 'Флаг',
            'url' => 'Ссылка',
            'local' => 'Локализация',
            'name' => 'Название',
            'default' => 'Главный',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
        ];
    }
	
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
	
	static function getCurrent()
	{
		if (self::$current === null) {
			self::$current = self::getDefaultLang();
		}
		return self::$current;
	}
	
	static function setCurrent($url = null)
	{
		$language = self::getLangByUrl($url);
		self::$current = ($language === null) ? self::getDefaultLang() : $language;
		Yii::$app->language = self::$current->local;
	}
	
	static function getDefaultLang()
	{
		if (self::$default === null) {
			self::$default = Lang::find()->where(self::tableName().'.default = :default', [':default' => 1])->one();
		}
		return self::$default;
	}
	
	static function getLangByUrl($url = null)
	{
		if ($url === null) {
			return null;
		} else {
			$language = Lang::find()->where('url = :url', [':url' => $url])->one();
			if ($language === null) {
				return null;
			} else {
				return $language;
			}
		}
	}
	
	static function getBehaviorsList()
	{
		if (self::$behaviors_list === null) {
			$list = ArrayHelper::map(self::find()->where('lang.default = 0')->all(), 'local', 'flag');
			$result = array();
			foreach ($list as $k => $v) {
				$parts = explode('-', $k);
				$result[$parts[0]] = Yii::$app->imageCache->img('.'.$v,'20x20', ['class'=>'img-circle']);
			}
			self::$behaviors_list = (count($result))?$result:$list;
		}
		return self::$behaviors_list;
	}

    public static function getHrefLang()
    {
        $hl = '';
        $parts = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
        $langs = array_keys(Lang::getBehaviorsList());
        if (in_array($parts[0], $langs)) {
            unset($parts[0]);
        }
        $nl = implode('/', $parts);
        $nl = (!empty($nl))?$nl.'/':$nl;

        $hl .= '<link rel="alternate" href="http://'.$_SERVER['SERVER_NAME'].'/'.$nl.'" hreflang="'.Lang::getCurrent()->local.'" />';
        foreach (Lang::find()->where('local <> :local', [':local' => Yii::$app->language])->all() as $item) {
            $hl .= '<link rel="alternate" href="http://'.$_SERVER['SERVER_NAME'].'/'.$item->url.'/'.$nl.'" hreflang="'.$item->local.'" />';
        }
        return $hl;
    }	
}
