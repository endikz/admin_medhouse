<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class Promo extends \yii\db\ActiveRecord
{
    public $dr;

    public static function tableName()
    {
        return 'promo';
    }

    public function rules()
    {
        return [
            [['code', 'prs', 'status'], 'required'],
            [['dur_type', 'prs', 'cart_type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['dur_from', 'dur_to', 'dr'], 'string', 'max' => 255],
            [['code'], 'string', 'min' => 10, 'max' => 15],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Промо код',
            'dur_type' => 'Срок действия',
            'dur_from' => 'Действует с',
            'dur_to' => 'Действует по',
            'dr' => 'Время действия',
            'prs' => 'Скидка, %',
            'cart_type' => 'Действие скидки',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Изменен',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),     
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!empty($this->dr)) {
                $parts = explode(' - ', $this->dr);
                $date_from = trim($parts[0]);
                $date_to = isset($parts[1])?$parts[1]:'';

                $this->dur_from = $date_from; 
                $this->dur_to = $date_to; 
            }
                       
            return true;
        }
        return false;
    }    
}
