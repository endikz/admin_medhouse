<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\PrTabFiles;

/**
 * PrTabFilesSearch represents the model behind the search form about `app\modules\admin\models\PrTabFiles`.
 */
class PrTabFilesSearch extends PrTabFiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tab_id', 'rate'], 'integer'],
            [['file', 'file_filename', 'caption_ru', 'caption_ua'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PrTabFiles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],			
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tab_id' => $this->tab_id,
            'rate' => $this->rate,
        ]);

        $query->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'file_filename', $this->file_filename])
            ->andFilterWhere(['like', 'caption_ru', $this->caption_ru])
            ->andFilterWhere(['like', 'caption_ua', $this->caption_ua]);

        return $dataProvider;
    }
}
