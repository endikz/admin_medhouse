<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $phone
 * @property string $email
 * @property string $fname
 * @property string $lname
 * @property string $city
 * @property string $addr
 * @property integer $delivery_id
 * @property integer $pay_id
 * @property string $descr
 * @property integer $uid
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $prc_id
 */
class Orders extends \yii\db\ActiveRecord
{
    public $val_validate;
    public $prc_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'email', 'fname', 'city', 'addr', 'delivery_id', 'pay_id'], 'required', 'on' => 'create'],
            [['email'], 'email', 'on' => 'create'],
            ['val_validate', 'required', 'requiredValue' => 1, 'message' => Yii::t('app', 'Необходимо добавить хотябы один товар в корзину.'), 'on' => 'create'],

            [['delivery_id', 'pay_id', 'uid', 'created_at', 'updated_at', 'status', 'removed', 'oid', 'prc_id'], 'integer'],
            [['descr','descr1'], 'string'],
            [['phone', 'email', 'fname', 'lname', 'city', 'addr', 'inn'], 'string', 'max' => 255]
        ];
    }
	
	public function getCart()
	{
		return $this->hasMany(Cart::className(), ['order_id' => 'id']);
	}	
    
    public function getP()
    {
        return $this->hasOne(PaymentHistory::className(), ['order_id' => 'id']);
    }    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'fname' => 'Имя',
            'lname' => 'Фамилия',
            'city' => 'Город',
            'addr' => 'Адрес',
            'delivery_id' => 'Доставка',
            'pay_id' => 'Оплата',
            'descr' => 'Описание',
            'descr1' => 'Комментарий Администратора',
            'uid' => 'Uid',
            'inn' => 'ИНН',
            'created_at' => 'Добавлен',
            'updated_at' => 'Изменен',
            'oid' => 'Оператор',
            'status' => 'Статус',
            'prc_id' => 'Товар',
        ];
    }
	
	public function behaviors()
    {
        return [
            TimestampBehavior::className(),		
        ];
    }	
}
