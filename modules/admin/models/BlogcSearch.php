<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Blogc;
use yii\helpers\ArrayHelper;

/**
 * BlogcSearch represents the model behind the search form about `app\modules\admin\models\Blogc`.
 */
class BlogcSearch extends Blogc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sort_order', 'parent_id'], 'integer'],
            [['title', 'descr', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Blogc::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],			
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sort_order' => $this->sort_order,
            'parent_id' => $this->parent_id,
        ]);

        if (!empty($this->url)) {
            $m = ArrayHelper::map(Alias::find()->where(['like', 'url', trim($this->url)])->andWhere(['content_type' => 0])->asArray()->all(), 'content_id', 'content_id');
            $m = (!count($m))?0:$m;
            $query->andFilterWhere(['id' => $m]);
        } 

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'descr', $this->descr]);

        return $dataProvider;
    }
}
