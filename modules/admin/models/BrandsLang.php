<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "brands_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $brands_id
 * @property string $title
 * @property string $title2
 * @property string $short_text
 * @property string $text
 */
class BrandsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brands_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brands_id'], 'integer'],
            [['short_text', 'text'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['title', 'title2'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'brands_id' => 'Brands ID',
            'title' => 'Title',
            'title2' => 'Title2',
            'short_text' => 'Short Text',
            'text' => 'Text',
        ];
    }
}
