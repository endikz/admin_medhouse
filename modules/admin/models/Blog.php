<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $url
 * @property string $short_text
 * @property string $text
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $event_date
 * @property integer $category_id
 */
class Blog extends \yii\db\ActiveRecord
{
    public $upload;
    public $upload2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['short_text', 'text'], 'string'],
            ['link', 'url'],
            ['link', 'filter', 'filter'=>'strtolower'],
            ['url', 'unique'],
            ['event_date', 'safe'],
            [['created_at', 'updated_at', 'category_id', 'sort_order'], 'integer'],
            [['title', 'image', 'upload2'], 'string', 'max' => 255],
            [['upload'], 'file', 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 10],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blogc::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Заголовок',
            'image' => 'Картинка',
            'url' => 'Ссылка',
            'short_text' => 'Краткий текст',
            'text' => 'Текст',
            'created_at' => 'Создана',
            'updated_at' => 'Изменена',
            'event_date' => 'Дата события',
            'category_id' => 'Категория',
            'sort_order' => 'Сортировка',
            'link' => 'Адрес сайта',
            'upload' => 'Загрузить фотографии в галерею с компьютера (jpg, jpeg, png)',
            'upload2' => 'Добавить фотографии в галерею из файлов сайта (jpg, jpeg, png)',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'blog_id',
                'tableName' => "{{%blog_lang}}",
                'attributes' => [
                    'title', 'short_text', 'text'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->url !== '/') {
                $this->url = str_replace(' ', '-', trim(trim($this->url, '/'))); 
            }
            $this->url = strtolower($this->url);
            $this->sort_order = !empty($this->sort_order)?intval($this->sort_order):0;
                       
            return true;
        }
        return false;
    }

    public function getCat() {
        return $this->hasOne(Blogc::className(), ['id' => 'category_id']);
    }     

    public function getGal()
    {
        return $this->hasMany(Galery::className(), ['item_id' => 'id'])
            ->where('item_type = 0');
    }

    public function beforeDelete()
    {
        if (!empty($this->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image)) {
            unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image);
        }              
        foreach ($this->gal as $item) {
            if (!empty($item->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image)) {
                unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image);
            }
            $item->delete();
        }
        
        return parent::beforeDelete();
    }       
}
