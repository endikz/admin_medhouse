<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\AutoCity;

/**
 * AutoCitySearch represents the model behind the search form about `app\modules\admin\models\AutoCity`.
 */
class AutoCitySearch extends AutoCity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['city_name', 'head_tel1', 'head_tel2', 'hot_tel1', 'hot_tel2', 'c_graf', 'c_adres', 'c_mail', 'c_tel1', 'c_tel2', 'c_tel3', 'c_tel4'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoCity::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],			
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'city_name', $this->city_name])
            ->andFilterWhere(['like', 'head_tel1', $this->head_tel1])
            ->andFilterWhere(['like', 'head_tel2', $this->head_tel2])
            ->andFilterWhere(['like', 'hot_tel1', $this->hot_tel1])
            ->andFilterWhere(['like', 'hot_tel2', $this->hot_tel2])
            ->andFilterWhere(['like', 'c_graf', $this->c_graf])
            ->andFilterWhere(['like', 'c_adres', $this->c_adres])
            ->andFilterWhere(['like', 'c_mail', $this->c_mail])
            ->andFilterWhere(['like', 'c_tel1', $this->c_tel1])
            ->andFilterWhere(['like', 'c_tel2', $this->c_tel2])
            ->andFilterWhere(['like', 'c_tel3', $this->c_tel3])
            ->andFilterWhere(['like', 'c_tel4', $this->c_tel4]);

        return $dataProvider;
    }
}
