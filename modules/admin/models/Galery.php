<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "galery".
 *
 * @property integer $id
 * @property string $image
 * @property integer $item_type
 * @property integer $item_id
 * @property string $item_name
 * @property integer $sort_order
 */
class Galery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'galery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_type', 'item_id', 'sort_order'], 'integer'],
            [['image', 'item_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'item_type' => 'Принадлежность',
            'item_id' => 'Страница принадлежности',
            'item_name' => 'Item Name',
            'sort_order' => 'Sort Order',
        ];
    }

    public function getBlog() {
        return $this->hasOne(Blog::className(), ['id' => 'item_id']);
    }    

    public function getPr() {
        return $this->hasOne(Pr::className(), ['id' => 'item_id']);
    }     

    public function beforeDelete()
    {
        if (!empty($this->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image)) {
            unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$this->image);
        }
        
        return parent::beforeDelete();
    }          
}
