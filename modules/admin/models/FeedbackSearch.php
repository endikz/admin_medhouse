<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Feedback;

/**
 * FeedbackSearch represents the model behind the search form about `app\modules\admin\models\Feedback`.
 */
class FeedbackSearch extends Feedback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'status', 'rating', 'type', 'pid', 'uid'], 'integer'],
            [['text', 'text2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Feedback::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],			
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'rating' => $this->rating,
            'type' => $this->type,
            'pid' => $this->pid,
            'uid' => $this->uid,
        ]);

        if (isset($_GET['created_at'])) {
            $parts = explode(' - ', $_GET['created_at']);
            $date_from = strtotime(trim($parts[0]));
            $date_to = isset($parts[1])?strtotime($parts[1]):'';
            if (!empty($date_from) && !empty($date_to)) {
                $query->andFilterWhere(['between', 'created_at', $date_from, $date_to]);
            }
        }          

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'text2', $this->text2]);

        return $dataProvider;
    }
}
