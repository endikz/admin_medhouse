<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "auto_city".
 *
 * @property integer $id
 * @property string $city_name
 * @property string $head_tel1
 * @property string $head_tel2
 * @property string $hot_tel1
 * @property string $hot_tel2
 * @property string $c_graf
 * @property string $c_adres
 * @property string $c_mail
 * @property string $c_tel1
 * @property string $c_tel2
 * @property string $c_tel3
 * @property string $c_tel4
 */
class AutoCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auto_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_name', 'head_tel1', 'head_tel2', 'hot_tel1', 'hot_tel2', 'c_adres', 'c_mail_order', 'c_mail', 'c_tel1', 'c_tel2', 'c_tel3', 'c_tel4'], 'string', 'max' => 45],
            [['c_graf'], 'string'],
            [['status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_name' => 'Город',
            'head_tel1' => 'Телефон1 в шапке сайта, (также используется для кнопок "позвонить")',
            'head_tel2' => 'Телефон2 в шапке сайта',
            'hot_tel1' => 'Телефон1 горячей линии',
            'hot_tel2' => 'Телефон2 горячей линии',
            'c_graf' => 'График работы',
            'c_adres' => 'Адрес',
            'c_mail' => 'Эл. Почта',
            'c_mail_order' => 'Эл. Почта для отправки заказов.',
            'c_tel1' => 'Телефон1',
            'c_tel2' => 'Телефон2',
            'c_tel3' => 'Телефон3',
            'c_tel4' => 'Телефон4',
            'status' => 'Статус',
        ];
    }
}
