<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "faq_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $faq_id
 * @property integer $q
 * @property integer $a
 */
class FaqLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['faq_id', 'q', 'a'], 'integer'],
            [['language'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'faq_id' => 'Faq ID',
            'q' => 'Q',
            'a' => 'A',
        ];
    }
}
