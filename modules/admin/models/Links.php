<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "links".
 *
 * @property integer $id
 * @property string $keyword
 * @property string $link
 * @property string $title
 * @property integer $status
 */
class Links extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keyword', 'link'], 'required'],
            [['keyword'], 'string'],
            [['status'], 'integer'],
            [['link', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword' => 'Ключевое слово',
            'link' => 'Ссылка',
            'title' => 'Title',
            'status' => 'Включена',
        ];
    }
}
