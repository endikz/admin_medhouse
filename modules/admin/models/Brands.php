<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "brands".
 *
 * @property integer $id
 * @property integer $sort_order
 * @property string $image
 * @property string $image_large
 * @property integer $country_id
 * @property string $url
 * @property string $title
 * @property string $text
 */
class Brands extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['sort_order', 'country_id'], 'integer'],
            [['text', 'short_text'], 'string'],
            [['image', 'image_large', 'url', 'title', 'title2', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sort_order' => 'Сортировка',
            'image' => 'Логотип',
            'image_large' => 'Изображение',
            'country_id' => 'Страна',
            'url' => 'Ссылка',
            'link' => 'Сайт',
            'title' => 'Название',
            'title2' => 'Категория',
            'short_text' => 'Краткий текст',
            'text' => 'Текст',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'brands_id',
                'tableName' => "{{%brands_lang}}",
                'attributes' => [
                    'title', 'title2', 'short_text', 'text'
                ]
            ],      
        ];
    }   
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->link = strtolower($this->link);
                       
            return true;
        }
        return false;
    }  

    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }    

    public function getT()
    {
        return $this->hasMany(PrTabTechImages::className(), ['id' => 'tech_id'])
            ->viaTable('pr_tab_tech_images_brands', ['brands_id' => 'id'])->orderBy('caption_ru ASC');
    }      
}
