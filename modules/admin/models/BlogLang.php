<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "blog_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $blog_id
 * @property string $title
 * @property string $short_text
 * @property string $text
 */
class BlogLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blog_id'], 'integer'],
            [['short_text', 'text'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'blog_id' => 'Blog ID',
            'title' => 'Title',
            'short_text' => 'Short Text',
            'text' => 'Text',
        ];
    }
}
