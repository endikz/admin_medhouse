<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\MapCities;

/**
 * MapCitiesSearch represents the model behind the search form about `app\modules\admin\models\MapCities`.
 */
class MapCitiesSearch extends MapCities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nearest_city'], 'integer'],
            [['caption_ru', 'region', 'transform', 'align'], 'safe'],
            [['cx', 'cy', 'r', 'font_size'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MapCities::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],			
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cx' => $this->cx,
            'cy' => $this->cy,
            'r' => $this->r,
            'font_size' => $this->font_size,
            'nearest_city' => $this->nearest_city,
        ]);

        $query->andFilterWhere(['like', 'caption_ru', $this->caption_ru])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'transform', $this->transform])
            ->andFilterWhere(['like', 'align', $this->align]);

        return $dataProvider;
    }
}
