<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_ent".
 *
 * @property integer $pr_id
 * @property integer $attr_id
 * @property integer $value_id
 *
 * @property PrValue $value
 * @property Pr $pr
 * @property PrAttr $attr
 */
class PrEnt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_ent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_id', 'attr_id', 'value_id'], 'integer'],
            [['value_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrValue::className(), 'targetAttribute' => ['value_id' => 'id']],
            [['pr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pr::className(), 'targetAttribute' => ['pr_id' => 'id']],
            [['attr_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrAttr::className(), 'targetAttribute' => ['attr_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pr_id' => 'Pr ID',
            'attr_id' => 'Attr ID',
            'value_id' => 'Value ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue()
    {
        return $this->hasOne(PrValue::className(), ['id' => 'value_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPr()
    {
        return $this->hasOne(Pr::className(), ['id' => 'pr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(PrAttr::className(), ['id' => 'attr_id']);
    }
}
