<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_tab_files_tab".
 *
 * @property integer $file_id
 * @property integer $tab_id
 *
 * @property Pr $tab
 * @property PrTabFiles $file
 */
class PrTabFilesTab extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tab_files_tab';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_id', 'tab_id'], 'required'],
            [['file_id', 'tab_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id' => 'File ID',
            'tab_id' => 'Tab ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTab()
    {
        return $this->hasOne(Pr::className(), ['id' => 'tab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(PrTabFiles::className(), ['id' => 'file_id']);
    }
}
