<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $title
 * @property string $link
 * @property string $image
 * @property integer $ptype
 * @property double $price
 * @property integer $quant
 * @property integer $user_id
 * @property integer $created_at
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'quant', 'user_id', 'created_at', 'order_id'], 'integer'],
            [['price'], 'number'],
            [['title', 'link', 'image'], 'string', 'max' => 255]
        ];
    }

    public function getPr()
    {
        return $this->hasOne(Pr::className(), ['id' => 'product_id']);
    }       

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'title' => 'Title',
            'link' => 'Link',
            'image' => 'Image',
            'price' => 'Price',
            'quant' => 'Quant',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }
}
