<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property integer $parent_id
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'sort_order', 'parent_id'], 'required'],
            [['parent_id', 'sort_order', 'visible_type', 'data_type', 'depth', 'menu_type'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Заголовок',
            'url' => 'Ссылка',
            'parent_id' => 'Родитель',
            'sort_order' => 'Сортировка',
            'visible_type' => 'Видимость',
            'data_type' => 'Тип http запроса',
            'depth' => 'Глубина',           
            'menu_type' => 'Тип меню',			
        ];
    }
	
	public function behaviors()
    {
        return [
			'ml' => [
				'class' => MultilingualBehavior::className(),
				'languages' => Lang::getBehaviorsList(),
				//'languageField' => 'language',
				//'localizedPrefix' => '',
				//'requireTranslations' => false',
				//'dynamicLangClass' => true',
				'defaultLanguage' => Lang::getCurrent()->local,
				'langForeignKey' => 'menu_id',
				'tableName' => "{{%menu_lang}}",
				'attributes' => [
					'name'
				]
			],		
        ];
    }	

    public function getPar() {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }       
	
	public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }	
}
