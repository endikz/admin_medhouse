<?php


namespace app\modules\admin\models;


use yii\db\ActiveRecord;

class ProductCity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_cities';
    }

    public function rules()
    {
        return [
            [['pr_id', 'auto_city_id'], 'integer'],
            [['status_id'], 'boolean']
        ];
    }
}