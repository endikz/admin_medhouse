<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "payment_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property double $price
 * @property integer $status
 * @property string $pay_data
 * @property string $pay_signature
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Orders $order
 * @property User $user
 */
class PaymentHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['pay_data', 'pay_signature'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'order_id' => 'Order ID',
            'price' => 'Price',
            'status' => 'Status',
            'pay_data' => 'Pay Data',
            'pay_signature' => 'Pay Signature',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
