<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_category_params_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $pr_category_params_id
 * @property integer $caption_ru
 *
 * @property PrCategoryParams $prCategoryParams
 */
class PrCategoryParamsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_category_params_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_category_params_id'], 'integer'],
            [['caption_ru'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'pr_category_params_id' => 'Pr Category Params ID',
            'caption_ru' => 'Caption Ru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrCategoryParams()
    {
        return $this->hasOne(PrCategoryParams::className(), ['id' => 'pr_category_params_id']);
    }
}
