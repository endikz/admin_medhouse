<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_pr_category".
 *
 * @property integer $pr_id
 * @property integer $pr_category_id
 */
class PrPrCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_pr_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_id', 'pr_category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pr_id' => 'Pr ID',
            'pr_category_id' => 'Pr Category ID',
        ];
    }
}
