<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

class PrCategoryFront extends \yii\db\ActiveRecord
{
    public static $tree = [];
    public static $tree_list = [];
    public static $child_full_tree = [];
    public static $child_tree = [];

    public static function tableName()
    {
        return 'pr_category';
    }

    public function getPar() {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getChld() {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pr_category_id',
                'tableName' => "{{%pr_category_lang}}",
                'attributes' => [
                    'title'
                ]
            ],      
        ];
    }   

    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }     

    public static function setTree($localized = '')
    {
        if (!count(self::$tree)) {
            if (empty($localized)) {
                $model = self::find()->where(['hidden' => 0])->multilingual()->orderBy('sort_order DESC')->all();
            } else {
                $model = self::find()->where(['hidden' => 0])->localized($localized)->orderBy('sort_order DESC')->all();
            }
            
            $t = [];
            foreach ($model as $item) {
                $item_cat = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'image' => $item->image,
                    'image2' => $item->image2,
                    'link' => $item->link,
                    'sort_order' => $item->sort_order,
                    'parent_id' => !empty($item->parent_id)?$item->parent_id:0
                ];
                $t[$item->id] = $item_cat;
            }

            self::$tree_list = $t;
            self::$tree = self::buildTree($t);
        }

        return self::$tree;
    }

    public static function getFullTree($id, $url = false, $reset = false, &$arr = []) {
        if ($reset) {
            self::$child_tree = [];
            $arr = [];
        }
        if (isset(self::$tree_list[$id])) {
            $e = self::$tree_list[$id];
            self::$child_tree[] = $e;
            $arr[] = ($url)?$e['link']:$e;
            self::getFullTree($e['parent_id'], $url, false, $arr); 
        }

        return ($url)?implode('/', array_reverse($arr)):array_reverse($arr);
    }

    private static function buildTree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = self::buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    private static function setChild($id = 0)
    {
        if (isset(self::$tree_list[$id])) {
            foreach (self::$tree_list as $k => $v) {
                if ($v['parent_id'] == $id) {
                    self::$child_full_tree[] = $k;
                    self::setChild($k);
                }
            }    
        }
    }

    public static function getChild($id = 0)
    {
        self::$child_full_tree = [];
        self::setChild($id);
        self::$child_full_tree[] = $id;
        return self::$child_full_tree;
    }       
}
