<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_tab_tech_images_lang".
 *
 * @property integer $id
 * @property integer $pr_tab_tech_images_id
 * @property string $language
 * @property string $caption_ru
 *
 * @property PrTabTechImages $prTabTechImages
 */
class PrTabTechImagesLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tab_tech_images_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pr_tab_tech_images_id'], 'integer'],
            [['language', 'caption_ru'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pr_tab_tech_images_id' => 'Pr Tab Tech Images ID',
            'language' => 'Language',
            'caption_ru' => 'Caption Ru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrTabTechImages()
    {
        return $this->hasOne(PrTabTechImages::className(), ['id' => 'pr_tab_tech_images_id']);
    }
}
