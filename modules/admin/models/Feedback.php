<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property string $text
 * @property string $text2
 * @property integer $created_at
 * @property integer $status
 * @property integer $rating
 * @property integer $type
 * @property integer $pid
 * @property integer $uid
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text', 'text2'], 'string'],
            [['created_at', 'status', 'rating', 'type', 'pid', 'uid'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Отзыв',
            'text2' => 'Ответ администратора',
            'created_at' => 'Дата добавления',
            'status' => 'Статус',
            'rating' => 'Рейтинг',
            'type' => 'Тип',
            'pid' => 'ID товара',
            'uid' => 'Пользователь',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            //$this->created_at = strtotime($this->created_at);   
            if (empty($this->uid)) {
                $this->uid = null; 
            }         
            return true;
        }
        return false;
    }    
}
