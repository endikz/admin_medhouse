<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "map_addresses".
 *
 * @property integer $id
 * @property integer $external_parent
 * @property string $type
 * @property string $address_ru
 * @property string $tel
 * @property double $map_x
 * @property double $map_y
 * @property integer $map_zoom
 *
 * @property MapCities $externalParent
 * @property MapAddressesLang[] $mapAddressesLangs
 */
class MapAddresses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_addresses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_ru', 'tel', 'map_x', 'map_y', 'map_zoom'], 'required'],
            [['external_parent', 'map_zoom'], 'integer'],
            [['type', 'address_ru'], 'string'],
            [['map_x', 'map_y'], 'number'],
            [['tel'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'external_parent' => 'External Parent',
            'type' => 'Type',
            'address_ru' => 'Адрес',
            'tel' => 'Номер телефона',
            'map_x' => 'Кординаты объекта (x)',
            'map_y' => 'Кординаты объекта (y)',
            'map_zoom' => 'Масштаб карты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExternalParent()
    {
        return $this->hasOne(MapCities::className(), ['id' => 'external_parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapAddressesLangs()
    {
        return $this->hasMany(MapAddressesLang::className(), ['map_addresses_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'map_addresses_id',
                'tableName' => "{{%map_addresses_lang}}",
                'attributes' => [
                    'address_ru'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }      
}
