<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Videos;

/**
 * VideosSearch represents the model behind the search form about `app\modules\admin\models\Videos`.
 */
class VideosSearch extends Videos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'good', 'brand', 'category', 'rate', 'good_rate'], 'integer'],
            [['video_id', 'caption_ru', 'caption_ua', 'image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Videos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],			
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'good' => $this->good,
            'brand' => $this->brand,
            'category' => $this->category,
            'rate' => $this->rate,
            'good_rate' => $this->good_rate,
        ]);

        $query->andFilterWhere(['like', 'video_id', $this->video_id])
            ->andFilterWhere(['like', 'caption_ru', $this->caption_ru])
            ->andFilterWhere(['like', 'caption_ua', $this->caption_ua])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
