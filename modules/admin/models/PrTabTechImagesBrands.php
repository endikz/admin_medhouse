<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "pr_tab_tech_images_brands".
 *
 * @property integer $brands_id
 * @property integer $tech_id
 *
 * @property PrTabTechImages $tech
 * @property Brands $brands
 */
class PrTabTechImagesBrands extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pr_tab_tech_images_brands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brands_id', 'tech_id'], 'required'],
            [['brands_id', 'tech_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'brands_id' => 'Brands ID',
            'tech_id' => 'Tech ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTech()
    {
        return $this->hasOne(PrTabTechImages::className(), ['id' => 'tech_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrands()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brands_id']);
    }
}
