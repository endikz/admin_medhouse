<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "map_cities".
 *
 * @property integer $id
 * @property string $caption_ru
 * @property string $region
 * @property double $cx
 * @property double $cy
 * @property double $r
 * @property double $font_size
 * @property string $transform
 * @property integer $nearest_city
 * @property string $align
 *
 * @property MapAddresses[] $mapAddresses
 * @property MapCities $nearestCity
 * @property MapCities[] $mapCities
 * @property MapCitiesLang[] $mapCitiesLangs
 */
class MapCities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['caption_ru', 'region', 'cx', 'cy', 'r', 'font_size'], 'required'],
            [['cx', 'cy', 'r', 'font_size'], 'number'],
            [['transform', 'align'], 'string'],
            [['nearest_city'], 'integer', 'skipOnEmpty' => true],
            [['caption_ru', 'region'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'caption_ru' => 'Заголовок',
            'region' => 'Регион',
            'cx' => 'Смещение X',
            'cy' => 'Смещение Y',
            'r' => 'Радиус',
            'font_size' => 'Размер шрифта (px)',
            'transform' => 'Трансформация',
            'nearest_city' => 'Ближайший город',
            'align' => 'Выравнивение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapAddresses()
    {
        return $this->hasMany(MapAddresses::className(), ['external_parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNearestCity()
    {
        return $this->hasOne(MapCities::className(), ['id' => 'nearest_city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapCities()
    {
        return $this->hasMany(MapCities::className(), ['nearest_city' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapCitiesLangs()
    {
        return $this->hasMany(MapCitiesLang::className(), ['map_cities_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'map_cities_id',
                'tableName' => "{{%map_cities_lang}}",
                'attributes' => [
                    'caption_ru'
                ]
            ],      
        ];
    }   
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }    
}
