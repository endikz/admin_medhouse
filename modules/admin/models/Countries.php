<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $country_id
 * @property string $title_ru
 * @property string $title_ua
 * @property string $title_be
 * @property string $title_en
 * @property string $title_es
 * @property string $title_pt
 * @property string $title_de
 * @property string $title_fr
 * @property string $title_it
 * @property string $title_po
 * @property string $title_ja
 * @property string $title_lt
 * @property string $title_lv
 * @property string $title_cz
 * @property string $title_zh
 * @property string $title_he
 * @property string $code_alpha2
 * @property string $code_alpha3
 * @property integer $code_iso
 * @property string $flag
 * @property integer $dc
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru', 'flag'], 'required'],
            [['code_iso', 'dc'], 'integer'],
            [['title_ru', 'title_ua', 'title_be', 'title_en', 'title_es', 'title_pt', 'title_de', 'title_fr', 'title_it', 'title_po', 'title_ja', 'title_lt', 'title_lv', 'title_cz', 'title_zh', 'title_he'], 'string', 'max' => 60],
            [['code_alpha2'], 'string', 'max' => 2],
            [['code_alpha3'], 'string', 'max' => 3],
            [['flag'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => '#',
            'title_ru' => 'Русский',
            'title_ua' => 'Украинский',
            'title_be' => 'Беларусский',
            'title_en' => 'Английский',
            'title_es' => 'Эстонский',
            'title_pt' => 'Португальский',
            'title_de' => 'Немецкий',
            'title_fr' => 'Французский',
            'title_it' => 'Итальянский',
            'title_po' => 'Польский',
            'title_ja' => 'Японский',
            'title_lt' => 'Латвийский',
            'title_lv' => 'Литовский',
            'title_cz' => 'Чежский',
            'title_zh' => 'Китайский',
            'title_he' => 'Иврит',
            'code_alpha2' => 'Alpha код',
            'code_alpha3' => 'Alpha код 2',
            'code_iso' => 'Iso код',
            'flag' => 'Флаг',
            'dc' => 'Телефон',
        ];
    }
}
