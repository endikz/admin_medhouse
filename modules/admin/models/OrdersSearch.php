<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\modules\admin\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'delivery_id', 'pay_id', 'uid', 'created_at', 'updated_at', 'status', 'oid', 'prc_id'], 'integer'],
            [['phone', 'email', 'fname', 'lname', 'city', 'addr', 'descr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find()->alias('ord');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 1000,
			],			
			'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]			
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->prc_id)) {
            $query->joinWith(['cart' => function ($query) {
                $query->where('{{cart}}.product_id = :id', [':id' => $this->prc_id]);
            }]);
        } 
        
        if (isset($_GET['del'])) {
            $query->andFilterWhere([
                'id' => $this->id,
                'removed' => 1,
                'delivery_id' => $this->delivery_id,
                // 'pay_id' => $this->pay_id,
                'uid' => $this->uid,
                // 'created_at' => $this->created_at,
                // 'updated_at' => $this->updated_at,
                'oid' => $this->oid,
                'status' => $this->status,
            ]);
        } else {
            $query->andFilterWhere([
                'id' => $this->id,
                'removed' => 0,
                'delivery_id' => $this->delivery_id,
                // 'pay_id' => $this->pay_id,
                // 'prc_id' =>  $this->prc_id,
                'uid' => $this->uid,
                // 'created_at' => $this->created_at,
                // 'updated_at' => $this->updated_at,
                'oid' => $this->oid,
                'status' => $this->status,
            ]);
        }

        if (isset($_GET['created_at'])) {
            $parts = explode(' - ', $_GET['created_at']);
            $date_from = trim($parts[0]);
            $date_to = isset($parts[1])?$parts[1]:'';
            if (!empty($date_from) && !empty($date_to)) {
                $query->andFilterWhere(['between', 'ord.created_at', strtotime($date_from), strtotime($date_to) + 86399]);
            }
        }
        

        $query->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'addr', $this->addr])
            ->andFilterWhere(['like', 'descr', $this->descr]);

        // return $dataProvider;

            return [
            'dataProvider' => $dataProvider,
            'total' => $this->countTotal($query)
        ];
    }
    private function countTotal($query) {
        $count_query = clone $query;
        $total = 0;
        $total_quant = 0;
        foreach ($count_query->select('c.id, c.price, c.quant')->joinWith('cart c')->asArray()->all() as $k => $v) {
            $total += $v['price'] * $v['quant'];
            $total_quant += $v['quant'];
        }
        $total = round($total, 2);
        $total_quant = round($total_quant, 2);
        return [
            'total' => $total,
            'total_quant' => $total_quant
        ];
    }
}
