<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "slider_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $slider_id
 * @property string $title
 */
class SliderLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['title', 'image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'slider_id' => 'Slider ID',
            'title' => 'Title',
        ];
    }
}
