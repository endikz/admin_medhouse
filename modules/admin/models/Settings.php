<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $descr
 * @property integer $maint
 * @property string $copy
 * @property string $logo
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'copy', 'email_admin'], 'required'],
            [['descr', 'descr1', 'email_tpl'], 'string'],
            [['maint', 'cclear'], 'integer'],
            [['kpp'], 'number'],
            [['burl'], 'url'],
            [['email_admin'], 'email'],
            [['name', 'copy', 'logo', 'maint_img', 'def_img', 'burl', 'bpath', 'pri1', 'pri2', 'pri3', 'pri4', 'title', 's1', 's2', 's3', 's4', 's5', 'google_feed_filename', 'google_feed_title', 'google_feed_description', 'catalog_img'], 'string', 'max' => 255],
            [['critical', 'critical2', 'ga', 'seo_text'], 'string', 'max' => 50000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Название сайта',
            'google_feed_filename' => 'Название файла для Google Merchant Feed',
            'google_feed_title' => 'Title для Google Merchant Feed',
            'google_feed_description' => 'Description для Google Merchant Feed',
            'title' => 'Заголовок сайта',
            'descr' => 'Описание сайта',
            'descr1' => 'Информация на странице (Контакты)',
            'maint' => 'Режим обслуживания',
            'copy' => 'Копирайт',
            'logo' => 'Логотип',
            'kpp' => 'Дополнительная стоимость для KazPost',
            'email_admin' => 'E-mail администратора',
            'email_tpl' => 'Шаблон письма',
            'maint_img' => 'Картинка заглушка режима обслуживания',
            'def_img' => 'Картинка по умолчанию',
            'catalog_img' => 'Картинка для каталога на главной',
            'burl' => 'Ссылка к файловому серверу, например: http://admin.mysite.com',
            'bpath' => 'Абсолютный путь к файловому серверу, например: /var/www/admin.mysite.com',
            'critical' => 'Critical CSS для главной',
            'critical2' => 'Critical CSS для не главной',
            'ga' => 'Код Google Analytics',
            'pri1' => 'Распродажа',
            'pri2' => 'Новинка',
            'pri3' => 'Скидка',
            'pri4' => 'Лидер продаж',
            's1' => 'Страница Вконтакте',
            's2' => 'Страница Google+',
            's3' => 'Страница Facebook',
            's4' => 'Страница Twitter',
            's5' => 'Страница Youtube',
            'cclear' => 'Очистить кеш при перезагрузке',
            'seo_text' => 'Текст футура',
        ];
    }
	
	public function behaviors()
    {
        return [
			'ml' => [
				'class' => MultilingualBehavior::className(),
				'languages' => Lang::getBehaviorsList(),
				//'languageField' => 'language',
				//'localizedPrefix' => '',
				//'requireTranslations' => false',
				//'dynamicLangClass' => true',
				'defaultLanguage' => Lang::getCurrent()->local,
				'langForeignKey' => 'settings_id',
				'tableName' => "{{%settings_lang}}",
				'attributes' => [
					'name', 'title', 'copy', 'descr', 'descr1', 'seo_text'
				]
			],		
        ];
    }	
	
	public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }	

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->bpath = '/'.trim($this->bpath, '/');
            $this->burl = trim($this->burl, '/');
                       
            return true;
        }
        return false;
    }      
}
