<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * This is the model class for table "pd".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $type
 * @property integer $status
 *
 * @property PdLang[] $pdLangs
 */
class Pd extends \yii\db\ActiveRecord
{
    public $ids_array;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type', 'status'], 'required'],
            [['text'], 'string'],
            [['ids_array'], 'safe'],
            [['type', 'status', 'price_from', 'price_prs', 'price_type'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Описание',
            'type' => 'Группа',
            'status' => 'Статус',
            'ids_array' => 'Связанные способы оплаты',
            'price_from' => 'Максимальная сумма заказа, до которой действует наценка',
            'price_prs' => 'Наценка. (% от заказа или фиксированная сумма)',
            'price_type' => 'Тип наценки: % от заказа или фиксированная сумма',
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Lang::getBehaviorsList(),
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                //'requireTranslations' => false',
                //'dynamicLangClass' => true',
                'defaultLanguage' => Lang::getCurrent()->local,
                'langForeignKey' => 'pd_id',
                'tableName' => "{{%pd_lang}}",
                'attributes' => [
                    'title', 'text'
                ]
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            
            if (is_array($this->ids_array)) {
                $this->ids = json_encode($this->ids_array);
            }
                       
            return true;
        }
        return false;
    }
    
    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();
        return $q;
    }

    public function getPdLangs()
    {
        return $this->hasMany(PdLang::className(), ['pd_id' => 'id']);
    }
}
