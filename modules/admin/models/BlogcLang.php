<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "blogc_lang".
 *
 * @property integer $id
 * @property integer $blogc_id
 * @property string $language
 * @property string $title
 * @property string $descr
 */
class BlogcLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blogc_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blogc_id'], 'integer'],
            [['descr'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'blogc_id' => 'Blogc ID',
            'language' => 'Language',
            'title' => 'Title',
            'descr' => 'Descr',
        ];
    }
}
