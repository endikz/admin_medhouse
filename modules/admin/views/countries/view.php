<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Countries */

$this->title = $model->country_id;
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Страны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>			
		</div>	
		<div class="section-body contain-xlg">	
		
			<h1><?= Html::encode($this->title) ?></h1>

			<p>
				<?= Html::a('Редактировать', ['update', 'id' => $model->country_id], ['class' => 'btn btn-primary']) ?>
				<?php if (Yii::$app->user->can('admin')) { ?>
					<?= Html::a('Удалить', ['delete', 'id' => $model->country_id], [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
							'method' => 'post',
						],
					]) ?>
				<?php } ?>
			</p>
		
			<div class="row">
				<div class="col-lg-12">

					<?= DetailView::widget([
						'model' => $model,
						'attributes' => [
							[
								'attribute' => 'flag',
								'value' => $model->flag,
								'format' => ['image', ['width' => '200']],
							],						
				            'country_id',
							'title_ru',
							'title_ua',
							'title_be',
							'title_en',
							'title_es',
							'title_pt',
							'title_de',
							'title_fr',
							'title_it',
							'title_po',
							'title_ja',
							'title_lt',
							'title_lv',
							'title_cz',
							'title_zh',
							'title_he',
							'code_alpha2',
							'code_alpha3',
							'code_iso',
							'dc',
						],
					]) ?>

				</div>
			</div>
		
		</div>
	</section>
</div>
