<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

?>
	
<div class="card">
	<div class="card-head style-primary">
		<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
		<div class="tools"></div>
	</div>

	<?php $form = ActiveForm::begin(['options' => ['onsubmit' => 'return false;', 'class' => 'form']]); ?>

		<div class="card-body floating-label">
			<div class="row">
				<div class="col-sm-12">

					<table class="table no-margin table-hover">
						<thead>
							<tr>
								<th>Название фильтра</th>
								<th>Сортировка</th>
								<th class="text-right"></th>
							</tr>
						</thead>
						<tbody id="attr-table">
							<?php foreach ($model as $item) { ?>
								<tr class="danger">
									<td><?= $item->title ?></td>
									<td><?= $item->sort_order ?></td>
									<td class="text-right">
										<button onclick="del(<?= $item->id ?>, this);" type="button" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Удалить фильтр"><i class="fa fa-trash-o"></i></button>
									</td>
								</tr>
								<tr id="table-val-for-filter<?= $item->id ?>">
									<td></td>
									<td></td>
									<td>
										<table class="table no-margin table-hover val-table">
											<thead>
												<tr>
													<th>Значение</th>
													<th>Сортировка</th>
													<th></th>
												</tr>
											</thead>
											<tbody id="val-for-filter<?= $item->id ?>">
												<?php foreach ($item->val as $item2) { ?>
													<tr>
														<td>
															<?= $item2->value ?>
														</td>
														<td>
															<?= $item2->sort_order ?>	
														</td>
														<td class="text-right">
															<button onclick="delVal(<?= $item2->id ?>, this);" type="button" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Удалить значение"><i class="fa fa-trash-o"></i></button>
														</td>
													</tr>
												<?php } ?>
											</tbody>
											<tfoot>
												<tr class="danger">
													<td>
														<div class="form-group">
															<?= Html::textInput('value', '', ['class' => 'form-control dirty', 'id' => 'value'.$item->id]) ?>
															<label for="value<?= $item->id ?>">Значение фильтра</label>
														</div>
													</td>
													<td>
														<div class="form-group">
															<?= Html::textInput('sort_order', 0, ['class' => 'form-control dirty', 'id' => 'sort_order'.$item->id, 'type' => 'number', 'min' => 0, 'max' => 1000]) ?>
															<label for="sort_order<?= $item->id ?>">Сортировка</label>
														</div>
													</td>
													<td>
														<div class="form-group">
															<?= Html::button('Добавить значение', ['onclick' => 'addVal('.$item->id.');', 'class' => 'btn btn-flat btn-primary ink-reaction']) ?>
														</div>
													</td>
												</tr>
											</tfoot>
										</table>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>

				</div>													
			</div>
			<br>
			<div class="card" id="addf">
				<div class="card-head style-primary">
					<header>Добавить новый фильтр</header>
				</div>
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<?= Html::textInput('title', '', ['class' => 'form-control', 'id' => 'title']) ?>
								<label for="title">Название фильтра</label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= Html::textInput('sort_order', 0, ['class' => 'form-control dirty', 'id' => 'sort_order', 'type' => 'number', 'min' => 0, 'max' => 1000]) ?>
								<label for="type">Сортировка</label>
							</div>
						</div>
					</div>
				</div>
				<div class="card-actionbar">
					<div class="card-actionbar-row">
						<?= Html::button('Добавить фильтр', ['onclick' => 'add();', 'class' => 'btn ink-reaction btn-raised btn-primary']) ?>
					</div>
				</div>
			</div>

		</div>
	<?php ActiveForm::end(); ?>

	<div class="card-actionbar">
		<div class="card-actionbar-row"></div>
	</div>
</div>
<em class="text-caption"><?= Html::encode($this->title) ?></em>	


<script type="text/javascript">
	function del(id, elem) {
		if (confirm('Вы уверены, что хотите удалить выбранный фильтр?')) {
			$.post('/admin/filters/del/', {'id':id}).done(function (data) {
				if (data == 'success') {
					$(elem).parent().parent().remove();
					$('#table-val-for-filter' + id).remove();
				}
			}).fail(function() {
				console.log('error');
			});
		}
	}

	function add() {
		title = $('#title').val();
		if (title != '') {
			$('#title').parent().removeClass('has-error');
			$.post('', {'title':title,'sort_order':$('#sort_order').val(),'add':1}).done(function (data) {
				if (data != '') {
					$('#attr-table').append(data);
					$('#title').val('').removeClass('dirty');
				}
			}).fail(function() {
				console.log('error');
			});	
		} else {
			$('#title').parent().addClass('has-error');
		}
	}	

	function addVal(id) {
		value = $('#value' + id).val();
		if (value != '') {
			$('#value' + id).parent().removeClass('has-error');
			$.post('', {'id':id,'value':value,'sort_order':$('#sort_order' + id).val(),'add-val':1}).done(function (data) {
				if (data != '') {
					$('#val-for-filter' + id).append(data);
					$('#value' + id).val('');
					$('#sort_order' + id).val(0);
				}
			}).fail(function() {
				console.log('error');
			});
		} else {
			$('#value' + id).parent().addClass('has-error');
		}
	}

	function delVal(id, elem) {
		if (confirm('Вы уверены, что хотите удалить выбранное значение фильтра?')) {
			$.post('/admin/filters/delval/', {'id':id}).done(function (data) {
				if (data == 'success') {
					$(elem).parent().parent().remove();
				}
			}).fail(function() {
				console.log('error');
			});
		}		
	}
</script>