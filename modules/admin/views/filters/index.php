<?php
use yii\widgets\Breadcrumbs;

$this->title = 'Фильтры';
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>			
		</div>	
		<div class="section-body contain-xlg">	
		
			<div class="row">
				<div class="col-lg-12">
								
					<?= $this->render('_form', [
		                'model' => $model
		            ]) ?>
						
				</div>
			</div>
		
		</div>
	</section>
</div>