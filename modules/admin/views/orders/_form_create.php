<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;
use app\modules\admin\models\Blogc;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Blog */
/* @var $form yii\widgets\ActiveForm */

?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">

						<div class="col-sm-6">

							<?= $form->field($model, 'phone', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'email', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'fname', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'lname', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'delivery_id')->dropdownList(ArrayHelper::map($delivery, 'id', 'title')) ?>

							<?= $form->field($model, 'pay_id')->dropdownList(ArrayHelper::map($payment, 'id', 'title')) ?>

						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'city', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'addr', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'inn', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'descr', ['template' => '{input}{label}{error}{hint}'])->textArea(['rows' => 4]) ?>
							<?= $form->field($model, 'descr1', ['template' => '{input}{label}{error}{hint}'])->textArea(['rows' => 4]) ?>
						</div>

					</div>
					<div class="row">
						
						<div class="col-sm-6">

							<h3>Добавить товары к заказу:</h3>

							<div class="row">

								<div class="col-sm-6">

									<?= \yii\jui\AutoComplete::widget([
									    'name' => 'cart',
									    'id' => 'cart_auto',
									    'options' => [
											'class' => 'form-control',
											'placeholder' => 'Название товара',
										],
										'clientOptions' => [
									        'source' => Url::to(['autocomplete']),
									        'minLength' => 1,
									        'select' => new JsExpression('function(event, ui) {
					                            $(".val-tpl:first #value_id").val(ui.item.id);
					                            $(".val-tpl:first #value_caption").empty().append(ui.item.value);
					                        }')
									    ],
									]) ?>

								</div>
								<div class="col-sm-4">

									<?= Html::textInput('quant', 1, [
										'class' => 'form-control',
										'id' => 'quant-form',
										'placeholder' => 'Количество',
										'type' => 'number',
										'min' => 1
									]) ?>

								</div>
								<div class="col-sm-2">

									<?= Html::a('<i class="fa fa-plus"></i>', 'javascript:void(0);', ['class' => 'btn btn-floating-action btn-primary', 'onclick' => 'addv();']) ?>

								</div>

							</div>

						</div>
						<div class="col-sm-6">

							<h3>Корзина:</h3>

							<div class="card-body no-padding">
								<ul class="list" id="cart-list">
									
									<div class="row val-tpl" style="margin: 10px 0;display:none;">
										<div class="col-md-8 col-lg-9">
											<p class="text-lg" id="value_caption"></p>
										</div>
										<div class="col-md-4 col-lg-3">
											<?= Html::a('<i class="fa fa-trash"></i>', 'javascript:void(0);', ['class' => 'btn ink-reaction btn-floating-action btn-success', 'onclick' => '$(this).parent().parent().remove();']) ?>
										</div>
										<input type="hidden" id="value_id" name="" value="">
										<input type="hidden" id="quant_id" name="" value="">
									</div>
								

								</ul>
							</div>

						</div>

					</div>
				</div>
			</div>			
		</div>
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>

<script type="text/javascript">
function addv() {
	if ($('#cart_auto').val() !== '') {
		q = $("#quant-form").val();
		val = $(".val-tpl:first #value_id").val();
		$(".val-tpl:first #value_caption").append(" - " + q + " шт.");
		$(".val-tpl:first #quant_id").attr('name', 'quant[]['+val+']').val(q);
		$(".val-tpl:first #value_id").attr('name', 'value[]');
		$(".val-tpl:first").clone().show().appendTo("#cart-list");
		$('#cart_auto').val('');
		$(".val-tpl:first #value_id").attr('name', '');
		$(".val-tpl:first #quant_id").attr('name', '');
	}
}
</script>