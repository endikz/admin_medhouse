<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Orders;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Информация о заказе</a></li>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?php /*
							<?php if (empty($model->email)): ?> */ ?>
								<?= $form->field($model, 'fname', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
								<?= $form->field($model, 'lname', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
								<?= $form->field($model, 'email', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
								<?= $form->field($model, 'city', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
								<?= $form->field($model, 'addr', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
								
							<?php /* <?php endif ?>*/ ?>
							

							
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'delivery_id')->dropdownList(ArrayHelper::map($delivery, 'id', 'title')) ?>

							<?= $form->field($model, 'pay_id')->dropdownList(ArrayHelper::map($payment, 'id', 'title')) ?>

							<?= $form->field($model, 'status')->dropdownList([0 => 'В обработке', 1 => 'Передан в доставку', 2 => 'Доставлен', 3 => 'Отказ']);  ?>

							<?= $form->field($model, 'oid')->dropdownList(ArrayHelper::merge([null => '-'], Yii::$app->params['operators']));  ?>
							<?= $form->field($model, 'descr1', ['template' => '{input}{label}{error}{hint}'])->textArea(['rows' => 4]) ?>
						</div>						
					</div>


					<div class="row">
						
						<div class="col-sm-6">

							<h3>Добавить товары к заказу:</h3>

							<div class="row">

								<div class="col-sm-6">

									<?= \yii\jui\AutoComplete::widget([
									    'name' => 'cart',
									    'id' => 'cart_auto',
									    'options' => [
											'class' => 'form-control',
											'placeholder' => 'Название товара',
										],
										'clientOptions' => [
									        'source' => Url::to(['autocomplete']),
									        'minLength' => 1,
									        'select' => new JsExpression('function(event, ui) {
					                            $(".val-tpl:first #value_id").val(ui.item.id);
					                            $(".val-tpl:first #value_caption").empty().append(ui.item.value);
					                        }')
									    ],
									]) ?>

								</div>
								<div class="col-sm-4">

									<?= Html::textInput('quant', 1, [
										'class' => 'form-control',
										'id' => 'quant-form',
										'placeholder' => 'Количество',
										'type' => 'number',
										'min' => 1
									]) ?>

								</div>
								<div class="col-sm-2">

									<?= Html::a('<i class="fa fa-plus"></i>', 'javascript:void(0);', ['class' => 'btn btn-floating-action btn-primary', 'onclick' => 'addv();']) ?>

								</div>

							</div>

						</div>
						<div class="col-sm-6">

							<h3>Корзина:</h3>

							<div class="card-body no-padding">
								<ul class="list" id="cart-list">
 <?php /*	*/ ?>
									<div class="row val-tpl" style="margin: 10px 0;display:none;">
										<div class="col-md-8 col-lg-9">
											<p class="text-lg" id="value_caption"></p>
										</div>
										<div class="col-md-4 col-lg-3">
											<?= Html::a('<i class="fa fa-trash"></i>', 'javascript:void(0);', ['class' => 'btn ink-reaction btn-floating-action btn-success', 'onclick' => '$(this).parent().parent().remove();']) ?>
										</div>
										<input type="hidden" id="value_id" name="value[]" value="">
										<input type="hidden" id="quant_id" name="quant[][]" value="">
									</div>
	

									<?php if (!empty($model->cart)) { ?>
										<?php foreach ($model->cart as $item): ?>
											<div class="row val-tpl" style="margin: 10px 0;display: block;;">
												<div class="col-md-8 col-lg-9">
													<p class="text-lg" id="value_caption"><?= $item->pr->title2 ?>  <?= $item->pr->title ?> - <?= $item->quant ?> шт.</p>
												</div>
												<div class="col-md-4 col-lg-3">
													<?= Html::a('<i class="fa fa-trash"></i>', 'javascript:void(0);', ['class' => 'btn ink-reaction btn-floating-action btn-success', 'onclick' => '$(this).parent().parent().remove();']) ?>
												</div>
												<input type="hidden" id="value_id" name="value[]" value="<?= $item->product_id ?>">
												<input type="hidden" id="quant_id" name="quant[][<?= $item->product_id ?>]" value="<?= $item->quant ?>">
											</div>
										<?php endforeach ?>
									<?php } else { ?>
										
									<?php } ?>


								</ul>
							</div>

						</div>

					</div>



					
					<div class="form-group">
					
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-printable style-default-light">
							<div class="card-head">
								<div class="tools">
									<div class="btn-group">
										<a class="btn btn-floating-action btn-primary" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a>
									</div>
								</div>
							</div><!--end .card-head -->
							<div class="card-body style-default-bright">

								<!-- BEGIN INVOICE HEADER -->
								<div class="row">
									<div class="col-xs-8">
										<h1 class="text-light"><i class="fa fa-cart-arrow-down fa-fw fa-2x text-accent-dark"> </i><strong class="text-accent-dark">Информация о заказе</strong></h1>
									</div>
									<div class="col-xs-4 text-right">
										<h1 class="text-light text-default-light">Заказ #<?= $model->id ?></h1>
									</div>
								</div><!--end .row -->
								<!-- END INVOICE HEADER -->

								<br/>

								<!-- BEGIN INVOICE DESCRIPTION -->
								<div class="row">
									<div class="col-xs-4">
										<h4 class="text-light">Информация о клиенте:</h4>
										
										<address>
											<strong>Имя:</strong> <?= $model->fname ?><br>
											<strong>Фамилия:</strong> <?= $model->lname ?><br>
											<strong>Телефон:</strong> <?= $model->phone ?><br>
											<strong>E-mail:</strong> <?= $model->email ?><br>
											<strong>Город:</strong> <?= $model->city ?><br>
											<strong>Адрес:</strong> <?= $model->addr ?><br>
											<strong>Гость / авторизованный:</strong> <?= (!empty($model->uid))?'Авторизованный пользователь (ID '.$model->uid.')':'Гость' ?><br>
										</address>
									</div><!--end .col -->
									<div class="col-xs-4">
										<h4 class="text-light">Доставка и оплата:</h4>
										<address>
											<strong>Доставка:</strong> <?= (isset($delivery[$model->delivery_id]))?$delivery[$model->delivery_id]->title:'' ?><br>
											<strong>Оплата:</strong> <?= (isset($payment[$model->pay_id]))?$payment[$model->pay_id]->title:'' ?><br>
											<?php if (!empty($model->p)) { ?>
												<br>
												<div class="well">
													<div class="clearfix">
														<div class="pull-left">Статус оплаты картой: </div>
														<div class="pull-right"> <?= (!empty($model->p->status))?'<span style="color:green;">оплата успешно выполнена</span>':'<span style="color:red;">ожидает оплаты</span>' ?></div>
													</div>
													<div class="clearfix">
														<div class="pull-left">Сумма оплаты: </div>
														<div class="pull-right"> <?= (!empty($model->p->price))?$model->p->price:'-' ?></div>
													</div>
													<div class="clearfix">
														<div class="pull-left">Время оплаты: </div>
														<div class="pull-right"> <?= (!empty($model->p->created_at))?date('d.m.Y H:i', $model->p->created_at):'-' ?></div>
													</div>
													<div class="clearfix">
														<div class="pull-left">Время ответа банка: </div>
														<div class="pull-right"> <?= (!empty($model->p->updated_at))?date('d.m.Y H:i', $model->p->updated_at):'-' ?></div>
													</div>
												</div>
											<?php } ?>
										</address>
									</div><!--end .col -->
									<div class="col-xs-4">
										<div class="well">
											<div class="clearfix">
												<div class="pull-left"> НОМЕР ЗАКАЗА : </div>
												<div class="pull-right"> #<?= $model->id ?> </div>
											</div>
											<div class="clearfix">
												<div class="pull-left"> ДАТА ОФОРМЛЕНИЯ : </div>
												<div class="pull-right"> <?= date('d.m.Y H:i', $model->created_at) ?> </div>
											</div>
										</div>
									</div><!--end .col -->
								</div><!--end .row -->
								<!-- END INVOICE DESCRIPTION -->

								<br/>

								<!-- BEGIN INVOICE PRODUCTS -->
								<div class="row">
									<div class="col-md-12">
										<table class="table">
											<thead>
												<tr>
													<th style="width:60px" class="text-center">#</th>
													<th class="text-left">Товар</th>
													<th class="text-left">Фото</th>
													<th class="text-left"></th>
													<th class="text-left">Код товара</th>
													<th class="text-left">Количество</th>
													<th style="width:140px" class="text-right">Стоимость единицы (тенге)</th>
													<th style="width:140px" class="text-right">Общая стоимость (тенге)</th>
												</tr>
											</thead>
											<tbody>
												<?php 
													$i = 1; 
													$fin_pr = 0;
													$total_pr = 0;
													$total_disco_pr = 0;
												?>
												<?php foreach ($model->cart as $item) { ?>
													<?php
														$fin_pr = round(($item->price * $item->quant), 2);
													?>
													<tr>
														<td class="text-center"><?= $i ?></td>
														<td><?= (!empty($item->pr->title))?$item->pr->title:'-' ?></td>
														<td>
															<?php if (!empty($item->pr->image)): ?>
																<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$item->pr->image,'100x100') ?>
															<?php endif ?>
														</td>
														<td></td>
														<td><?= (!empty($item->pr->sku))?$item->pr->sku:'-' ?></td>
														<td><?= $item->quant ?></td>
														<td class="text-right"><?= $item->price ?> тенге</td>
														<td class="text-right"><?= $fin_pr ?> тенге</td>
													</tr>
													<?php 
														$i++; 
														$total_pr += $fin_pr;															
													?>
												<?php } ?>

												<tr>
													<td colspan="5"></td>
													<td colspan="2" class="text-right"><strong class="text-lg">Доставка</strong></td>
													<td class="text-right"><strong class="text-lg"><?= number_format($model->del, 2, '.', ' ') ?> тенге</strong></td>
												</tr>
												<tr>
													<td colspan="5" rowspan="4">
														<h3 class="text-light opacity-50">Примечание к заказу</h3>
														<p><small><?= $model->descr ?></small></p>
													</td>
													<td colspan="2" class="text-right"><strong class="text-lg text-accent">Общая стоимость</strong></td>
													<td class="text-right">
														<strong class="text-lg text-accent">
															<?php 
																$order_total = $total_pr + $model->del;
																echo number_format($order_total, 2, '.', ' ').' тенге';
															?>
														</strong>
													</td>
												</tr>


											</tbody>
										</table>
									</div><!--end .col -->
								</div><!--end .row -->
								<!-- END INVOICE PRODUCTS -->

							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
				</div><!--end .row -->

			</div>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton('Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>

<script type="text/javascript">
function addv() {
	if ($('#cart_auto').val() !== '') {
		q = $("#quant-form").val();
		val = $(".val-tpl:first #value_id").val();
		$(".val-tpl:first #value_caption").append(" - " + q + " шт.");
		$(".val-tpl:first #quant_id").attr('name', 'quant[]['+val+']').val(q);
		$(".val-tpl:first #value_id").attr('name', 'value[]');
		$(".val-tpl:first").clone().show().appendTo("#cart-list");
		$('#cart_auto').val('');
		$(".val-tpl:first #value_id").attr('name', '');
		$(".val-tpl:first #quant_id").attr('name', '');
	}
}
</script>