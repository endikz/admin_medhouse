<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use kartik\daterange\DateRangePicker;
use yii\helpers\VarDumper;


// var_dump(ArrayHelper::map($prc_id, 'id', 'title'));


/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>			



		</div>
		<div class="section-body contain-xlg">

			<div class="card">

				<div class="card-head style-primary">
					<header><i class="fa fa-table"></i> <?= Html::encode($this->title) ?></header>
					<div class="tools">
						<?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
					</div>
				</div>

				<div class="card-body">

					<div class="row">
						<div class="col-lg-12">
							
							<?php /* if ($totalprsum != 0) { ?>
								<p>Общая сумма товаров:  <?= number_format($totalprsum, 2, '.', ' ') ?> тенге. </p>
								<p>Общее ко-во товаров: <?= $totalprquant ?> шт.</p>
							<?php } else { ?>
								<h3>Общая сумма заказов: <?= number_format($total, 2, '.', ' ') ?> тенге.</h3>
							<?php } */?>
							<h3>Общая сумма заказов: <?= number_format($total['total'], 2, '.', ' ') ?> тенге.</h3>
							<p>Общее ко-во товаров: <?= $total['total_quant'] ?> шт.</p>
						</div>
						<div class="wrapper1">
						    <div class="div1">
						    </div>
						</div>
						<div class="col-lg-12">



							<?= GridView::widget([
								'dataProvider' => $dataProvider,
								'id' => 'grid',
								'layout'=>"
									<div class='dataTables_info'>{summary}</div>
									<div class='card'>
										<div class='card-body no-padding'>
											<div class='table-responsive no-margin'>{items}</div>
										</div>
									</div>
									<div class='dataTables_paginate paging_simple_numbers'>{pager}</div>
								",		
								'tableOptions' => [
									'class' => 'table table-striped no-margin table-hover table-bordered',
								],	
								// 'rowOptions' => function ($model, $key, $index, $grid) {
								// 	$u = Url::toRoute('/admin/orders/update/'.$model['id']);
								//     return ['id' => $model['id'], 'onclick' => 'document.location.replace("'.$u.'");', 'style' => 'cursor:pointer;'];
								// },								
								'filterModel' => $searchModel,
								'columns' => [
									//['class' => 'yii\grid\SerialColumn'],
									['class' => 'yii\grid\CheckboxColumn'],
									//'id',
									'phone',
									'email',
									'fname',
									'city',
									'addr',
									'delivery_id' => [
										'attribute' => 'delivery_id',
										'filter' => ArrayHelper::map($delivery, 'id', 'title'),
										'value' => function($model, $index, $widget) use ($delivery) { 
											return (isset($delivery[$model->delivery_id]))?$delivery[$model->delivery_id]->title:'';
										},
									],

									// 'pay_id' => [
									// 	'attribute' => 'pay_id',
									// 	'filter' => ArrayHelper::map($payment, 'id', 'title'),
									// 	'value' => function($model, $index, $widget) use ($payment) { 
									// 		return (isset($payment[$model->pay_id]))?$payment[$model->pay_id]->title:'';
									// 	},
									// ],


									'prc_id' => [
										'attribute' => 'prc_id',
										'filter' => ArrayHelper::map($prc_id, 'product_id', 'title'),
										'value' => function($model, $index, $widget) { 
												$ttt = '';
												foreach ($model->cart as $v) {
													$ttt .= $v['title'].'- '.$v['quant'].' шт, ';
												}
											return $ttt;
										},
									],				



									//'descr',
									[
										'attribute' => 'created_at',
										'format' => 'datetime',
										'filter' => DateRangePicker::widget([
										    'name'=>'created_at',
										    'value' => isset($_GET['created_at'])?$_GET['created_at']:'',
										    'convertFormat' => true,
										    'pluginOptions' => [
										        'timePicker' => false,
										        'locale' => [
										            'format' => 'Y-m-d'
										        ],
										        'opens' => 'left'
										    ]
										]),							
									],
									'status' => [
										'attribute' => 'status',
										'filter' => [0 => 'В обработке', 1 => 'Передан в доставку', 2 => 'Доставлен', 3 => 'Отказ'],
										'value' => function($model, $index, $widget) {
											if ($model->status == 0) {
												return 'В обработке';
											} else if ($model->status == 1) {
												return 'Передан в доставку';
											} else if ($model->status == 2) {
												return 'Доставлен';
											} else {
												return 'Отказ';
											}
										},
									],
									[
										'label' => 'Сумма заказа',
										'value' => function($model) {
											$qq = 0;
											foreach ($model->cart as $item) {
												$qq += $item->price * $item->quant;
											}
											return $qq;
										},
									],		
									'oid' => [
										'attribute' => 'oid',
										'filter' => Yii::$app->params['operators'],
										'value' => function($model, $index, $widget) {
											return isset(Yii::$app->params['operators'][$model->oid])?Yii::$app->params['operators'][$model->oid]:'-';
										},
									],									
									[
										'class' => 'yii\grid\ActionColumn',
										'template' => '{update} {delete}',
										'contentOptions' => [ 
											'style' => 'width: 100px;'
										], 	
										'buttons' => [										
											'update' => function ($url, $model) {
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-pencil"></i></button>', $url);
											},	
											'delete' => function ($url, $model) {
												 $options = [
							                    'title' => Yii::t('yii', 'Delete'),
							                    'aria-label' => Yii::t('yii', 'Delete'),
							                    'data-confirm' => Yii::t('yii', 'Вы уверены, что хотите удалить этот элемент?'),
							                    'data-method' => 'post',
							                    'data-pjax' => '0',
							                    ];
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-trash-o"></i></button>', $url, $options);
											},										
										]
									],
								],
							]); ?>
												
						</div>
					</div>	
				</div>
			</div>

		</div>
	</section>
</div>

<script> 

setTimeout(function() { 
	if($('.table-responsive').length > 0) {
		$.each($('.table-responsive'), function(key, h_scroll) {
			var wdth = $(h_scroll).find('.table').width();
			if(wdth>960) {
				var b_scroll = $('<div style="overflow-x:scroll;width:100%"/>');
				var b_cont = $('<div style="height:2px"></div>').width(wdth);
				b_scroll.append(b_cont);
				b_scroll.scroll(function(){
					$(h_scroll).scrollLeft(b_scroll.scrollLeft());
				});
				$(h_scroll).before(b_scroll);
				$(h_scroll).scroll(function(){
					b_scroll.scrollLeft($(h_scroll).scrollLeft());
				});
			}
		});
	}	
}, 1000);
	
	

</script>
