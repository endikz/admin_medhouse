<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\PrTabTechImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Технологии вкладок товаров';
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>			
		</div>
		<div class="section-body contain-xlg">

			<div class="card">

				<div class="card-head style-primary">
					<header><i class="fa fa-table"></i> <?= Html::encode($this->title) ?></header>
					<div class="tools">
						<?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
						<?php if (Yii::$app->user->can('admin')) { ?>							<?= Html::a('<i class="fa fa-fire"></i>', [''], [
								'class' => 'btn ink-reaction btn-floating-action btn-default-light',
								'onclick'=>"
									var keys = $('#grid').yiiGridView('getSelectedRows');
									if (keys!='') {
										if (confirm('Вы уверены, что хотите удалить выбранные элементы?')) {
											$.ajax({
												type : 'POST',
												data: {keys : keys},
												success : function(data) {}
											});
										}
									}
									return false;
								",
							]) ?>
						<?php  } ?>					
					</div>
				</div>

				<div class="card-body">

					<div class="row">
						<div class="col-lg-12">
							<h4></h4>
						</div>
						<div class="col-lg-12">

							<?= GridView::widget([
								'dataProvider' => $dataProvider,
								'id' => 'grid',
								'layout'=>"
									<div class='dataTables_info'>{summary}</div>
									<div class='card'>
										<div class='card-body no-padding'>
											<div class='table-responsive no-margin'>{items}</div>
										</div>
									</div>
									<div class='dataTables_paginate paging_simple_numbers'>{pager}</div>
								",		
								'tableOptions' => [
									'class' => 'table table-striped no-margin table-hover table-bordered',
								],	
								'filterModel' => $searchModel,
								'columns' => [
									//['class' => 'yii\grid\SerialColumn'],
									['class' => 'yii\grid\CheckboxColumn'],
									[
										'filter' => false,
										'format' => 'raw',
										'value' => function($data) {  
											return Html::a(Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$data->image_small,'80x'), $data->image_small, ['data-fancybox' => 1, 'data-caption' => $data->caption_ru]);
										},
									],
									[
										'filter' => false,
										'format' => 'raw',
										'value' => function($data) {  
											return Html::a(Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$data->image,'80x'), $data->image, ['data-fancybox' => 2, 'data-caption' => $data->caption_ru]);
										},
									],
									'caption_ru',
									[
									    'class'=>'kartik\grid\EditableColumn',
									    'attribute'=>'rate',
									    'editableOptions'=>[
									    	'format' => Editable::FORMAT_BUTTON,    
									    	'inputType' => Editable::INPUT_TEXT,
											'formOptions' => [
												'action' => ['edit']
											],
									    ],

									],
									[
										'class' => 'yii\grid\ActionColumn',
										'contentOptions' => [ 
											'style' => Yii::$app->user->can('admin')?'width: 100px;':'width: 50px;'
										], 	
										'template' => Yii::$app->user->can('admin')?'{update} {delete}':'{update}',	
										'buttons' => [										
											'update' => function ($url, $model) {
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-pencil"></i></button>', $url);
											},											
											'delete' => function ($url, $model) {
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-trash-o"></i></button>', $url, ['data-confirm'=>'Вы уверены, что хотите удалить этот элемент?', 'data-method'=>'post', 'data-pjax'=>'0']);
											},
										]
									],
								],
							]); ?>
												
						</div>
					</div>	
				</div>

			</div>

		</div>
	</section>
</div>
