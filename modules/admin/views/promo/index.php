<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\PromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Промо коды';
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>			
		</div>
		<div class="section-body contain-xlg">

			<div class="card">

				<div class="card-head style-primary">
					<header><i class="fa fa-table"></i> <?= Html::encode($this->title) ?></header>
					<div class="tools">
						<?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
						<?php if (Yii::$app->user->can('admin')) { ?>							<?= Html::a('<i class="fa fa-fire"></i>', [''], [
								'class' => 'btn ink-reaction btn-floating-action btn-default-light',
								'onclick'=>"
									var keys = $('#grid').yiiGridView('getSelectedRows');
									if (keys!='') {
										if (confirm('Вы уверены, что хотите удалить выбранные элементы?')) {
											$.ajax({
												type : 'POST',
												data: {keys : keys},
												success : function(data) {}
											});
										}
									}
									return false;
								",
							]) ?>
						<?php  } ?>					
					</div>
				</div>

				<div class="card-body">

					<div class="row">
						<div class="col-lg-12">
							<h4></h4>
						</div>
						<div class="col-lg-12">

							<?= GridView::widget([
								'dataProvider' => $dataProvider,
								'id' => 'grid',
								'layout'=>"
									<div class='dataTables_info'>{summary}</div>
									<div class='card'>
										<div class='card-body no-padding'>
											<div class='table-responsive no-margin'>{items}</div>
										</div>
									</div>
									<div class='dataTables_paginate paging_simple_numbers'>{pager}</div>
								",		
								'tableOptions' => [
									'class' => 'table table-striped no-margin table-hover',
								],	
								'filterModel' => $searchModel,
								'columns' => [
									//['class' => 'yii\grid\SerialColumn'],
									['class' => 'yii\grid\CheckboxColumn'],
									'code',
									[
										'attribute' => 'dur_type',
										'filter' => [
											'1' => 'Бесконечно',
											'0' => 'Один раз',
										],										
										'value' => function ($model, $index, $widget) {
											switch ($model->dur_type) {
												case 1:
													return 'Бесконечно';
												break;
												
												case 0:
													return 'Один раз';
												break;											
											}
										},
									],
									'dur_from',
									'dur_to',
									'prs',
									[
										'attribute' => 'cart_type',
										'filter' => [
											'1' => 'Только на товары без акции',
											'0' => 'На всю корзину',
										],										
										'value' => function ($model, $index, $widget) {
											switch ($model->cart_type) {
												case 1:
													return 'Только на товары без акции';
												break;
												
												case 0:
													return 'На всю корзину';
												break;											
											}
										},
									],
									[
										'attribute' => 'status',
										'filter' => [
											'1' => 'Включен',
											'0' => 'Выключен',
										],										
										'value' => function ($model, $index, $widget) {
											switch ($model->status) {
												case 1:
													return 'Включен';
												break;
												
												case 0:
													return 'Выключен';
												break;											
											}
										},
									],
									'updated_at:datetime',
									[
										'class' => 'yii\grid\ActionColumn',
										'contentOptions' => [ 
											'style' => Yii::$app->user->can('admin')?'width: 100px;':'width: 50px;'
										], 	
										'template' => Yii::$app->user->can('admin')?'{update} {delete}':'{update}',	
										'buttons' => [										
											'update' => function ($url, $model) {
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-pencil"></i></button>', $url);
											},											
											'delete' => function ($url, $model) {
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-trash-o"></i></button>', $url, ['data-confirm'=>'Вы уверены, что хотите удалить этот элемент?', 'data-method'=>'post', 'data-pjax'=>'0']);
											},
										]
									],
								],
							]); ?>
												
						</div>
					</div>	
				</div>

			</div>

		</div>
	</section>
</div>
