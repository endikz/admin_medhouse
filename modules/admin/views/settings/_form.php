<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>
	
	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>

	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Главные</a></li>
				<li><a href="#tab2">HTML письмо</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>				
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">			
					<div class="row">
						<div class="col-sm-6">
							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->logo,'x140') ?>
						
							<?= $form->field($model, 'logo')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector',
								]
							) ?>		

							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->maint_img,'x140') ?>
						
							<?= $form->field($model, 'maint_img')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector',
								]
							) ?>							

							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->def_img,'x140') ?>
						
							<?= $form->field($model, 'def_img')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector',
								]
							) ?>							
							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->catalog_img,'x140') ?>
						
							<?= $form->field($model, 'catalog_img')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector',
								]
							) ?>	

							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->pri1,'x80') ?>
							<?= $form->field($model, 'pri1')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector',
								]
							) ?>

							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->pri2,'x80') ?>
							<?= $form->field($model, 'pri2')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector',
								]
							) ?>	

							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->pri3,'x80') ?>
							<?= $form->field($model, 'pri3')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector',
								]
							) ?>	

							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->pri4,'200x') ?>
							<?= $form->field($model, 'pri4')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector',
								]
							) ?>																					
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'google_feed_filename', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'google_feed_title', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'google_feed_description', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'copy', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							
							<?= $form->field($model, 'title', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'name', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							
							<?= $form->field($model, 'email_admin', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							
							<?= $form->field($model, 'burl', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							
							<?= '<b>DOCUMENT_ROOT</b>: '.$_SERVER['DOCUMENT_ROOT'] ?>
							<?= $form->field($model, 'bpath', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
						
							<?= $form->field($model, 'maint')->dropdownList([0 => 'Нет', 1 => 'Да']); ?>
							<?= $form->field($model, 'cclear')->dropdownList([0 => 'Нет', 1 => 'Да']); ?>

							<?= $form->field($model, 'kpp', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'ga', ['template' => '{input}{label}{error}{hint}'])->textArea(['maxlength' => true, 'rows' => 8]) ?>
							<?= $form->field($model, 'seo_text', ['template' => '{input}{label}{error}{hint}'])->textArea(['maxlength' => true, 'rows' => 8]) ?>

							<?= $form->field($model, 's1', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 's2', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 's3', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 's4', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 's5', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
						</div>
					</div>				
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'descr')->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>
							<?= $form->field($model, 'descr1')->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'critical', ['template' => '{input}{label}{error}{hint}'])->textArea(['maxlength' => true, 'rows' => 10]) ?>
							
							<?= $form->field($model, 'critical2', ['template' => '{input}{label}{error}{hint}'])->textArea(['maxlength' => true, 'rows' => 10]) ?>
						</div>
					</div>
					<div class="form-group">

					</div>
				</div>
			</div>
			<div class="tab-pane" id="tab2">
				<div class="card-body floating-label">	
					<h3>{title} - заголовок письма</h3>
					<h3>{text} - текст письма</h3>
					<h3>{http_host} - адрес сайта</h3>
					<h3>{burl} - Ссылка к файловому серверу, например: http://admin.mysite.com</h3>
					<h3>{bottom} - подпись внизу письма</h3>
					<div class="col-sm-6">		
						<?= $form->field($model, 'email_tpl', ['template' => '{input}{label}{error}{hint}'])->textArea(['style' => 'height: 600px;']) ?>
					</div>	
					<div class="col-sm-6">	
						<iframe src="/admin/settings/email/" style="width:100%;height:600px;border:0;"></iframe>
					</div>			
				</div>			
			</div>				
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'name_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('name').' '.$v) ?>
								<?= $form->field($model, 'title_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('title').' '.$v) ?>
							</div>								
							<div class="col-sm-6">
								<?= $form->field($model, 'copy_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('copy').' '.$v) ?>
								<?= $form->field($model, 'seo_text_'.$k, ['template' => '{input}{label}{error}{hint}'])->textArea(['maxlength' => true, 'rows' => 8])->label($model->getAttributeLabel('seo_text').' '.$v) ?>
							</div>						
						</div>
						<div class="form-group">
							<?= $form->field($model, 'descr_'.$k)->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							)->label($model->getAttributeLabel('descr')) ?>
						</div>
					</div>
				</div>
			<?php } ?>			
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton('Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>
	
<?php ActiveForm::end(); ?>