<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;
?>

<div class="col-sm-6">

	<div class="card tabs-left style-default-light">
		<?php
		$lc = Lang::getCurrent();
		?>
		<ul class="card-head nav nav-tabs text-center" data-toggle="tabs">
			<li class="active"><a href="#block<?= $item->id ?>lang<?= $lc->url ?>"><?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$lc->flag,'20x20', ['class' => 'img-circle']) ?></a></li>
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<li><a href="#block<?= $item->id ?>lang<?= $k ?>"><?= $v ?></a></li>
			<?php } ?>
		</ul>
		<div class="card-body tab-content style-default-bright">
			<div class="tab-pane active" id="block<?= $item->id ?>lang<?= $lc->url ?>">

				<div class="row">
					<div class="col-sm-10">
						<div class="form-group">
							<?= Html::textInput('tab['.$item->id.']['.$lc->url.'][title]', $item->caption_ru, ['class' => 'form-control']) ?>
							<?= Html::label('Заголовок вкладки '.Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$lc->flag,'20x20', ['class' => 'img-circle']), 'tab['.$item->id.']['.$lc->url.'][title]', ['class' => 'control-label']) ?>
						</div>
					</div>
					<div class="col-sm-2 text-right">
						<button type="button" class="btn ink-reaction btn-floating-action btn-primary" onclick="dtab(this);"><i class="fa fa-trash"></i></button>
					</div>
					<div class="col-sm-12">

						<?= TinyMce::widget(
							[
								'id' => 'tabtext'.$item->id.'lang'.$lc->url,
								'name' => 'tab['.$item->id.']['.$lc->url.'][text]',
								'value' => $item->text_ru,
								'fileManager' => [
									'class' => TinyMceElFinder::className(),
									'connectorRoute' => 'el-finder/connector_abs',
								],
							]
						) ?>

					</div>
				</div>

			</div>
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="block<?= $item->id ?>lang<?= $k ?>">

					<?php
						$caption_name = 'caption_ru_'.$k;
						$text_name = 'text_ru_'.$k;
					?>

					<div class="row">
						<div class="col-sm-10">
							<div class="form-group">
								<?= Html::textInput('tab['.$item->id.']['.$k.'][title]', $item->$caption_name, ['class' => 'form-control']) ?>
								<?= Html::label('Заголовок вкладки '.$v, 'tab['.$item->id.']['.$k.'][title]', ['class' => 'control-label']) ?>
							</div>
						</div>
						<div class="col-sm-2 text-right">
							<button type="button" class="btn ink-reaction btn-floating-action btn-primary" onclick="$(this).parents().eq(5).remove();"><i class="fa fa-trash"></i></button>
						</div>
						<div class="col-sm-12">

							<?= TinyMce::widget(
								[
									'id' => 'tabtext'.$item->id.'lang'.$k,
									'name' => 'tab['.$item->id.']['.$k.'][text]',
									'value' => $item->$text_name,
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>

						</div>
					</div>

				</div>
			<?php } ?>
		</div>
	</div>

</div>

<script type="text/javascript">
	function dtab(elem) {
		if (confirm("Вы уверены, что хотите удалить вкладку?")) {
		    $(elem).parents().eq(5).remove();
		}
	}
</script>