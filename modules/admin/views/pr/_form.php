<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\PrCategory;
use app\modules\admin\models\PrAttr;
use app\modules\admin\models\PrEnt;
use app\modules\admin\models\Lang;
use app\modules\admin\models\PrTabTechImages;
use app\modules\admin\models\PrTabFiles;
use app\modules\admin\models\Brands;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Pr */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>

		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>				
				<li><a href="#gal">Дополнительные изображения</a></li>
				<?php if (!$model->isNewRecord) { ?>
					<li><a href="#params">Значения параметров</a></li>
				<?php } ?>
				<li><a href="#tabs">Вкладки</a></li>
				<li><a href="#f">Файлы</a></li>
				<li><a href="#t">Технологии</a></li>
				<li><a href="#ac">Дополнительные категории</a></li>
                <li><a href="#available_in_cities">Наличие в городах</a></li>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">

					<div class="row">
						<div class="col-sm-6">
							<div>
								<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
								<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image,'140x') ?>
							
								<?= $form->field($model, 'image')->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector_products',
									]
								) ?>
							</div>
							<div>
								<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
								<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image_mob,'140x') ?>
							
								<?= $form->field($model, 'image_mob')->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector_products',
									]
								) ?>
							</div>
							<?= $form->field($model, 'title', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'title2', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							
							<div class="row">
								<div class="col-sm-11">
									<?= $form->field($model, 'link', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
								</div>
								<div class="col-sm-1">
									<?= Html::a('<i class="fa fa-link"></i>', 'javascript:void(0);', ['class' => 'btn btn-floating-action btn-default-light', 'onclick' => "v=$('#pr-title').val();if(v!=''){tr=translit(v);$('#pr-link').val(tr).addClass('dirty');}"]) ?>
									<input type="hidden" id="parurl" value="<?= (!empty($model->cat->link))?trim($model->cat->link, '/').'/':'' ?>">
								</div>
							</div>

    						<?= $form->field($model, 'category_id')->dropdownList(PrCategory::setTree()) ?>

  						    <?= $form->field($model, 'sku', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

						    <?= $form->field($model, 'sku2', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
						    <?= $form->field($model, 'sort_order', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>  	

						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'gtin', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
							<?= $form->field($model, 'google_cat', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
							<?= $form->field($model, 'google_product_type', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
						    <?= $form->field($model, 'price', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
						    <div class="row">
						    	<div class="col-md-6">
						    		<?= $form->field($model, 'price2', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
						    	</div>
						    	<div class="col-md-6">
						    		<?= $form->field($model, 'exdate_sale', ['template' => '{input}{label}{error}{hint}'])->widget(\yii\jui\DatePicker::classname(), [
												'language' => 'ru',
												'options' => [
													'class' => 'form-control'							
												],
												'clientOptions' => [
													'changeMonth' => true,
													'changeYear'=> true,
													'showButtonPanel' => true,				
												],
												'dateFormat' => 'yyyy-MM-dd',
											]) ?>
						    	</div>
						    </div>

						    <?= $form->field($model, 'stock')->dropdownList(['0' => 'Нет', '1' => 'Да']) ?>

						    <?= $form->field($model, 'new')->dropdownList(['0' => 'Нет', '1' => 'Да']) ?>

						    <?= $form->field($model, 'sale')->dropdownList(['0' => 'Нет', '1' => 'Да']) ?>
						    <?= $form->field($model, 'bestseller')->dropdownList(['0' => 'Нет', '1' => 'Да']) ?>
						    <?= $form->field($model, 'bdisco')->dropdownList(['0' => 'Нет', '1' => 'Да']) ?>
						    <?= $form->field($model, 'hidden')->dropdownList(['0' => 'Нет', '1' => 'Да']) ?>
						    <?= $form->field($model, 'brands_id')->dropdownList(ArrayHelper::map(Brands::find()->orderBy('title ASC')->all(), 'id', 'title')) ?>

								<div class="row">
						    	<div class="col-md-6">
						    		<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
											<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->fimage,'80x') ?>
										
											<?= $form->field($model, 'fimage')->widget(
												ElFinderInput::className(),
												[
													'connectorRoute' => 'el-finder/connector_products',
												]
											) ?>
						    	</div>
						    	<div class="col-md-6">
						    			<?= $form->field($model, 'ftitle', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
						    	</div>
						    </div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'text1')->widget(
								TinyMce::className(),
								[						
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'text2')->widget(
								TinyMce::className(),
								[						
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>
						</div>
					</div>
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'title_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('title').' '.$v) ?>
							</div>		
							<div class="col-sm-6">
								<?= $form->field($model, 'title2_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('title2').' '.$v) ?>
							</div>					
						</div>
					</div>
				</div>
			<?php } ?>			
			<div class="tab-pane" id="gal">
				<div class="card-body floating-label">

					<ul class="nav nav-tabs nav-justified" data-toggle="tabs">
						<li class="active"><a href="#gal1">Загрузить фото с компьютера</a></li>
						<li><a href="#gal2">Выбрать фото с сервера</a></li>
					</ul>

					<div class="row">
						<div class="card-body tab-content">
							<div class="tab-pane active" id="gal1">
								<div class="col-sm-6">
									<?= $form->field($model, 'upload[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' => '']) ?>
									<?= Html::submitButton('Загрузить фото', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
								</div>		
							</div>
							<div class="tab-pane" id="gal2">
								<div class="col-sm-6">
									<?= $form->field($model, 'upload2')->widget(
										ElFinderInput::className(),
										[
											'connectorRoute' => 'el-finder/connector_products',
										]
									) ?>
									<?= Html::submitButton('Загрузить фото', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
								</div>
							</div>				
						</div>
					</div>
					<hr>
			        <div class="row">
			        	<?php foreach ($model->gal as $item) { ?>
				            <div class="col-lg-2 col-md-2 col-xs-2 thumb" id="foto<?= $item->id ?>">
				            	<?= Html::a('<i class="fa fa-trash-o"></i>', '#offcanvas-demo-left', ['class' => 'btn btn-floating-action btn-default-light delgalbtn', 'data-toggle' => 'offcanvas', 'data-backdrop' => 'false', 'onclick' => '$("#dfid").val('.$item->id.');']) ?>
				                <a class="thumbnail" href="<?= $item->image ?>" rel="lightbox[group]">
				                    <?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image,'300x300') ?>
				                </a>
				            </div>
				        <?php } ?>
				        <?= (!count($model->gal))?'<h3 class="text-light">Нет фотографий.</h3>':'' ?>
			        </div>

				</div>				
			</div>
			<?php if (!$model->isNewRecord) { ?>
				<div class="tab-pane" id="params">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?php foreach ($pcp as $item) { ?>
									<div class="form-group">
										<?php if ($item->type == 'char') { ?>
											<?= Html::textInput('value'.$item->id, (isset($pv[$item->id]))?$pv[$item->id]->value_char:'', ['class' => 'form-control dirty', 'id' => 'value'.$item->id]) ?>
											<label for="value<?= $item->id ?>"><?= $item->caption_ru ?></label>
										<?php } else if ($item->type == 'bool') { ?>
											<?= Html::dropdownList('value'.$item->id, (isset($pv[$item->id]))?$pv[$item->id]->value_bool:'', [0	=> 'Нет', 1	=> 'Да'], ['class' => 'form-control dirty', 'id' => 'value'.$item->id]) ?>
											<label for="value<?= $item->id ?>"><?= $item->caption_ru ?></label>
										<?php } else if ($item->type == 'image') { ?>
											
											<div>

												<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
												<?php 
												if (!empty($pv[$item->id]->i->image_small) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$pv[$item->id]->i->image_small)) { 
													$v = $pv[$item->id]->i->image_small;
													echo Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$v,'80x');
												} else {
													$v = '';
												}
												?>
												<label><?= $item->caption_ru ?> - изображение превью</label>
		
												<?= ElFinderInput::widget(
													[
														'name' => 'value'.$item->id.'[0]',
														'value' => $v,
														'connectorRoute' => 'el-finder/connector',
													]
												) ?>
											</div>

											
											<div>
												
												<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
												<?php 
												if (!empty($pv[$item->id]->i->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$pv[$item->id]->i->image)) { 
													$v = $pv[$item->id]->i->image;
													echo Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$v,'80x');
												} else {
													$v = '';
												}
												?>
												<label><?= $item->caption_ru ?> - полное изображение изображение</label>
		
												<?= ElFinderInput::widget(
													[
														'name' => 'value'.$item->id.'[1]',
														'value' => $v,
														'connectorRoute' => 'el-finder/connector',
													]
												) ?>
											</div>											
										<?php } ?>
									</div>
								<?php } ?>
								<?= Html::a('Добавить новый параметр категории', ['/admin/pr-category-params/create/?pr_category_id='.$model->category_id], ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
							</div>
							<div class="col-sm-6">
								
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<div class="tab-pane" id="tabs">
				<div class="floating-label">

					<div style="position: relative;">
						<button type="button" class="btn ink-reaction btn-floating-action btn-primary" onclick="addtab(this);"><i class="fa fa-plus-square"></i></button>
						<img src="/admin_assets/img/preloader.jpg" class="preloader" id="add-tab-preloader" style="left:0;width:44px;right:inherit;bottom:inherit;">
						<hr>
					</div>

					<div class="row" id="tabs_block">
						<?php 
						$max_id = 0;
						foreach ($model->tabs as $item) { 
							$max_id = ($max_id < $item->id)?$item->id:$max_id;
							?>
							<?= $this->render('_tab', ['item' => $item]) ?>	
							<?php 
						} 
						?>
					</div>
				</div>
			</div>			
			<div class="tab-pane" id="f">
				<div class="checkbox checkbox-styled">
					<?php
						$fl = [];
						$pti = PrTabFiles::find()->orderBy('caption_ru ASC')->all();
						foreach ($pti as $item) {
							$fl[$item->id] = '<a target="_blank" href="'.$item->file.'">'.$item->caption_ru.'</a>';
						}
					?>
					<?= Html::checkboxList('f', 
						ArrayHelper::map($model->f, 'id', 'id'), 
						$fl,
						[
							'item' => function ($index, $label, $name, $checked, $value) {
								$check = $checked ? ' checked="checked"' : '';
								return "<div class='col-md-2' style='min-height: 60px;'><label><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span>$label</span></label></div>";
							}
						]
					); ?>
				</div>
			</div>
			<div class="tab-pane" id="t">
				<div class="checkbox checkbox-styled">
					<?php
						$tl = [];
						$pti = PrTabTechImages::find()->orderBy('rate DESC')->all();
						foreach ($pti as $item) {
							$tl[$item->id] = Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image_small,'80x').'<br>'.$item->caption_ru;
						}
					?>
					<?= Html::checkboxList('t', 
						ArrayHelper::map($model->t, 'id', 'id'), 
						$tl,
						[
							'item' => function ($index, $label, $name, $checked, $value) {
								$check = $checked ? ' checked="checked"' : '';
								return "<div class='col-md-2' style='min-height: 200px;'><label><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span>$label</span></label></div>";
							}
						]
					); ?>
				</div>
			</div>	
			<div class="tab-pane" id="ac">
				<div class="checkbox checkbox-styled">
					<?= Html::checkboxList('ac', 
						ArrayHelper::map($model->ac, 'id', 'id'), 
						PrCategory::setTree(),
						[
							'item' => function ($index, $label, $name, $checked, $value) {
								$check = $checked ? ' checked="checked"' : '';
								return "<label><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span>$label</span></label><br />";
							}
						]
					); ?>
				</div>
			</div>
            <div class="tab-pane" id="available_in_cities">
                <div class="checkbox checkbox-styled">
                    <?= Html::checkboxList('available_in_cities',
                        $model->autoCities,
                        $model->autoCityList,
                        [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                $check = $checked ? ' checked="checked"' : '';
                                return "<label><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span>$label</span></label><br />";
                            }
                        ]
                    ); ?>
                </div>
            </div>
        </div>
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>

	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

	<?php if (!$model->isNewRecord) { ?>
		<div class="offcanvas">
			<div id="offcanvas-demo-left" class="offcanvas-pane style-primary width-10">
				<input type="hidden" id="dfid" value="0">
				<div class="offcanvas-head">
					<header>Вы уверены, что хотите удалить фотографию?</header>
					<div class="offcanvas-tools">
						<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
							<i class="md md-close"></i>
						</a>
					</div>
				</div>
				<div class="offcanvas-body">
					<?= Html::a('Удалить', 'javascript:void(0);', ['class' => 'btn btn-block ink-reaction btn-default-bright', 'data-dismiss' => 'offcanvas', 'onclick' => 'delfoto();']) ?>
				</div>
			</div>
		</div>
	<?php } ?>	

<?php ActiveForm::end(); ?>

<script>
function delfoto() {
	id = $('#dfid').val();
	$.post('/admin/pr/delfoto/', {id:id}).done(function (data) {
		data = JSON.parse(data);
		if (data.status == 1) {
			$('#foto' + id).remove();
		}
	}).fail(function() {
		console.log('Server error.');
	});
}

var last_tab_id = <?= $max_id ?>;

function addtab(elem) {
	last_tab_id++;
	$(elem).attr('style', 'visibility:hidden');
	$('#add-tab-preloader').show();
	$.post('/admin/pr/addtab/', {id:last_tab_id}).done(function (data) {
		$(elem).attr('style', 'visibility:inherit');
		$('#add-tab-preloader').hide();
		$('#tabs_block').prepend(data);
		jsReinit();
	}).fail(function() {
		$(elem).attr('style', 'visibility:inherit');
		$('#add-tab-preloader').hide();
		console.log('Server error.');
	});
}

function jsReinit() {
	$('[data-toggle="tabs"] a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});

	$('.floating-label .form-control').on('keyup change', function (e) {
		var input = $(e.currentTarget);

		if ($.trim(input.val()) !== '') {
			input.addClass('dirty').removeClass('static');
		} else {
			input.removeClass('dirty').removeClass('static');
		}
	});

	$('.floating-label .form-control').each(function () {
		var input = $(this);

		if ($.trim(input.val()) !== '') {
			input.addClass('static').addClass('dirty');
		}
	});
}
</script>

