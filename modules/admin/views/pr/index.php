<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use app\modules\admin\models\PrCategory;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\PrSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>			
		</div>
		<div class="section-body contain-xlg">

			<div class="card">

				<div class="card-head style-primary">
					<header><i class="fa fa-table"></i> <?= Html::encode($this->title) ?></header>
					<div class="tools">
						<?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
						<?php if (Yii::$app->user->can('admin')) { ?>
							<?= Html::a('<i class="fa fa-fire"></i>', [''], [
								'class' => 'btn ink-reaction btn-floating-action btn-default-light',
								'onclick'=>"
									var keys = $('#grid').yiiGridView('getSelectedRows');
									if (keys!='') {
										if (confirm('Вы уверены, что хотите удалить выбранные элементы?')) {
											$.ajax({
												type : 'POST',
												data: {keys : keys},
												success : function(data) {}
											});
										}
									}
									return false;
								",
							]) ?>
						<?php } ?>
						<?= Html::a('<i class="fa fa-file-excel-o"></i>', ['import'], ['class' => 'btn btn-floating-action btn-default-light', 'title' => 'Импорт из Excel']) ?>
						<?= Html::a('<i class="fa fa-download"></i>', ['export'], ['class' => 'btn btn-floating-action btn-default-light', 'title' => 'Экспорт в Excel']) ?>
					</div>
				</div>

				<div class="card-body">

					<div class="row">
						<div class="col-lg-12">
							<h4></h4>
						</div>
						<div class="col-lg-12">

							<?= GridView::widget([
								'dataProvider' => $dataProvider,
								'id' => 'grid',
								'layout'=>"
									<div class='dataTables_info'>{summary}</div>
									<div class='card'>
										<div class='card-body no-padding'>
											<div class='table-responsive no-margin'>{items}</div>
										</div>
									</div>
									<div class='dataTables_paginate paging_simple_numbers'>{pager}</div>
								",		
								'tableOptions' => [
									'class' => 'table table-striped no-margin table-hover table-bordered',
								],	
								'filterModel' => $searchModel,
								'columns' => [
									//['class' => 'yii\grid\SerialColumn'],
									['class' => 'yii\grid\CheckboxColumn'],
									'id',
									[
										'filter' => false,
										'format' => 'html',
										'value' => function($data) {  
											return Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$data->image,'100x'); 
										},
									],
									'title',
									'link',
									[
										'attribute' => 'category_id',
										'filter' => ArrayHelper::merge([], PrCategory::setTree()),										
										'value' => function ($model, $index, $widget) {
											return (!empty($model->cat->title))?$model->cat->title:'';
										},
									],
									'price',
									'discount',
									[
										'attribute' => 'stock',
										'filter' => [
											'0' => 'Нет',
											'1' => 'Да',
										],										
										'value' => function ($model, $index, $widget) {
											switch ($model->stock) {
												case 0:
													return 'Нет';
												break;
												
												case 1:
													return 'Да';
												break;											
											}
										},
									],
									[
										'attribute' => 'hidden',
										'filter' => [
											'0' => 'Нет',
											'1' => 'Да',
										],										
										'value' => function ($model, $index, $widget) {
											switch ($model->hidden) {
												case 0:
													return 'Нет';
												break;
												
												case 1:
													return 'Да';
												break;											
											}
										},
									],
									'sku',
									[
										'attribute' => 'new',
										'filter' => [
											'0' => 'Нет',
											'1' => 'Да',
										],										
										'value' => function ($model, $index, $widget) {
											switch ($model->new) {
												case 0:
													return 'Нет';
												break;
												
												case 1:
													return 'Да';
												break;											
											}
										},
									],
									[
										'attribute' => 'sale',
										'filter' => [
											'0' => 'Нет',
											'1' => 'Да',
										],										
										'value' => function ($model, $index, $widget) {
											switch ($model->sale) {
												case 0:
													return 'Нет';
												break;
												
												case 1:
													return 'Да';
												break;											
											}
										},
									],
									[
										'class' => 'yii\grid\ActionColumn',
										'contentOptions' => [ 
											'style' => Yii::$app->user->can('admin')?'width: 100px;':'width: 50px;'
										], 	
										'template' => Yii::$app->user->can('admin')?'{update} {delete}':'{update}',	
										'buttons' => [										
											'update' => function ($url, $model) {
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-pencil"></i></button>', $url);
											},											
											'delete' => function ($url, $model) {
												return Html::a('<button type="button" class="btn btn-icon-toggle"><i class="fa fa-trash-o"></i></button>', $url, ['data-confirm'=>'Вы уверены, что хотите удалить этот элемент?', 'data-method'=>'post', 'data-pjax'=>'0']);
											},
										]
									],
								],
							]); ?>
												
						</div>
					</div>	
				</div>

			</div>

		</div>
	</section>
</div>
