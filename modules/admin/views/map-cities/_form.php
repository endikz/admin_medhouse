<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\MapCities */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>

	<?= Html::errorSummary($model_addr, ['class' => 'alert-danger alert fade in']) ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
				<li><a href="#addr">Региональное отделение</a></li>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'caption_ru', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

						    <?= $form->field($model, 'region', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

						    <?= $form->field($model, 'cx', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

						    <?= $form->field($model, 'cy', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
						</div>
						<div class="col-sm-6">
						    <?= $form->field($model, 'r', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

						    <?= $form->field($model, 'font_size', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

						    <?= $form->field($model, 'transform', ['template' => '{input}{label}{error}{hint}'])->textInput(['rows' => 6]) ?>

						    <?= $form->field($model, 'nearest_city')->dropDownList(ArrayHelper::merge([null => 'Нет'], ArrayHelper::map($model::find()->all(), 'id', 'caption_ru'))) ?>

						    <?= $form->field($model, 'align')->dropDownList(['c' => 'По-центру', 'l' => 'Слева', 'r' => 'Справа']) ?>
						</div>
					</div>
					<div class="form-group">
					
					</div>
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'caption_ru_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('caption_ru').' '.$v) ?>
							</div>		
						</div>
					</div>
				</div>
			<?php } ?>
			<div class="tab-pane" id="addr">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<?= Html::activeTextInput($model_addr, 'address_ru', ['class' => 'form-control']) ?>
								<?= Html::activeLabel($model_addr, 'address_ru', ['class' => 'control-label']) ?>
								<?= Html::error($model_addr, 'address_ru', ['class' => 'help-block']) ?>
							</div>

							<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
								<div class="form-group">
									<?= Html::activeTextInput($model_addr, 'address_ru_'.$k, ['class' => 'form-control']) ?>
									<?= Html::label($model_addr->getAttributeLabel('address_ru').' '.$v, ['class' => 'control-label']) ?>
									<?= Html::error($model_addr, 'address_ru_'.$k, ['class' => 'help-block']) ?>
								</div>
							<?php } ?>

							<div class="form-group">
								<?= Html::activeTextInput($model_addr, 'tel', ['class' => 'form-control']) ?>
								<?= Html::activeLabel($model_addr, 'tel', ['class' => 'control-label']) ?>
								<?= Html::error($model_addr, 'tel', ['class' => 'help-block']) ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= Html::activeTextInput($model_addr, 'map_x', ['class' => 'form-control']) ?>
								<?= Html::activeLabel($model_addr, 'map_x', ['class' => 'control-label']) ?>
								<?= Html::error($model_addr, 'map_x', ['class' => 'help-block']) ?>
							</div>

							<div class="form-group">
								<?= Html::activeTextInput($model_addr, 'map_y', ['class' => 'form-control']) ?>
								<?= Html::activeLabel($model_addr, 'map_y', ['class' => 'control-label']) ?>
								<?= Html::error($model_addr, 'map_y', ['class' => 'help-block']) ?>
							</div>

							<div class="form-group">
								<?= Html::activeTextInput($model_addr, 'map_zoom', ['class' => 'form-control']) ?>
								<?= Html::activeLabel($model_addr, 'map_zoom', ['class' => 'control-label']) ?>
								<?= Html::error($model_addr, 'map_zoom', ['class' => 'help-block']) ?>
							</div>
						</div>			
					</div>
				</div>
			</div>			
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn btn-flat btn-primary ink-reaction']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>