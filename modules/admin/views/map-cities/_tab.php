<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;
?>

<div class="col-sm-6">

	<div class="card tabs-left style-default-light">
		<ul class="card-head nav nav-tabs text-center" data-toggle="tabs">
			<li class="active"><a href="#tab1"><?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.Lang::getCurrent()->flag,'20x20', ['class' => 'img-circle']) ?></a></li>
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<li><a href="#<?= $k ?>"><?= $v ?></a></li>
			<?php } ?>
		</ul>
		<div class="card-body tab-content style-default-bright">
			<div class="tab-pane active" id="tab1">

				<div class="row">
					<div class="col-sm-10">
						<div class="form-group">
							<?= Html::textInput('tel', '', ['class' => 'form-control']) ?>
							<?= Html::label('tel', ['class' => 'control-label']) ?>
						</div>
					</div>
					<div class="col-sm-2 text-right">
						<button type="button" class="btn ink-reaction btn-floating-action btn-primary"><i class="fa fa-trash"></i></button>
					</div>
					<div class="col-sm-12">

						<?= TinyMce::widget(
							[
								'name' => 'asd',
									'fileManager' => [
									'class' => TinyMceElFinder::className(),
									'connectorRoute' => 'el-finder/connector_abs',
								],
							]
						) ?>

					</div>
				</div>

			</div>	
		</div>
	</div>

</div>	