<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;
use app\modules\admin\models\PrCategory;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\PrCategoryParams */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'pr_category_id')->dropdownList(PrCategory::setTree()) ?>

    						<?= $form->field($model, 'caption_ru', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

    						<?= $form->field($model, 'type')->dropDownList(['char' => 'Строка', 'bool' => '+/-', 'image' => 'Картинка'], ['onchange' => 'if(this.value=="image"){$("#imgb").show()}else{$("#imgb").hide()}']) ?>
						</div>
						<div class="col-sm-6">
    						<?= $form->field($model, 'rate', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
    						
    						<?= $form->field($model, 'cmp')->dropdownList([0 => 'Нет', 1 => 'Да']) ?>
    						<?= $form->field($model, 'tbl')->dropdownList([0 => 'Нет', 1 => 'Да']) ?>

    						
						</div>
					</div>

					<?php /*
					<div class="card card-bordered style-primary" id="imgb" style="<?= ($model->isNewRecord || $model->type !== 'image')?'display:none;':'' ?>">
						<div class="card-head">
							<header><i class="fa fa-fw fa-tag"></i> Изображения</header>
						</div>
						<div class="card-body style-default-bright">
							<div class="form-group">
								<div class="col-sm-6">
									<div>
										<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
										<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image_small,'80x') ?>
									
										<?= $form->field($model, 'image_small')->widget(
											ElFinderInput::className(),
											[
												'connectorRoute' => 'el-finder/connector'
											]
										) ?>
									</div>
								</div>
								<div class="col-sm-6">
									<div>
										<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
										<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image,'80x') ?>
									
										<?= $form->field($model, 'image')->widget(
											ElFinderInput::className(),
											[
												'connectorRoute' => 'el-finder/connector'
											]
										) ?>
									</div>

									<?= $form->field($model, 'image_rate', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
								</div>
							</div>
						</div>
					</div>
					*/ ?>

					
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'caption_ru_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('caption_ru').' '.$v) ?>
							</div>			
						</div>
					</div>
				</div>
			<?php } ?>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn btn-flat btn-primary ink-reaction']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>