<?php 
$this->beginContent('@app/modules/admin/views/layouts/main.php'); 

use yii\helpers\Url; 
$cont = Yii::$app->controller->id;
$act = Yii::$app->controller->action->id;

$link = (isset($_SERVER['HTTPS'])?"https":"http")."://$_SERVER[HTTP_HOST]";
$link = str_replace('admin.', '', $link);
?>

<?= $content ?>

<div id="menubar" class="menubar-inverse">
	<div class="menubar-fixed-panel">
		<div>
			<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
				<i class="fa fa-bars"></i>
			</a>
		</div>
		<div class="expanded">
			<a href="<?= $link ?>" target="_blank">
				<span class="text-lg text-bold text-primary "><?= Yii::$app->controller->coreSettings->name ?></span>
			</a>
		</div>
	</div>
	<div class="menubar-scroll-panel">

		<ul id="main-menu" class="gui-controls">

			<li>
				<a href="<?= $link ?>" target="_blank">
					<div class="gui-icon"><i class="md md-home"></i></div>
					<span class="title">На сайт</span>
				</a>
			</li>

			<li class="gui-folder">
				<a>
					<div class="gui-icon"><i class="fa fa-users"></i></div>
					<span class="title">Пользователи</span>
				</a>
				<ul>
					<li><a href="<?= Url::toRoute('/admin/default') ?>" <?= ($cont=='default' && ($act=='index' || $act=='update' || $act=='view'))?'class="active"':'' ?>><span class="title">Управление пользователями</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/default/create') ?>" <?= ($cont=='default' && $act=='create')?'class="active"':'' ?>><span class="title">Новый пользователь</span></a></li>
					<?php if (Yii::$app->user->can('admin')) { ?>
						<li class="gui-folder">
							<a href="javascript:void(0);">
								<span class="title">Роли пользователей</span>
							</a>
							<ul>
								<li><a href="<?= Url::toRoute('/admin/roles') ?>" <?= ($cont=='roles' && ($act=='index' || $act=='update' || $act=='view'))?'class="active"':'' ?>><span class="title">Управление ролями</span></a></li>
								<li><a href="<?= Url::toRoute('/admin/roles/create') ?>" <?= ($cont=='roles' && $act=='create')?'class="active"':'' ?>><span class="title">Новая роль</span></a></li>
							</ul>
						</li>
					<?php } ?>
				</ul>
			</li>
			
			<li class="gui-folder">
				<a>
					<div class="gui-icon"><i class="fa fa-flag-checkered"></i></div>
					<span class="title">Интернационализация</span>
				</a>
				<ul>
					<li><a href="<?= Url::toRoute('/admin/lang') ?>" <?= ($cont=='lang')?'class="active"':'' ?>><span class="title">Языки системы</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/message') ?>" <?= ($cont=='message')?'class="active"':'' ?>><span class="title">Переводы</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/countries') ?>" <?= ($cont=='countries')?'class="active"':'' ?>><span class="title">Страны</span></a></li>			
				</ul>
			</li>
			
			<li class="gui-folder">
				<a>
					<div class="gui-icon"><i class="md md-computer"></i></div>
					<span class="title">Страницы</span>
				</a>
				<ul>
					<li><a href="<?= Url::toRoute('/admin/blog') ?>" <?= ($cont=='blog')?'class="active"':'' ?>><span class="title">Страницы</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/blogc') ?>" <?= ($cont=='blogc')?'class="active"':'' ?>><span class="title">Категории страниц</span></a></li>
				</ul>
			</li>						

			<li class="gui-folder">
				<a>
					<div class="gui-icon"><i class="fa fa-cubes"></i></div>
					<span class="title">Модули</span>
				</a>
				<ul>
					<li><a href="<?= Url::toRoute('/admin/slider') ?>" <?= ($cont=='slider')?'class="active"':'' ?>><span class="title">Слайдер</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/menu') ?>" <?= ($cont=='menu')?'class="active"':'' ?>><span class="title">Меню</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/feedback') ?>" <?= ($cont=='feedback')?'class="active"':'' ?>><span class="title">Отзывы</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/brands') ?>" <?= ($cont=='brands')?'class="active"':'' ?>><span class="title">Бренды</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/news-and-actions') ?>" <?= ($cont=='news-and-actions')?'class="active"':'' ?>><span class="title">Акции</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/auto-city') ?>" <?= ($cont=='auto-city')?'class="active"':'' ?>><span class="title">Города и контакты</span></a></li>
				</ul>
			</li>

			<li class="expanding expanded">
				<a href="<?= Url::toRoute('/admin/map-cities') ?>" <?= ($cont=='map-cities')?'class="active"':'' ?>>
					<div class="gui-icon"><i class="fa fa-map-marker"></i></div>
					<span class="title">Карта</span>
				</a>
			</li>				

			<li class="gui-folder">
				<a>
					<div class="gui-icon"><i class="fa fa-video-camera"></i></div>
					<span class="title">Видео</span>
				</a>
				<ul>
					<li><a href="<?= Url::toRoute('/admin/videos') ?>" <?= ($cont=='videos')?'class="active"':'' ?>><span class="title">Видео</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/video-categories') ?>" <?= ($cont=='video-categories')?'class="active"':'' ?>><span class="title">Категории видео</span></a></li>
				</ul>
			</li>				

			<li class="gui-folder">
				<a>
					<div class="gui-icon"><i class="fa fa-cart-arrow-down"></i></div>
					<span class="title">Магазин</span>
				</a>
				<ul>
					<li><a href="<?= Url::toRoute('/admin/pr') ?>" <?= ($cont=='pr')?'class="active"':'' ?>><span class="title">Товары</span></a></li>

					
					<li class="gui-folder">
						<a href="javascript:void(0);">
							<span class="title">Категории товаров</span>
						</a>
						<ul>
							<li><a href="<?= Url::toRoute('/admin/pr-category') ?>" <?= ($cont=='pr-category')?'class="active"':'' ?>><span class="title"> Управление категориями</span></a></li>
							<li><a href="<?= Url::toRoute('/admin/pr-category-params') ?>" <?= ($cont=='pr-category-params')?'class="active"':'' ?>><span class="title"> Параметры категорий</span></a></li>
						</ul>
					</li>
					<li><a href="<?= Url::toRoute('/admin/pr-tab-tech-images') ?>" <?= ($cont=='pr-tab-tech-images')?'class="active"':'' ?>><span class="title">Технологии товаров</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/pr-tab-files') ?>" <?= ($cont=='pr-tab-files')?'class="active"':'' ?>><span class="title">Файлы товаров</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/pd') ?>" <?= ($cont=='pd')?'class="active"':'' ?>><span class="title">Доставка и оплата</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/promo') ?>" <?= ($cont=='promo')?'class="active"':'' ?>><span class="title">Промо коды</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/orders') ?>" <?= ($cont=='orders')?'class="active"':'' ?>><span class="title">Заказы</span></a></li>
				</ul>
			</li>			

			<li class="gui-folder">
				<a>
					<div class="gui-icon"><i class="fa fa-line-chart"></i></div>
					<span class="title">SEO</span>
				</a>
				<ul>
					<li><a href="<?= Url::toRoute('/admin/geka') ?>" <?= ($cont=='geka')?'class="active"':'' ?>><span class="title">Управление META</span></a></li>
					<li><a href="<?= Url::toRoute('/admin/geka2') ?>" <?= ($cont=='geka2')?'class="active"':'' ?>><span class="title">Управление ошибками</span></a></li>			
					<li><a href="<?= Url::toRoute('/admin/links') ?>" <?= ($cont=='links')?'class="active"':'' ?>><span class="title">Перелинковка</span></a></li>			
				</ul>
			</li>				
			
			<hr />
			<li class="expanding expanded">
				<a href="<?= Url::toRoute('/admin/settings') ?>" <?= ($cont=='settings')?'class="active"':'' ?>>
					<div class="gui-icon"><i class="fa fa-cogs"></i></div>
					<span class="title">Настройки системы</span>
				</a>
			</li>					
			
			<li class="expanding expanded">
				<a href="<?= Url::toRoute('/admin/file') ?>" <?= ($cont=='file')?'class="active"':'' ?>>
					<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
					<span class="title">Файловый менеджер</span>
				</a>
			</li>	

			<?php /*
			<li class="expanding expanded">
				<a href="<?= Url::toRoute('/admin/upload') ?>" <?= ($cont=='upload')?'class="active"':'' ?>>
					<div class="gui-icon"><i class="fa fa-upload fa-fw"></i></div>
					<span class="title">Загрузка файлов</span>
				</a>
			</li>	
			*/ ?>				

		</ul>

		<div class="menubar-foot-panel">
			<small class="no-linebreak hidden-folded">
				<span class="opacity-75"><?= Yii::$app->controller->coreSettings->copy ?> </span><strong><a href="http://webheads.com.ua/" target="_blank">WebHeads</a></strong>
			</small>
		</div>
	</div>
</div>

<div class="webheads">
	<p>Made by <a href="http://webheads.com.ua/" target="_blank">Web#Heads</a></p>
	<p>Technical support: email@webheads.com.ua or +38 (096) 07-07-000</p>
	<p><a data-toggle="offcanvas" data-backdrop="false" href="#offcanvas-contact" target="_blank">Write us</a></p>
</div>

<div class="offcanvas">
	<div id="offcanvas-contact" class="offcanvas-pane style-primary width-10">
		<div class="offcanvas-head">
			<header id="h1">Написать разработчикам</header>
			<header id="h2" style="display:none;">Ваше сообщение успешно отправлено. Мы свяжемся с вами в ближайшее время.</header>
			<div class="offcanvas-tools">
				<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
					<i class="md md-close"></i>
				</a>
			</div>
		</div>
		<div class="offcanvas-body" id="webheads-body">
			
			<div class="card">
				<div class="card-body">
					<form class="form" role="form" id="webheads-form">
						<div class="form-group floating-label">
							<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
							<textarea name="message" id="textarea2" class="form-control" rows="10" placeholder="" style="resize:none;"></textarea>
							<label for="textarea2">Текст сообщения</label>
							<div id="webheads-help" class="help-block" style="display:none;">Необходимо написать текст сообщения</div>
						</div>
						<div class="form-group floating-label">
							<button type="button" class="btn ink-reaction btn-raised btn-primary btn-block" onclick="webheads(this);">Отправить сообщение</button>
						</div>
					</form>
					<img src="/admin_assets/img/preloader.jpg" class="preloader" id="webheads-preloader" />
				</div>
			</div>

		</div>
	</div>
</div>

<?php $this->endContent(); ?>