<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Countries;
use app\modules\admin\models\Lang;
use app\modules\admin\models\PrTabTechImages;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Brands */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
				<li><a href="#t">Технологии бренда</a></li>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<div>
								<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
								<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image,'x30') ?>
							
								<?= $form->field($model, 'image')->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector'
									]
								) ?>
							</div>

							<div>
								<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
								<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image_large,'140x') ?>
							
								<?= $form->field($model, 'image_large')->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector'
									]
								) ?>
							</div>

						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'sort_order', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

						    <?= $form->field($model, 'country_id')->dropdownList(ArrayHelper::map(Countries::find()->orderBy('title_ru ASC')->all(), 'country_id', 'title_ru')) ?>

						    <?= $form->field($model, 'url', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

						    <?= $form->field($model, 'title', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
						    <?= $form->field($model, 'title2', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

						    <?= $form->field($model, 'link', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<?= $form->field($model, 'short_text')->widget(
								TinyMce::className(),
								[
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>	
						</div>	
						<div class="col-sm-6">
							<?= $form->field($model, 'text')->widget(
								TinyMce::className(),
								[						
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>
						</div>			
					</div>
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'title_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('title').' '.$v) ?>
							</div>								
							<div class="col-sm-6">
								<?= $form->field($model, 'title2_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('title2').' '.$v) ?>
							</div>						
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<?= $form->field($model, 'short_text_'.$k)->widget(
									TinyMce::className(),
									[								
										'fileManager' => [
											'class' => TinyMceElFinder::className(),
											'connectorRoute' => 'el-finder/connector_dynamic_abs',
										],
									]
								)->label($model->getAttributeLabel('short_text')) ?>	
							</div>
							<div class="col-sm-6">
								<?= $form->field($model, 'text_'.$k)->widget(
									TinyMce::className(),
									[
										'fileManager' => [
											'class' => TinyMceElFinder::className(),
											'connectorRoute' => 'el-finder/connector_dynamic_abs',
										],
									]
								)->label($model->getAttributeLabel('text')) ?>	
							</div>					
						</div>						
					</div>
				</div>
			<?php } ?>
			<div class="tab-pane" id="t">
				<div class="checkbox checkbox-styled">
				<div class="row">
					<?php
						$tl = [];
						$pti = PrTabTechImages::find()->orderBy('rate DESC')->all();
						foreach ($pti as $item) {
							$tl[$item->id] = Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image_small,'80x').'<br>'.$item->caption_ru;
						}
					?>
					<?= Html::checkboxList('t', 
						ArrayHelper::map($model->t, 'id', 'id'), 
						$tl,
						[
							'item' => function ($index, $label, $name, $checked, $value) {
								$check = $checked ? ' checked="checked"' : '';
								return "<div class='col-md-2' style='min-height: 200px;'><label><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span>$label</span></label></div>";
							}
						]
					); ?>
				</div>
				</div>
			</div>			
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn btn-flat btn-primary ink-reaction']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>

<style type="text/css">
	.checkbox-styled:not(ie8) label, .radio-styled:not(ie8) label {margin:10px 0 !important;}
</style>