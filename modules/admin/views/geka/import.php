<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use zxbodya\yii2\elfinder\ElFinderInput;
use app\components\widgets\Alert;
use yii\widgets\ActiveForm;

$this->title = 'Импорт из Excel';
$this->params['breadcrumbs'][] = ['label' => 'Админ панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'META', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
	<section>
		<div class="section-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>		
		</div>	
		<div class="section-body contain-xlg">	
		
			<div class="row">
				<div class="col-lg-12">

					<div class="card card-bordered style-primary">
						<div class="card-head style-primary">
							<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
							<div class="tools">
								<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
								<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
							</div>
						</div>
						
						<div class="card-head style-default-bright">
							<ul class="nav nav-tabs" data-toggle="tabs">
								<li class="active"><a href="#tab1">Данные</a></li>
							</ul>
						</div>

						<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'form']]); ?>

							<div class="card-body tab-content style-default-bright">
								<div class="tab-pane active" id="tab1">
									<div class="card-body floating-label">
										<div class="form-group">
											<div class="col-sm-12">
												<?= Alert::widget() ?>

												<h4>Выберите файл с данными для импорта в формате XLS или XLSX</h4>

												<?= ElFinderInput::widget([
													'name' => 'importfile',
													'connectorRoute' => 'el-finder/connector_import',
												]) ?>			
											</div>					
										</div>
									</div>
								</div>		
						
							</div>	
							
							<div class="card-actionbar style-default-bright">
								<div class="card-actionbar-row">
									<?= Html::submitButton('Импортировать данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
								</div>
							</div>
						</div>
						<em class="text-caption"><?= Html::encode($this->title) ?></em>	
				
					<?php ActiveForm::end(); ?>

				</div>
			</div>
		
		</div>
	</section>
</div>
