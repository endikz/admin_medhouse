<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\AuthItem;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use yii\jui\AutoComplete;
use yii\web\JsExpression; 
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>
	
	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>

	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php if (Yii::$app->user->can('admin')) { ?><li><a href="#tab2">Роли</a></li><?php } ?>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div>
						<?= $form->field($model, 'status', ['template' => '{input}{error}'])->radioList(
							[
								0 => 'Заблокирован', 
								1 => 'Активен', 
								2 => 'Не подтвержден'
							],
							[
								'unselect' => null,
								'item' => function ($index, $label, $name, $checked, $value) {
									$check = $checked ? ' checked="checked"' : '';
									return "<label class=\"radio-inline radio-styled\"><input type=\"radio\" name=\"$name\" value=\"$value\"$check><span>$label</span></label>";
								}
							]
						); ?>
					</div>
					<br/>

				    <input style="display:none;" type="text" name="User[fname]" />
				    <input style="display:none;" type="password" name="User[reset_password]" />

					<div class="row">
						<div class="col-sm-6">
							<div>
								<button onclick="$(this).parent().find('img').remove();$(this).parent().find('input').val('');" type="button" class="btn ink-reaction btn-icon-toggle btn-primary"><i class="fa fa-trash-o"></i></button>
								<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image,'50x50') ?>
							
								<?= $form->field($model, 'image')->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector_userpics',
									]
								) ?>
							</div>
							
							<?= $form->field($model, 'fname', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							
							<?php if ($model->isNewRecord) { ?>
								<?= $form->field($model, 'password_hash', ['template' => '{input}{label}{error}{hint}'])->passwordInput(['maxlength' => true]) ?>	
							<?php } else { ?>
								<?= $form->field($model, 'reset_password', ['template' => '{input}{label}{error}{hint}'])->passwordInput(['maxlength' => true]) ?>
							<?php } ?>							
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'email', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

							<?= $form->field($model, 'phone', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'city', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
							<?= $form->field($model, 'info')->dropdownList([0 => 'Нет', 1 => 'Да']) ?>

						</div>
					</div>
				</div>
			</div>	
			<div class="tab-pane" id="tab2">
				<div class="checkbox checkbox-styled">
					<?= Html::checkboxList('roles', 
						array_keys(ArrayHelper::map(Yii::$app->authManager->getRolesByUser($model->id), 'name', 'description')), 
						ArrayHelper::map(AuthItem::find()->orderBy('description ASC')->all(), 'name', 'description'),
						[
							'item' => function ($index, $label, $name, $checked, $value) {
								$check = $checked ? ' checked="checked"' : '';
								return "<label><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span>$label</span></label><br />";
							}
						]
					); ?>
				</div>
			</div>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>
	
<?php ActiveForm::end(); ?>