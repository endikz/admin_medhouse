<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Pr;
use yii\helpers\Url;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;
use app\modules\admin\models\PrTabTechImages;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\PrTabs */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
				<?php /*
				<?php if (!$model->isNewRecord) { ?>
					<li><a href="#f">Файлы вкладки</a></li>
				<?php } ?>
				<li><a href="#t">Технологии вкладки</a></li>
				*/ ?>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							
							<?php /*
							<?= $form->field($model, 'pr_id', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
							*/ ?>

							<?= $form->field($model, 'rate', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

    						<?= $form->field($model, 'caption_ru', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>



							<?php





$data = [1 => "First", 2 => "Second", 3 => "Third", 4 => "Fourth", 5 => "Fifth"];
echo Select2::widget([
    'name' => 'kv_lang_select1',
    'language' => 'ru',
    'data' => $data,
    'options' => ['placeholder' => 'Выберите состояние ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);
							?>


						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'text_ru')->widget(
								TinyMce::className(),
								[						
									'fileManager' => [
										'class' => TinyMceElFinder::className(),
										'connectorRoute' => 'el-finder/connector_abs',
									],
								]
							) ?>
						</div>
					</div>
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'caption_ru_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('caption_ru').' '.$v) ?>
							</div>	
							<div class="col-sm-6">
								<?= $form->field($model, 'text_ru_'.$k)->widget(
									TinyMce::className(),
									[
										'fileManager' => [
											'class' => TinyMceElFinder::className(),
											'connectorRoute' => 'el-finder/connector_abs',
										],
									]
								)->label($model->getAttributeLabel('text_ru')) ?>
							</div>				
						</div>
					</div>
				</div>
			<?php } ?>
			<?php /*
			<?php if (!$model->isNewRecord) { ?>
				<div class="tab-pane" id="f">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?php foreach ($model->f as $item) { ?>
									<div class="row">
										<div class="col-sm-2">
											<a class="btn btn-floating-action btn-default-light" href="/admin/pr-tab-files/update/<?= $item->id ?>/"><i class="fa fa-edit"></i></a>
										</div>
										<div class="col-sm-10">
											<h4><?= $item->caption_ru ?></h4>
										</div>
									</div>
									<br>
								<?php } ?>
								<?= Html::a('Редактировать все файлы', ['/admin/pr-tab-files/?PrTabFilesSearch[tab_id]='.$model->id], ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
								<?= Html::a('Добавить новый файл', ['/admin/pr-tab-files/create/?tab_id='.$model->id], ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<div class="tab-pane" id="t">
				<div class="checkbox checkbox-styled">
					<?php
						$tl = [];
						$pti = PrTabTechImages::find()->orderBy('caption_ru ASC')->all();
						foreach ($pti as $item) {
							$tl[$item->id] = Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$item->image_small,'80x').'<br>'.$item->caption_ru;
						}
					?>
					<?= Html::checkboxList('t', 
						ArrayHelper::map($model->t, 'id', 'id'), 
						$tl,
						[
							'item' => function ($index, $label, $name, $checked, $value) {
								$check = $checked ? ' checked="checked"' : '';
								return "<div class='col-md-2' style='min-height: 200px;'><label><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span>$label</span></label></div>";
							}
						]
					); ?>
				</div>
			</div>	
			*/ ?>		
		</div>
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn btn-flat btn-primary ink-reaction']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>

<style type="text/css">
	.checkbox-styled:not(ie8) label, .radio-styled:not(ie8) label {margin:10px 0 !important;}
</style>