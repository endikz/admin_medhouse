<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\ElFinderWidget;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\modules\admin\models\Lang;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

	<?= $form->errorSummary($model, ['class' => 'alert-danger alert fade in']); ?>
	
	<div class="card">
		<div class="card-head style-primary">
			<header><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></header>
			<div class="tools">
				<?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-floating-action btn-default-light']) ?>
				<?= Html::a('<i class="fa fa-reply"></i>', ['index'], ['class' => 'btn btn-floating-action btn-default-light']) ?>
			</div>
		</div>
		
		<div class="card-head">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<li class="active"><a href="#tab1">Данные</a></li>
				<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
					<li><a href="#<?= $k ?>"><?= $v ?></a></li>
				<?php } ?>
			</ul>
		</div>
		<div class="card-body tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="card-body floating-label">
					<div class="row">
						<div class="col-sm-6">
							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image,'140x') ?>
						
							<?= $form->field($model, 'image')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector_slider',
								]
							) ?>
							<p>Оптимальный размер изображения: 1140 на 346 пикселей.</p>
							<br>
							<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->image_mob,'140x') ?>
						
							<?= $form->field($model, 'image_mob')->widget(
								ElFinderInput::className(),
								[
									'connectorRoute' => 'el-finder/connector_slider',
								]
							) ?>
							<p>Оптимальный размер изображения: 540 на 400 пикселей.</p>
						</div>
						<div class="col-sm-6">
						    <?= $form->field($model, 'title', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>

						    <?= $form->field($model, 'sort_order', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>
						    
						    <?= $form->field($model, 'url', ['template' => '{input}{label}{error}{hint}'])->textInput() ?>

						    <?= $form->field($model, 'status')->dropdownList([1 => 'Доступен', 0 => 'Скрыт']) ?>

						    <?= $form->field($model, 'status_mob')->dropdownList([0 => 'Скрыт', 1 => 'Доступен']) ?>
						</div>
					</div>
					<div class="form-group">
					
					</div>
				</div>
			</div>	
			<?php foreach (Lang::getBehaviorsList() as $k => $v) { ?>
				<div class="tab-pane" id="<?= $k ?>">
					<div class="card-body floating-label">
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'title_'.$k, ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true])->label($model->getAttributeLabel('title').' '.$v) ?>
							</div>	
							<div class="col-sm-6">
								<?php $n = 'image_'.$k; ?>
								<?= Yii::$app->imageCache->img($_SERVER['DOCUMENT_ROOT'].'/web'.$model->$n,'140x') ?>
							
								<?= $form->field($model, 'image_'.$k)->widget(
									ElFinderInput::className(),
									[
										'connectorRoute' => 'el-finder/connector_slider',
									]
								)->label($model->getAttributeLabel('image').' '.$v) ?>
							</div>													
						</div>
					</div>
				</div>
			<?php } ?>
		</div>	
		
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				<?= Html::submitButton($model->isNewRecord ? 'Добавить запись' : 'Сохранить данные', ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
			</div>
		</div>
	</div>
	<em class="text-caption"><?= Html::encode($this->title) ?></em>	

<?php ActiveForm::end(); ?>