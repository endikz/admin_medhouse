<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Pr;
use app\modules\admin\models\Pd;
use app\modules\admin\models\Cart;
use app\modules\admin\models\Orders;
use app\modules\admin\models\OrdersSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\helpers\VarDumper;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete', 'autocomplete'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                ],
            ],      
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
            $keys = (isset($_POST['keys']))?$_POST['keys']:[];
            if (count($keys)) {
                foreach ($keys as $k => $v) {
                    if (($model = Orders::findOne($v)) !== null) {
                        $model->delete();
                    }
                }
                return $this->redirect(['index']);
            }
        }

        $payment = [];
        $delivery = [];
        $pd = Pd::find()->where(['status' => 1])->all();
        foreach ($pd as $item) {
            if ($item->type == 0) {
                $payment[$item->id] = $item;
            } else {
                $delivery[$item->id] = $item;
            }
        }        
        $prc_id = Cart::find()
            ->select(['id','product_id', 'title', 'order_id', 'quant'])
            ->where(['NOT', ['product_id' => NULL]])
            ->orderBy('title ASC')
            ->groupBy('product_id')
            ->all();
        // $searchModel = new OrdersSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel = new OrdersSearch();
        $searchData = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $searchData['dataProvider'];
        $total = $searchData['total'];

        // $tc = Yii::$app->request->queryParams;
        // $tc['page'] = 100000;
        // $tc['per-page'] = 1000000;
        // $dataProviderTotal = $searchModel->search($tc);
        // $model = $dataProviderTotal->getModels();
        // $total = 0;
        $totalprsum = 0;
        $totalprquant = 0;
        // foreach ($model as $item) {
        //     foreach ($item->cart as $item2) {
        //         $total += $item2->price * $item2->quant;
        //         if (isset($searchModel->prc_id) && $item2->product_id == $searchModel->prc_id) {
        //            $totalprsum += $item2->price * $item2->quant;
        //            $totalprquant += $item2->quant;
        //         }
        //     }
        // }



        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'payment' => $payment,
            'delivery' => $delivery,
            'total' => $total,
            'prc_id' => $prc_id,
            'totalprsum' => $totalprsum,
            'totalprquant' => $totalprquant
        ]);
    }

    public function actionCreate()
    {
        $total = 0;
        $model = new Orders();
        $model->scenario = 'create';
        $model->status = 0;
        $model->created_at = time();
        $model->updated_at = time();
        $model->del = 0;
        $model->val_validate = 0;

        if (isset($_POST['Orders'])) {
            $quant_arr = [];
            $model->load(Yii::$app->request->post());
            $vals = isset($_POST['value'])?$_POST['value']:[];
            $quant = isset($_POST['quant'])?$_POST['quant']:[];
            $vals = is_array($vals)?$vals:[];
            $quant = is_array($quant)?$quant:[];
            foreach ($quant as $k => $v) {
                foreach ($v as $k2 => $v2) {
                    if (isset($quant_arr[$k2])) {
                        $quant_arr[$k2] += $v2;
                    } else {
                        $quant_arr[$k2] = $v2;
                    }
                }
            }
            $vals = array_unique($vals);

            $pr_model = Pr::find()->where(['id' => $vals])->all();
            if ($pr_model) {
                foreach ($pr_model as $item) {
                    $qq = isset($quant_arr[$item->id])?$quant_arr[$item->id]:1;
                    if (!empty($item->price2)) {
                        $total += $item->price * $qq;
                    } else {
                        $total += $item->price * $qq;
                    }
                }
                $model->val_validate = 1;
            }

            $model_delivery = Pd::find()->where(['status' => 1, 'type' => 1, 'id' => $model->delivery_id])->one();
            
            if ($model_delivery) {
                $price_from = intval($model_delivery->price_from);
                $price_prs = intval($model_delivery->price_prs);

                if ($total < $price_from) {
                    if ($model_delivery->price_type == 0) {
                        $model->del = round($total / 100 * $price_prs, 2);
                    } else {
                        $model->del = round($price_prs, 2);
                    }                    
                }
            }

            if ($model->save()) {

                foreach ($pr_model as $item) {
                    $cart_model = new Cart();
                    $cart_model->product_id = $item->id;
                    $cart_model->title = $item->title;
                    $cart_model->link = $item->link;
                    $cart_model->image = $item->image;
                    if (!empty($item->price2)) {
                        $cart_model->price = $item->price2;
                    } else {
                        $cart_model->price = $item->price;
                    }
                    $qq = isset($quant_arr[$item->id])?$quant_arr[$item->id]:1;
                    $cart_model->quant = $qq;
                    $cart_model->created_at = time();
                    $cart_model->order_id = $model->id;
                    $cart_model->product_id = $item->id;
                    $cart_model->save(false);
                }
                return $this->redirect(['index']);
            }
        }

        $payment = [];
        $delivery = [];
        $pd = Pd::find()->where(['status' => 1])->all();
        foreach ($pd as $item) {
            if ($item->type == 0) {
                $payment[$item->id] = $item;
            } else {
                $delivery[$item->id] = $item;
            }
        }  

        return $this->render('create', [
            'model' => $model,
            'payment' => $payment,
            'delivery' => $delivery,
        ]);
    }

    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $old_status = $model->status;

        $total = 0;
        $model->val_validate = 0;


        // VarDumper::dump($_POST,10,1);

        if (isset($_POST['Orders'])) {
            $quant_arr = [];
            $model->load(Yii::$app->request->post());
            $vals = isset($_POST['value'])?$_POST['value']:[];
            $quant = isset($_POST['quant'])?$_POST['quant']:[];
            $vals = is_array($vals)?$vals:[];
            $quant = is_array($quant)?$quant:[];
            foreach ($quant as $k => $v) {
                foreach ($v as $k2 => $v2) {
                    if (isset($quant_arr[$k2])) {
                        $quant_arr[$k2] += $v2;
                    } else {
                        $quant_arr[$k2] = $v2;
                    }
                }
            }
            $vals = array_unique($vals);

            $pr_model = Pr::find()->where(['id' => $vals])->all();
            
            if ($pr_model) {
                foreach ($pr_model as $item) {
                    if (!empty($item->price2)) {
                        $total += $item->price;
                    } else {
                        $total += $item->price;
                    }
                }
                $model->val_validate = 1;
            }

            $model_delivery = Pd::find()->where(['status' => 1, 'type' => 1, 'id' => $model->delivery_id])->one();

            if ($model_delivery) {
                $price_from = intval($model_delivery->price_from);
                $price_prs = intval($model_delivery->price_prs);

                if ($total < $price_from) {
                    if ($model_delivery->price_type == 0) {
                        $model->del = round($total / 100 * $price_prs, 2);
                    } else {
                        $model->del = round($price_prs, 2);
                    }                    
                }
            }
        }




        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            
            //if ($model->save()) {
                Cart::deleteAll(['order_id' => $model->id]);
                foreach ($pr_model as $item) {
                    $cart_model = new Cart();
                    $cart_model->product_id = $item->id;
                    $cart_model->title = $item->title;
                    $cart_model->link = $item->link;
                    $cart_model->image = $item->image;
                    if (!empty($item->price2)) {
                        $cart_model->price = $item->price2;
                    } else {
                        $cart_model->price = $item->price;
                    }
                    $qq = isset($quant_arr[$item->id])?$quant_arr[$item->id]:1;
                    $cart_model->quant = $qq;
                    $cart_model->created_at = time();
                    $cart_model->order_id = $model->id;
                    $cart_model->product_id = $item->id;
                    $cart_model->save(false);
                }
               // return $this->redirect(['index']);
            //}


            Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');


            /*
            if ($old_status !== $model->status && $model->status == 1) {
                if (!empty(Yii::$app->controller->coreSettings['email_tpl'])) {

                    // client email
                    $title = 'Comanda de pe magazinul online dr-house.md a fost finalizată cu succes';   
                    $text = '<p>Comanda Dvs. de pe magazinul online dr-house.md este confirmată. </p>';               

                    $msg_text = str_replace('{title}', $title, Yii::$app->controller->coreSettings['email_tpl']);
                    $msg_text = str_replace('{text}', $text, $msg_text);
                    $msg_text = str_replace('{http_host}', $_SERVER['HTTP_HOST'], $msg_text);
                    $msg_text = str_replace('{burl}', Yii::$app->controller->coreSettings['burl'], $msg_text);
                    $msg_text = str_replace('{bottom}', '', $msg_text);

                    $mail = Yii::$app->mailer->compose()
                        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->controller->coreSettings['name']])
                        ->setTo($model->email)
                        ->setSubject($title)
                        ->setHtmlBody($msg_text)
                        ->send();
                }
            }
            */

            return $this->redirect(['update', 'id' => $model->id]);
        } else {

            $payment = [];
            $delivery = [];
            $pd = Pd::find()->where(['status' => 1])->all();
            foreach ($pd as $item) {
                if ($item->type == 0) {
                    $payment[$item->id] = $item;
                } else {
                    $delivery[$item->id] = $item;
                }
            }  

            return $this->render('update', [
                'model' => $model,
                'payment' => $payment,
                'delivery' => $delivery,
            ]);
        }
    }

    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->removed = 1;
        $model->save(false);

        return $this->redirect(['index']);
    }

    public function actionAutocomplete()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [];
            Yii::$app->response->format = Response::FORMAT_JSON;

            $term = isset($_GET['term'])?strip_tags($_GET['term']):'';
            foreach (Pr::find()->asArray()->limit(10)->where(['hidden' => 0])->andWhere(['or', ['like', 'title', $term], ['like', 'title2', $term]])->all() as $k => $v) {
                $answer[] = [
                    //'label' => $title,
                    'value' => $v['title2'].' '.$v['title'],
                    'id' => $v['id']
                ];
            }
            
            return $answer;
        }
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
