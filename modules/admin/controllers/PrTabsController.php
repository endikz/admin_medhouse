<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\PrTabTechImages;
use app\modules\admin\models\Pr;
use app\modules\admin\models\PrTabs;
use app\modules\admin\models\PrTabsSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * PrTabsController implements the CRUD actions for PrTabs model.
 */
class PrTabsController extends Controller
{
	public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'load-pr'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                        'actions' => ['delete', 'delete-group'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PrTabs models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->request->isAjax) {
			$keys = (isset($_POST['keys']))?$_POST['keys']:[];
			if (count($keys)) {
				foreach ($keys as $k => $v) {
					if (($model = PrTabs::findOne($v)) !== null) {
						$model->delete();
					}
				}
				return $this->redirect(['index']);
			}
		}
		
        $searchModel = new PrTabsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new PrTabs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PrTabs();
        $model->rate = 0;
        if (isset($_GET['pr_id'])) {
            $model->pr_id = intval($_GET['pr_id']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            /*
            if (isset($_POST['t'])) {
                foreach (Yii::$app->request->post('t') as $k => $v) {
                    $t = PrTabTechImages::findOne($v);
                    if ($t) {
                        $model->link('t', $t);
                    }
                }
            }
            */

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PrTabs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            /*
            if (isset($_POST['t'])) {
                $model->unlinkAll('t', true);
                foreach (Yii::$app->request->post('t') as $k => $v) {
                    $t = PrTabTechImages::findOne($v);
                    if ($t) {
                        $model->link('t', $t);
                    }
                }
            }
            */

			Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id, $ml = true)
    {
        if ($ml) {
            $model = PrTabs::find()->where('id = :id', [':id' => $id])->multilingual()->one();
        } else {
            $model = PrTabs::findOne($id);
        }
        
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }        
}
