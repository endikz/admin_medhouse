<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\components\Controller;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Json;

class UploadController extends Controller
{
	public $layout = "./sidebar";
	
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'file-upload'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                ],
            ],		
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFileUpload()
    {
        $uploadPath = '/files/images/upload/';

        $files = UploadedFile::getInstancesByName('file');
        $cnt = 0;
        foreach ($files as $file) {
            $fname = rand(1000, 9999).'_'.time();
            $fpath = $_SERVER['DOCUMENT_ROOT'].'/web'.$uploadPath.$fname.'.'.$file->extension;
            if ($file->saveAs($fpath)) {
                $answer = [];
                $answer['out'] = ['file_id' => $cnt];
                echo Json::encode($answer);  
            }
            $cnt++;           
        }
    }    
}
