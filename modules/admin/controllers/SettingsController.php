<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Settings;
use app\modules\admin\components\Controller;
use yii\filters\AccessControl;

class SettingsController extends Controller
{
	public $layout = "./sidebar";
	
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'email'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
		$model = Settings::find()->where('id = 1')->multilingual()->one();
		
		if ($model === null) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
		}
		
        return $this->render('index', [
			'model' => $model
		]);
    }

    public function actionEmail()
    {
        $model = Settings::find()->where('id = 1')->multilingual()->one();
        
        if ($model === null) {
            echo 'HTML письмо не найдено';
        } else {
            $tpl = str_replace('{http_host}', $_SERVER['HTTP_HOST'], $model->email_tpl);
            echo str_replace('{burl}', $model->burl, $tpl);
        }
    }}
