<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\components\Controller;
use yii\filters\AccessControl;
use app\modules\admin\models\PrValue;
use app\modules\admin\models\PrAttr;
use app\modules\admin\models\PrEnt;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class FiltersController extends Controller
{
	public $layout = "./sidebar";
	
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'del', 'delval'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                ],
            ]
        ];
    }

    public function actionDel()
    {
        if (Yii::$app->request->isAjax) {
            $id = (isset($_POST['id']))?intval($_POST['id']):0;
            PrAttr::deleteAll(['id' => $id]);
            echo 'success';
        }
    }    

    public function actionDelval()
    {
        if (Yii::$app->request->isAjax) {
            $id = (isset($_POST['id']))?intval($_POST['id']):0;
            PrValue::deleteAll(['id' => $id]);
            echo 'success';
        }
    }

    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
            if (isset($_POST['add'])) {
                $title = (isset($_POST['title']))?strip_tags($_POST['title']):'';
                $sort_order = (isset($_POST['sort_order']))?intval($_POST['sort_order']):0;
                $prattr = new PrAttr();
                $prattr->title = $title;
                $prattr->sort_order = $sort_order;
                $prattr->save(false);

                $tpl_val = '
                    <tr>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td class="text-right">
                            <button onclick="delVal(1, this);" type="button" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Удалить значение"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                ';

                $tpl_val_table = '
                    <table class="table no-margin table-hover val-table">
                        <thead>
                            <tr>
                                <th>Значение</th>
                                <th>Сортировка</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="val-for-filter'.$prattr->id.'">

                        </tbody>
                        <tfoot>
                            <tr class="danger">
                                <td>
                                    <div class="form-group">
                                        '.Html::textInput('value', '', ['class' => 'form-control dirty', 'id' => 'value'.$prattr->id]).'
                                        <label for="value'.$prattr->id.'">Значение фильтра</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        '.Html::textInput('sort_order', 0, ['class' => 'form-control dirty', 'id' => 'sort_order'.$prattr->id, 'type' => 'number', 'min' => 0, 'max' => 1000]).'
                                        <label for="sort_order'.$prattr->id.'">Сортировка</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        '.Html::button('Добавить значение', ['onclick' => 'addVal('.$prattr->id.');', 'class' => 'btn btn-flat btn-primary ink-reaction']).'
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                '; 

                $tpl = '
                    <tr class="danger">
                        <td>'.$title.'</td>
                        <td>'.$sort_order.'</td>
                        <td class="text-right">
                            <button onclick="del('.$prattr->id.', this);" type="button" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Удалить фильтр"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <tr id="table-val-for-filter'.$prattr->id.'">
                        <td></td>
                        <td></td>
                        <td>'.$tpl_val_table.'</td>
                    </tr>
                ';

                echo $tpl;
            } else if (isset($_POST['add-val'])) {
                $value = (isset($_POST['value']))?strip_tags($_POST['value']):'';
                $sort_order = (isset($_POST['sort_order']))?intval($_POST['sort_order']):0;
                $id = (isset($_POST['id']))?intval($_POST['id']):0;
                $prattr = PrAttr::find()->where(['id' => $id])->one();
                if ($prattr) {
                    $pv = new PrValue();
                    $pv->attr_id = $id;
                    $pv->value = $value;
                    $pv->sort_order = $sort_order;
                    $pv->save(false);

                    $tpl_val = '
                        <tr>
                            <td>'.$value.'</td>
                            <td>'.$sort_order.'</td>
                            <td class="text-right">
                                <button onclick="delVal('.$pv->id.', this);" type="button" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Удалить значение"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>
                    ';

                    echo $tpl_val;
                }
            }
        } else {
            $model = PrAttr::find()->orderBy('id ASC')->all();

            return $this->render('index', [
                'model' => $model
            ]);
        }
    }
}
