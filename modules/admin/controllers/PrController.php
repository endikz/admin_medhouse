<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\ProductCity;
use Yii;
use app\modules\admin\models\PrTabTechImages;
use app\modules\admin\models\PrTabFiles;
use app\modules\admin\models\PrAttr;
use app\modules\admin\models\PrEnt;
use app\modules\admin\models\PrValue;
use app\modules\admin\models\PrCategory;
use app\modules\admin\models\PrCategoryParams;
use app\modules\admin\models\PrCategoryParamImages;
use app\modules\admin\models\PrParams;
use app\modules\admin\models\Galery;
use app\modules\admin\models\Pr;
use app\modules\admin\models\PrTabs;
use app\modules\admin\models\PrSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use app\modules\admin\models\Lang;
use app\components\Translit;

/**
 * PrController implements the CRUD actions for Pr model.
 */
class PrController extends Controller
{
	public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'addtab'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                        'actions' => ['delete', 'delfoto', 'export', 'import', 'rem'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDelfoto()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [];
            $answer['status'] = 0;
            $id = (isset($_POST['id']))?intval($_POST['id']):0;
            $gal = Galery::findOne($id);
            if ($gal) {
                if (!empty($gal->image) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$gal->image)) {
                    unlink($_SERVER['DOCUMENT_ROOT'].'/web'.$gal->image);
                }
                $gal->delete();
                $answer['status'] = 1;
            }

            echo json_encode($answer);
        }
    }     

    public function actionAddtab()
    {
        if (Yii::$app->request->isAjax) {
            $id = isset($_POST['id'])?intval($_POST['id']):0;
            $id++;
            $item = new PrTabs();
            $item->id = $id;
            echo $this->renderAjax('_tab', ['item' => $item]);
        }
    }

    /**
     * Lists all Pr models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->request->isAjax) {
			$keys = (isset($_POST['keys']))?$_POST['keys']:[];
			if (count($keys) && Yii::$app->user->can('admin')) {
				foreach ($keys as $k => $v) {
					if (($model = Pr::findOne($v)) !== null) {
						$model->delete();
					}
				}
				return $this->redirect(['index']);
			}
		}
		
        $searchModel = new PrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Pr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pr();
        $model->discount = 0;
        $model->sort_order = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (isset($_POST['t'])) {
                foreach (Yii::$app->request->post('t') as $k => $v) {
                    $t = PrTabTechImages::findOne($v);
                    if ($t) {
                        $model->link('t', $t);
                    }
                }
            }

            if (isset($_POST['f'])) {
                foreach (Yii::$app->request->post('f') as $k => $v) {
                    $t = PrTabFiles::findOne($v);
                    if ($t) {
                        $model->link('f', $t);
                    }
                }
            }

            if (isset($_POST['ac'])) {
                foreach (Yii::$app->request->post('ac') as $k => $v) {
                    $t = PrCategory::findOne($v);
                    if ($t) {
                        $model->link('ac', $t);
                    }
                }
            }

            $availableInCity = Yii::$app->request->post('available_in_cities');

            if(isset($availableInCity))
            {
                foreach ($availableInCity as $item) {
                    $productCities = ProductCity::find()->where([
                        'pr_id' => $model->id,
                        'auto_city_id'=> $item
                    ])->one();

                    if(!$productCities)
                    {
                        $prCity = new ProductCity;
                        $prCity->pr_id = $model->id;
                        $prCity->auto_city_id = $item;
                        $prCity->save();
                    }
                }

                foreach ($model->autoCities as $autoCity) {
                    if(!in_array($autoCity, $availableInCity))
                    {
                        $pr = ProductCity::find()->where(['auto_city_id' => $autoCity, 'pr_id' => $model->id])->one();
                        if($pr)
                            $pr->delete();
                    }
                }
            }else{
                ProductCity::deleteAll(['pr_id' => $model->id]);
            }

            if (isset($_POST['fil'])) {
                foreach (Yii::$app->request->post('fil') as $k => $v) {
                    $pv = PrValue::find()->where(['id' => $v])->one();
                    if ($pv) {
                        $pe = PrEnt::find()->where(['pr_id' => $model->id, 'value_id' => $v])->one();
                        if ($pe === null) {
                            $pe = new PrEnt();
                            $pe->pr_id = $model->id;
                            $pe->attr_id = $pv->attr_id;
                            $pe->value_id = $pv->id;
                            $pe->save(false);
                        }
                    }
                }
            }

            $model->upload = UploadedFile::getInstances($model, 'upload');
            foreach ($model->upload as $file) {
                $fname = rand(1000, 9999).'_'.time();
                $attachment_name = $_SERVER['DOCUMENT_ROOT'].'/web/files/images/products/goods/'.$fname.'.'.$file->extension;
                if ($file->saveAs($attachment_name)) {
                    $g = new Galery();
                    $g->image = '/files/images/products/goods/'.$fname.'.'.$file->extension;
                    $g->item_type = 1;
                    $g->sort_order = 0;
                    $g->item_id = $model->id;
                    $g->item_name = $file->name;
                    $g->save();
                }
            }

            if (!empty($_POST['Pr[upload2]']) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$_POST['Pr[upload2]'])) {
                $parts = explode('/', $_POST['Pr[upload2]']);
                $fname = (isset($parts[count($parts) - 1]))?$parts[count($parts) - 1]:$parts[0];
                $g = new Galery();
                $g->image = $_POST['Pr[upload2]'];
                $g->item_type = 1;
                $g->sort_order = 0;
                $g->item_id = $model->id;
                $g->item_name = $fname;
                $g->save();                
            }  

            if (isset($_POST['tab'])) {
                $lc = Lang::getCurrent();
                foreach ($_POST['tab'] as $k => $v) {

                    $pr_tabs_model = new PrTabs();
                    $pr_tabs_model->pr_id = $model->id;

                    if (isset($v[$lc->url])) {
                        $pr_tabs_model->caption_ru = isset($v[$lc->url]['title'])?$v[$lc->url]['title']:'';
                        $pr_tabs_model->text_ru = isset($v[$lc->url]['text'])?$v[$lc->url]['text']:'';
                    }

                    foreach (Lang::getBehaviorsList() as $kl => $vl) { 
                        if (isset($v[$kl])) {
                            $caption_name = 'caption_ru_'.$kl;
                            $text_name = 'text_ru_'.$kl;
                            $pr_tabs_model->$caption_name = isset($v[$kl]['title'])?$v[$kl]['title']:'';
                            $pr_tabs_model->$text_name = isset($v[$kl]['text'])?$v[$kl]['text']:'';
                        }
                    }

                    $pr_tabs_model->save(false);
                }
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        PrCategory::setTree();
        $ftids = [];
        foreach (PrCategory::getFullTree($model->category_id) as $k => $v) {
            $ftids[] = $v['id'];
        }
        $pcp = PrCategoryParams::find()->where(['pr_category_id' => $ftids])->orderBy('rate DESC')->all();        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            foreach ($pcp as $item) {
                if (isset($_POST['value'.$item->id])) {
                    $pp = PrParams::find()->where(['param_id' => $item->id, 'pr_id' => $id])->one();
                    if ($pp) {
                        if ($item->type == 'char') {
                            $pp->value_char = $_POST['value'.$item->id];
                        } else if ($item->type == 'bool') {
                            $pp->value_bool = $_POST['value'.$item->id];
                        } else if ($item->type == 'image') {
                            $pci = PrCategoryParamImages::find()->where(['id' => $pp->value_image])->one();
                            if ($pci) {
                                $pci->image_small = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                                $pci->image = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                                $pci->save(false);
                            } else {
                                $pci = new PrCategoryParamImages();
                                $pci->image_small = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                                $pci->image = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                                $pci->rate = 0;
                                $pci->external_parent = $item->id;
                                $pci->save(false);
                                $pp->value_image = $pci->id;                                
                            }
                        }
                    } else {
                        $pp = new PrParams();
                        $pp->pr_id = $id;
                        $pp->param_id = $item->id;
                        $pp->value_char = '';
                        $pp->value_bool = 0;
                        $pp->value_image = null;
                        if ($item->type == 'char') {
                            $pp->value_char = $_POST['value'.$item->id];
                        } else if ($item->type == 'bool') {
                            $pp->value_bool = $_POST['value'.$item->id];
                        } else if ($item->type == 'image') {
                            $pci = new PrCategoryParamImages();
                            $pci->image_small = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                            $pci->image = isset($_POST['value'.$item->id][0])?$_POST['value'.$item->id][0]:'';
                            $pci->rate = 0;
                            $pci->external_parent = $item->id;
                            $pci->save(false);
                            $pp->value_image = $pci->id;
                        }
                    }
                    $pp->save(false);
                }
            }

            if (isset($_POST['fil'])) {
                PrEnt::deleteAll(['pr_id' => $id]);
                foreach (Yii::$app->request->post('fil') as $k => $v) {
                    $pv = PrValue::find()->where(['id' => $v])->one();
                    if ($pv) {
                        $pe = PrEnt::find()->where(['pr_id' => $id, 'value_id' => $v])->one();
                        if ($pe === null) {
                            $pe = new PrEnt();
                            $pe->pr_id = $id;
                            $pe->attr_id = $pv->attr_id;
                            $pe->value_id = $pv->id;
                            $pe->save(false);
                        }
                    }
                }
            }

            $model->unlinkAll('t', true);
            if (isset($_POST['t'])) {
                foreach (Yii::$app->request->post('t') as $k => $v) {
                    $t = PrTabTechImages::findOne($v);
                    if ($t) {
                        $model->link('t', $t);
                    }
                }
            }

            $model->unlinkAll('f', true);
            if (isset($_POST['f'])) {
                foreach (Yii::$app->request->post('f') as $k => $v) {
                    $t = PrTabFiles::findOne($v);
                    if ($t) {
                        $model->link('f', $t);
                    }
                }
            }

            $model->unlinkAll('ac', true);
            if (isset($_POST['ac'])) {
                foreach (Yii::$app->request->post('ac') as $k => $v) {
                    $t = PrCategory::findOne($v);
                    if ($t) {
                        $model->link('ac', $t);
                    }
                }
            }            

            if (isset($_POST['tab'])) {
                $lc = Lang::getCurrent();
                $new_tabs_ids = [];
                foreach ($_POST['tab'] as $k => $v) {

                    $pr_tabs_model = PrTabs::find()->where(['id' => $k, 'pr_id' => $id])->multilingual()->one();
                    if (null === $pr_tabs_model) {
                        $pr_tabs_model = new PrTabs(); 
                        $pr_tabs_model->pr_id = $id;
                    }

                    if (isset($v[$lc->url])) {
                        $pr_tabs_model->caption_ru = isset($v[$lc->url]['title'])?$v[$lc->url]['title']:'';
                        $pr_tabs_model->text_ru = isset($v[$lc->url]['text'])?$v[$lc->url]['text']:'';
                    }

                    foreach (Lang::getBehaviorsList() as $kl => $vl) { 
                        if (isset($v[$kl])) {
                            $caption_name = 'caption_ru_'.$kl;
                            $text_name = 'text_ru_'.$kl;
                            $pr_tabs_model->$caption_name = isset($v[$kl]['title'])?$v[$kl]['title']:'';
                            $pr_tabs_model->$text_name = isset($v[$kl]['text'])?$v[$kl]['text']:'';
                        }
                    }

                    $pr_tabs_model->save(false);
                    $new_tabs_ids[] = $pr_tabs_model->id;
                }

                PrTabs::deleteAll(['and', ['not in', 'id', $new_tabs_ids], ['pr_id' => $id]]);
            } else {
                PrTabs::deleteAll(['pr_id' => $id]);
            }

            $availableInCity = Yii::$app->request->post('available_in_cities');

            if(isset($availableInCity))
            {
                foreach ($availableInCity as $item) {
                    $productCities = ProductCity::find()->where([
                        'pr_id' => $id,
                        'auto_city_id'=> $item
                    ])->one();

                    if(!$productCities)
                    {
                        $prCity = new ProductCity;
                        $prCity->pr_id = $id;
                        $prCity->auto_city_id = $item;
                        $prCity->save();
                    }
                }

                foreach ($model->autoCities as $autoCity) {
                    if(!in_array($autoCity, $availableInCity))
                    {
                        $pr = ProductCity::find()->where(['auto_city_id' => $autoCity, 'pr_id' => $model->id])->one();
                        if($pr)
                            $pr->delete();
                    }
                }
            }else{
                ProductCity::deleteAll(['pr_id' => $model->id]);
            }

            $model->upload = UploadedFile::getInstances($model, 'upload');
            foreach ($model->upload as $file) {
                $fname = rand(1000, 9999).'_'.time();
                $attachment_name = $_SERVER['DOCUMENT_ROOT'].'/web/files/images/products/goods/'.$fname.'.'.$file->extension;
                if ($file->saveAs($attachment_name)) {
                    $g = new Galery();
                    $g->image = '/files/images/products/goods/'.$fname.'.'.$file->extension;
                    $g->item_type = 1;
                    $g->sort_order = 0;
                    $g->item_id = $model->id;
                    $g->item_name = $file->name;
                    $g->save();
                }
            }

            if (!empty($_POST['Pr']['upload2']) && file_exists($_SERVER['DOCUMENT_ROOT'].'/web'.$_POST['Pr']['upload2'])) {
                $parts = explode('/', $_POST['Pr']['upload2']);
                $fname = (isset($parts[count($parts) - 1]))?$parts[count($parts) - 1]:$parts[0];
                $g = new Galery();
                $g->image = $_POST['Pr']['upload2'];
                $g->item_type = 1;
                $g->sort_order = 0;
                $g->item_id = $model->id;
                $g->item_name = $fname;
                $g->save();                
            }

			Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $pv = [];
            foreach ($model->paramsValues as $item) {
                $pv[$item->param_id] = $item;
            }

            return $this->render('update', [
                'model' => $model,
                'pv' => $pv,
                'pcp' => $pcp,
            ]);
        }
    }

    /**
     * Deletes an existing Pr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id, $ml = true)
    {
        if ($ml) {
            $model = Pr::find()->where('id = :id', [':id' => $id])->multilingual()->one();
        } else {
            $model = Pr::findOne($id);
        }
        
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionExport()
    {
        $filename = 'export-'.date('d-m-Y-His').'.xlsx';
        $fname_new = $_SERVER['DOCUMENT_ROOT'].'/web/files/import/'.$filename;
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);

        $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', 'Название')
            ->setCellValue('B1', 'Модель')
            ->setCellValue('C1', 'Артикул')
            ->setCellValue('D1', 'Категория, ID')
            ->setCellValue('E1', 'Категория (поле не учавствует в импорте)')
            ->setCellValue('F1', 'Цена')
            ->setCellValue('G1', 'Акционная цена')
            ->setCellValue('H1', 'Наличие')
            ->setCellValue('I1', 'Новинка')
            ->setCellValue('J1', 'Распродажа')
            ->setCellValue('K1', 'Лидер продаж')
            ->setCellValue('L1', 'Скрывать на сайте')
            ->setCellValue('M1', 'Сортировка')
            ->setCellValue('N1', 'ID');

        $objPHPExcel->getActiveSheet()->setTitle('Товары');
        $i = 3;
        foreach (Pr::find()->all() as $item) {
            $objPHPExcel->getActiveSheet()
                ->setCellValue('A'.$i, $item->title)
                ->setCellValue('B'.$i, $item->sku)
                ->setCellValue('C'.$i, $item->sku2)
                ->setCellValue('D'.$i, $item->category_id)
                ->setCellValue('E'.$i, !empty($item->cat->title)?$item->cat->title:'')
                ->setCellValue('F'.$i, $item->price)
                ->setCellValue('G'.$i, $item->price2)
                ->setCellValue('H'.$i, $item->stock)
                ->setCellValue('I'.$i, $item->new)
                ->setCellValue('J'.$i, $item->sale)
                ->setCellValue('K'.$i, $item->bestseller)
                ->setCellValue('L'.$i, $item->hidden)
                ->setCellValue('M'.$i, $item->sort_order)
                ->setCellValue('N'.$i, $item->id);

            $i++;
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($fname_new);
        header("Content-Disposition: attachment; filename=".$filename);
        header("Content-Length: ".filesize($fname_new));
        header('Content-Type: application/x-force-download; name="'.$filename.'"'); 
        readfile($fname_new);
    }

    public function actionImport()
    {
        if (isset($_POST['importfile'])) {
            $filename = $_POST['importfile'];
            $path = $_SERVER['DOCUMENT_ROOT'].'/web'.$filename;
            $objPHPExcel = \PHPExcel_IOFactory::load($path);
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow();
            $j = 0;
            for ($row = 3; $row <= $highestRow; ++$row) {
                $model = Pr::find()->where(['id' => $objWorksheet->getCellByColumnAndRow(13, $row)->getValue()])->one();

                if ($model === null) {
                    $model = new Pr();
                    $model->link = Translit::t($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                }

                if (empty($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()) || PrCategory::find()->where(['id' => $objWorksheet->getCellByColumnAndRow(3, $row)->getValue()])->exists()) {

                    $model->title = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $model->sku = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $model->sku2 = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $model->category_id = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $model->price = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $model->price2 = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $model->stock = $objWorksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $model->new = $objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $model->sale = $objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $model->bestseller = $objWorksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $model->hidden = $objWorksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $model->sort_order = $objWorksheet->getCellByColumnAndRow(12, $row)->getValue();                

                    $model->save(false);
                    $j++;
                }
            }

            Yii::$app->getSession()->setFlash('success', 'Данные импортированы. Добавлено или обновлено '.$j.' записей.');
        }

        return $this->render('import'); 
    }     

     public function actionRem()
    {
        // $secret_key = (isset($_GET['secret_key']))?$_GET['secret_key']:'';
        // if (md5(Yii::$app->params['secret_key']) == $secret_key) {      

            $pr_model = Pr::find()
                ->where('price > 0')
                ->andWhere(['hidden' => 0])
                ->orderBy('stock DESC, sort_order DESC')
                // ->limit(2)
                ->all();

            

            $filename = 'test1'.time().'.xlsx';
            $fname_new = $_SERVER['DOCUMENT_ROOT'].'/web/files/import/'.$filename;
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);


            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

            $objPHPExcel->getActiveSheet()
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'Item title')
                ->setCellValue('C1', 'Final URL')
                ->setCellValue('D1', 'Image URL')
                ->setCellValue('E1', 'Sale price')
                ->setCellValue('F1', 'Price')
                ->setCellValue('G1', 'Item subtitle')
                ->setCellValue('H1', 'Item description');

            $objPHPExcel->getActiveSheet()->setTitle('Каталог продукции');
            $i = 2;

            // $searchModel = new PrSearch();

            // $pr_model = $searchModel->searchExport(Yii::$app->session->get('params_save'));

            

            foreach ($pr_model as $item) {

                $item->text1 = strip_tags($item->text1, '');
                mb_internal_encoding("UTF-8");
                $item->text1 = mb_substr($item->text1,0,25);
                $item->title = mb_substr( $item->title,0,25);
                $item->title2 = mb_substr( $item->title2,0,25);
                // $item->text1 = substr($item->text1, 0, 25);

                // var_dump($item->text1);

                // $prs = empty($item->price_cur)?$item->price:$item->price_cur;
                $item->text1 = empty($item->text1)?'Покупай без риска!':$item->text1;
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'.$i, $item->id)
                    ->setCellValue('B'.$i, $item->title)
                    ->setCellValue('C'.$i, 'https://dr-house.kz/'.$item->link)
                    ->setCellValue('D'.$i, 'https://admin.dr-house.kz'.Yii::$app->imageCache->imgSrc(Yii::$app->controller->coreSettings['bpath'].$item->image, '300x300', ['fit' => 300]))
                    ->setCellValue('E'.$i, $item->price2)
                    ->setCellValue('F'.$i, $item->price)
                    ->setCellValue('G'.$i, $item->title2)
                    ->setCellValue('H'.$i, $item->text1);

                $i++;
            }


            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($fname_new);
            header("Content-Disposition: attachment; filename=".$filename);
            header("Content-Length: ".filesize($fname_new));
            header('Content-Type: application/x-force-download; name="'.$filename.'"'); 
            readfile($fname_new);


        // }
    } 

}
