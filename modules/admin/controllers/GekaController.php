<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\PrCategoryFront;
use app\modules\admin\models\Pr;
use app\modules\admin\models\Geka;
use app\modules\admin\models\Brands;
use app\modules\admin\models\GekaSearch;
use app\modules\admin\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * GekaController implements the CRUD actions for Geka model.
 */
class GekaController extends Controller
{
	public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['update-seo'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],                
                    [
                        'actions' => ['index', 'create', 'update', 'view'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                        'actions' => ['delete', 'export', 'import'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Geka models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->request->isAjax) {
			$keys = (isset($_POST['keys']))?$_POST['keys']:[];
			if (count($keys) && Yii::$app->user->can('admin')) {
				foreach ($keys as $k => $v) {
					if (($model = Geka::findOne($v)) !== null) {
						$model->delete();
					}
				}
				return $this->redirect(['index']);
			}
		}
		
        $searchModel = new GekaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Geka model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Geka model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Geka();
        $model->sort_order = 0;
        $model->r1 = 1;
        $model->r2 = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Geka model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, true);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Geka model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Geka model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Geka the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	 
    protected function findModel($id, $ml = true)
    {
		if ($ml) {
			$model = Geka::find()->where('id = :id', [':id' => $id])->multilingual()->one();
		} else {
			$model = Geka::findOne($id);
		}
		
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }	

    public function actionExport()
    {
        $filename = 'export-'.date('d-m-Y-His').'.xlsx';
        $fname_new = $_SERVER['DOCUMENT_ROOT'].'/web/files/import/'.$filename;
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);

        $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', '#')
            ->setCellValue('B1', 'Шаблон')
            ->setCellValue('C1', 'title RO')
            ->setCellValue('D1', 'description RO')
            ->setCellValue('E1', 'title RU')
            ->setCellValue('F1', 'description RU')
            ->setCellValue('G1', 'seo text RO')
            ->setCellValue('H1', 'index RO')
            ->setCellValue('I1', 'follow RO')
            ->setCellValue('J1', 'canonical RO')
            ->setCellValue('K1', 'seo text RU')
            ->setCellValue('L1', 'index RU')
            ->setCellValue('M1', 'follow RU')
            ->setCellValue('N1', 'canonical RU');

        $objPHPExcel->getActiveSheet()->setTitle('SEO');
        $i = 3;
        foreach (Geka::find()->multilingual()->all() as $item) {
            $objPHPExcel->getActiveSheet()
                ->setCellValue('A'.$i, $item->id)
                ->setCellValue('B'.$i, $item->tpl)
                ->setCellValue('C'.$i, $item->title)
                ->setCellValue('D'.$i, $item->description)
                ->setCellValue('E'.$i, $item->title_ru)
                ->setCellValue('F'.$i, $item->description_ru)
                ->setCellValue('G'.$i, $item->seo_text)
                ->setCellValue('H'.$i, $item->r1)
                ->setCellValue('I'.$i, $item->r2)
                ->setCellValue('J'.$i, $item->canonical)
                ->setCellValue('K'.$i, $item->seo_text_ru)
                ->setCellValue('L'.$i, $item->r1_ru)
                ->setCellValue('M'.$i, $item->r2_ru)
                ->setCellValue('N'.$i, $item->canonical_ru);

            $i++;
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($fname_new);
        header("Content-Disposition: attachment; filename=".$filename);
        header("Content-Length: ".filesize($fname_new));
        header('Content-Type: application/x-force-download; name="'.$filename.'"'); 
        readfile($fname_new);
    }

    public function actionImport()
    {
        if (isset($_POST['importfile'])) {
            $filename = $_POST['importfile'];
            $path = $_SERVER['DOCUMENT_ROOT'].'/web'.$filename;
            $objPHPExcel = \PHPExcel_IOFactory::load($path);
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow();
            $j = 0;
            for ($row = 3; $row <= $highestRow; ++$row) {
                $lnk = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $model = Geka::find()->multilingual()->where(['tpl' => $lnk])->one();

                if ($model === null) {
                    $model = new Geka();
                    $model->sort_order = 0;
                    $model->tpl = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
                }

                $model->tpl = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
                $model->title = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
                $model->description = (string)$objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
                $model->title_ru = (string)$objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
                $model->description_ru = (string)$objWorksheet->getCellByColumnAndRow(5, $row)->getValue();

                $model->seo_text = (string)$objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
                $model->r1 = $objWorksheet->getCellByColumnAndRow(7, $row)->getValue();
                $model->r2 = $objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
                $model->canonical = (string)$objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
                $model->seo_text_ru = (string)$objWorksheet->getCellByColumnAndRow(10, $row)->getValue();
                $model->r1_ru = $objWorksheet->getCellByColumnAndRow(11, $row)->getValue();
                $model->r2_ru = $objWorksheet->getCellByColumnAndRow(12, $row)->getValue();
                $model->canonical_ru = (string)$objWorksheet->getCellByColumnAndRow(13, $row)->getValue();

                $model->save(false);
                $j++;
            }

            Yii::$app->getSession()->setFlash('success', 'Данные импортированы. Добавлено или обновлено '.$j.' записей.');
        }

        return $this->render('import'); 
    }  

    public function actionUpdateSeo()
    {
        $secret_key = (isset($_GET['secret_key']))?$_GET['secret_key']:'';
        if (!empty($secret_key) && Yii::$app->params['secret_key'] == $secret_key) { 

            


        }
    }        
}
