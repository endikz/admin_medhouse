<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<h1>Панель управления</h1>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form floating-label']]); ?>
    
    <input style="display:none;" type="text" name="LoginForm[email]" />
    <input style="display:none;" type="password" name="LoginForm[password]" />
    
    <?= $form->field($model, 'email', ['template' => '{input}{label}{error}{hint}'])->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'password', ['template' => '{input}{label}{error}{hint}'])->passwordInput(['maxlength' => true]) ?>
     
    <div class="row">
        <div class="col-xs-6 text-left">
            <div class="checkbox checkbox-inline checkbox-styled">
                <label>
                    <input type="checkbox" name="LoginForm[rememberMe]" id="loginform-rememberme" value="1">
                    <span><?= Yii::t('app', 'Запомнить меня') ?></span>
                </label>
            </div>
        </div>
        <br>
        <div class="col-xs-6 text-right">
            <?= Html::submitButton(Yii::t('app', 'Логин'), ['class' => 'btn btn-primary btn-raised', 'name' => 'login-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>