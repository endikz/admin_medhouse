<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= nl2br(Html::encode($message)) ?>
    </p>

    <div class="row">
        <div class="col-xs-6">
            <?= Html::a('Главная', Url::toRoute('/'), ['class' => 'btn btn-primary btn-raised']) ?>
        </div>
    </div>

</div>
