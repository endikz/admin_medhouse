<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AutoCitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auto-city-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'city_name') ?>

    <?= $form->field($model, 'head_tel1') ?>

    <?= $form->field($model, 'head_tel2') ?>

    <?= $form->field($model, 'hot_tel1') ?>

    <?php // echo $form->field($model, 'hot_tel2') ?>

    <?php // echo $form->field($model, 'c_graf') ?>

    <?php // echo $form->field($model, 'c_adres') ?>

    <?php // echo $form->field($model, 'c_mail') ?>

    <?php // echo $form->field($model, 'c_tel1') ?>

    <?php // echo $form->field($model, 'c_tel2') ?>

    <?php // echo $form->field($model, 'c_tel3') ?>

    <?php // echo $form->field($model, 'c_tel4') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
