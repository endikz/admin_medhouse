<?php

return [
    'adminEmail' => 'titov292@gmail.com',
    'supportEmail' => 'info@mydomain.net',
	'user.passwordResetTokenExpire' => 3600,
	'secret_key' => '57f8cdc41973adf5284519fe2baa5074',
	'allowedIps' => [
		'159.224.232.210',
		'185.65.244.13'
	],
	'deliveryType' => [
		0 => 'Курьерская доставка',
		1 => 'Новая Почта',
	],
	'operators' => [
		1 => 'Талипова Сауле',
		2 => 'Сазонова Анастасия',
		3 => 'Жасталапова Анель',
	],
	'payType' => [
		0 => 'Наличными курьеру',
		1 => 'Наложенный платёж',
		2 => 'VISA/MASTERCARD'
	],		
	'adapterConfigs' => [
		'vk' => [
			'auth_type' => 0,
			'client_id' => '',
			'client_secret' => '',
			'redirect_uri' => 'https://dr-house.kz/login-with/?provider=vk',
			'image_path' => '/files/images/vkontakte.png'
		],
		'google' => [
			'auth_type' => 1,
			'client_id' => '',
			'client_secret' => '',
			'redirect_uri' => 'https://dr-house.kz/login-with/?provider=google',
			'image_path' => '/files/images/google_plus.png'
		],
		'facebook' => [
			'auth_type' => 2,
			'client_id' => '',
			'client_secret' => '',
			'redirect_uri' => 'https://dr-house.kz/login-with/?provider=facebook',
			'image_path' => '/files/images/facebook.png'
		],
		'twitter' => [
			'auth_type' => 3,
			'client_id' => '',
			'client_secret' => '',
			'redirect_uri' => 'https://dr-house.kz/login-with/?provider=twitter',
			'image_path' => '/files/images/twitter.png'
		]
	]
];
