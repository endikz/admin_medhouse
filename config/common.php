<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config = [
    'id' => '',
    'name' => '',
    'timeZone' => 'Asia/Almaty',
    'defaultRoute' => 'admin/default/index',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log'
    ],
    'on beforeRequest' => function ($event) {
        if (!Yii::$app->request->isSecureConnection) {}
        $req_url = Yii::$app->request->getAbsoluteUrl();
        $url = str_replace('http://', 'https://', $req_url);
        $url = str_replace('//www.', '//', $url);
        if (substr($url, -1) !== '/' && strpos($url, '?') == false) {
            $url .= '/';
        }
        if (strpos($url, '?') !== false && strpos($url, '/?') == false) {
            $url = str_replace('?', '/?', $url);
        }
        if ($url !== $req_url) {
            Yii::$app->getResponse()->redirect($url, 301);
            Yii::$app->end();
        }
    },
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'wkxTVJpPdmhme2zA_BWWUp15UFdIp_c3',
            'class' => 'app\components\LangRequest',
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'sourceMessageTable'=>'{{%source_message}}',
                    'messageTable'=>'{{%message}}',
                    'sourceLanguage' => 'ru-RU',
                    'enableCaching' => true,
                    'cachingDuration' => 3600
                ],
            ]
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['user/default/login'],
        ],
        'imageCache' => [
            'class' => 'letyii\imagecache\imageCache',
            'cachePath' => '@app/web/files/cache/back',
            'cacheUrl' => '/files/cache/back',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'class'=>'app\components\LangUrlManager',
            'rules' => [
                '' => 'admin/default/index',
                '<_a:(login|logout)>' => 'user/default/<_a>',

                '<_m:(admin)>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
                '<_m:(admin)>/<_c:[\w\-]+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
                '<_m:(admin)>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                '<_m:(admin)>' => '<_m>/default/index',
                '<_m:(admin)>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'user/default/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeFormat' => 'php:H:i:s',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];

return $config;