<?php

namespace app\components;

use app\modules\admin\models\Settings;
use yii\helpers\Url;
use Yii;
use yii\helpers\ArrayHelper;

class Controller extends \yii\web\Controller
{
	public $coreSettings;
	public $title = '';
	public $description = '';	

	public function init() {

		$this->coreSettings = Settings::find()
			->where('id = 1')
			->multilingual()
			->one();
		
		$this->title = 'Панель администрирования';
		$this->description = 'Панель администрирования';	

		parent::init();
	}
}