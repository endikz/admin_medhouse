<?php

use yii\helpers\Html;

$adapters = array();
foreach (Yii::$app->params['adapterConfigs'] as $adapter => $settings) {
	$class_name = ucfirst($adapter);
	$class_path =  str_replace('/', '\\', 'app/components/SocialAuther/Adapter/').$class_name;
	$adapters[$adapter] = new $class_path($settings);	
}
?>

<?php foreach ($adapters as $title => $adapter) { ?>

<?= Html::a('<img src="'.Yii::$app->params['adapterConfigs'][$title]['image_path'].'">', 'javascript:void(0);', ['class' => 'btn btn-floating-action btn-default-light', 'onClick' => 'loginWith("'.$adapter->getAuthUrl().'");']) ?>

<?php } ?>