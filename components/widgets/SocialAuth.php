<?php

namespace app\components\widgets;

class SocialAuth extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        return $this->render('socialAuth', []);
    }
}