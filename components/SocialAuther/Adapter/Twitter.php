<?php

namespace app\components\SocialAuther\Adapter;

class Twitter extends AbstractAdapter
{
	private $oauth_token = '';
	private $oauth_token_secret = '';
	
    public function __construct($config)
    {
        parent::__construct($config);

        $this->socialFieldsMap = array(
            'socialId'   => 'id',
            'email'      => '',
            'name'       => 'name',
            'socialPage' => 'url',
            'sex'        => '',
            'birthday'   => ''
        );

        $this->provider = 'twitter';
		
		define('CONSUMER_KEY', $this->clientId);
		define('CONSUMER_SECRET', $this->clientSecret);
		define('REQUEST_TOKEN_URL', 'https://api.twitter.com/oauth/request_token');
		define('AUTHORIZE_URL', 'https://api.twitter.com/oauth/authorize');
		define('ACCESS_TOKEN_URL', 'https://api.twitter.com/oauth/access_token');
		define('ACCOUNT_DATA_URL', 'https://api.twitter.com/1.1/users/show.json');
		define('CALLBACK_URL', $this->redirectUri);
		define('URL_SEPARATOR', '&');		
    }

    /**
     * Get url of user's avatar or null if it is not set
     *
     * @return string|null
     */
    public function getAvatar()
    {
        $result = null;
        if (isset($this->userInfo['profile_image_url'])) {
           $result = str_replace('_normal', '', $this->userInfo['profile_image_url']);
        }

        return $result;
    }

    /**
     * Authenticate and return bool result of authentication
     *
     * @return bool
     */
    public function authenticate()
    {
        $result = false;

		if (!empty($_GET['oauth_token']) && !empty($_GET['oauth_verifier'])) {
			// готовим подпись для получения токена доступа

			$oauth_nonce = md5(uniqid(rand(), true));
			$oauth_timestamp = time();
			$this->oauth_token = $_GET['oauth_token'];
			$oauth_verifier = $_GET['oauth_verifier'];


			$oauth_base_text = "GET&";
			$oauth_base_text .= urlencode(ACCESS_TOKEN_URL)."&";

			$params = array(
				'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
				'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
				'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
				'oauth_token=' . $this->oauth_token . URL_SEPARATOR,
				'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
				'oauth_verifier=' . $oauth_verifier . URL_SEPARATOR,
				'oauth_version=1.0'
			);	

			$key = CONSUMER_SECRET . URL_SEPARATOR . $this->oauth_token_secret;
			$oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(ACCESS_TOKEN_URL) . URL_SEPARATOR . implode('', array_map('urlencode', $params));
			$oauth_signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));

			// получаем токен доступа
			$params = array(
				'oauth_nonce=' . $oauth_nonce,
				'oauth_signature_method=HMAC-SHA1',
				'oauth_timestamp=' . $oauth_timestamp,
				'oauth_consumer_key=' . CONSUMER_KEY,
				'oauth_token=' . urlencode($this->oauth_token),
				'oauth_verifier=' . urlencode($oauth_verifier),
				'oauth_signature=' . urlencode($oauth_signature),
				'oauth_version=1.0'
			);
			$url = ACCESS_TOKEN_URL . '?' . implode('&', $params);

			//$response = file_get_contents($url);
			$response = $this->http($url, 'GET');
			parse_str($response, $response);		

			// формируем подпись для следующего запроса
			$oauth_nonce = md5(uniqid(rand(), true));
			$oauth_timestamp = time();

			$this->oauth_token = (isset($response['oauth_token']))?$response['oauth_token']:'';
			$this->oauth_token_secret = (isset($response['oauth_token_secret']))?$response['oauth_token_secret']:'';
			$screen_name = (isset($response['screen_name']))?$response['screen_name']:'';

			$params = array(
				'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
				'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
				'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
				'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
				'oauth_token=' . $this->oauth_token . URL_SEPARATOR,
				'oauth_version=1.0' . URL_SEPARATOR,
				'screen_name=' . $screen_name
			);
			$oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(ACCOUNT_DATA_URL) . URL_SEPARATOR . implode('', array_map('urlencode', $params));

			$key = CONSUMER_SECRET . '&' . $this->oauth_token_secret;
			$signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));

			// получаем данные о пользователе
			$params = array(
				'oauth_consumer_key=' . CONSUMER_KEY,
				'oauth_nonce=' . $oauth_nonce,
				'oauth_signature=' . urlencode($signature),
				'oauth_signature_method=HMAC-SHA1',
				'oauth_timestamp=' . $oauth_timestamp,
				'oauth_token=' . urlencode($this->oauth_token),
				'oauth_version=1.0',
				'screen_name=' . $screen_name
			);

			$url = ACCOUNT_DATA_URL . '?' . implode(URL_SEPARATOR, $params);

			//$response = file_get_contents($url);
			$response = $this->http($url, 'GET');
			
			$user_data = json_decode($response, true);	

			$this->userInfo = $user_data;
			
			$result = true;
		}

        return $result;
    }

    /**
     * Prepare params for authentication url
     *
     * @return array
     */
    public function prepareAuthParams()
    {		
		$oauth_nonce = md5(uniqid(rand(), true));
		$oauth_timestamp = time();
		
		$params = array(
			'oauth_callback=' . urlencode(CALLBACK_URL) . URL_SEPARATOR,
			'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
			'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
			'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
			'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
			'oauth_version=1.0'
		);		
		
		$oauth_base_text = implode('', array_map('urlencode', $params));
		$key = CONSUMER_SECRET . URL_SEPARATOR;
		$oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(REQUEST_TOKEN_URL) . URL_SEPARATOR . $oauth_base_text;
		$oauth_signature = base64_encode(hash_hmac('sha1', $oauth_base_text, $key, true));
		
		// получаем токен запроса
		$params = array(
			URL_SEPARATOR . 'oauth_consumer_key=' . CONSUMER_KEY,
			'oauth_nonce=' . $oauth_nonce,
			'oauth_signature=' . urlencode($oauth_signature),
			'oauth_signature_method=HMAC-SHA1',
			'oauth_timestamp=' . $oauth_timestamp,
			'oauth_version=1.0'
		);
		$url = REQUEST_TOKEN_URL . '?oauth_callback=' . urlencode(CALLBACK_URL) . implode('&', $params);

		//$response = file_get_contents($url);
		$response = $this->http($url, 'GET');
		parse_str($response, $response);

		$this->oauth_token = (isset($response['oauth_token']))?$response['oauth_token']:'';
		$this->oauth_token_secret = (isset($response['oauth_token_secret']))?$response['oauth_token_secret']:'';
		
        return array(
            'auth_url'    => AUTHORIZE_URL,
            'auth_params' => array(
                'oauth_token'     => $this->oauth_token,
            )
        );
    }
	
	function http($url, $method, $postfields = NULL, $authorization_header= false) {
		$this->http_info = array();
		$this->header = '';
		$ci = curl_init();

		$headers = Array('Expect:');
		curl_setopt($ci, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ci, CURLOPT_HEADER, FALSE);

		switch ($method) {
		  case 'POST':
			curl_setopt($ci, CURLOPT_POST, TRUE);
			if ($authorization_header)
			  $headers[] = $authorization_header;
			if (!empty($postfields)) {
			  curl_setopt($ci, CURLOPT_POSTFIELDS, $postfields);
			}
			break;
		  case 'DELETE':
			curl_setopt($ci, CURLOPT_CUSTOMREQUEST, 'DELETE');
			if (!empty($postfields)) {
			  $url = "{$url}?{$postfields}";
			}
		}

		curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ci, CURLOPT_URL, $url);
		$response = curl_exec($ci);

		$this->http_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);
		$this->http_info = array_merge($this->http_info, curl_getinfo($ci));
		$this->url = $url;
		$header_size = curl_getinfo($ci, CURLINFO_HEADER_SIZE);
		$this->header = substr($response, 0, $header_size);
		curl_close ($ci);
		return $response;
	}	
}