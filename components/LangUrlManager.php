<?php

namespace app\components;

use yii\web\UrlManager;
use app\modules\admin\models\Lang;

class LangUrlManager extends UrlManager
{
    public function createUrl($params)
    {
        if (isset($params['lang_id'])) {
            $lang = Lang::findOne($params['lang_id']);
            if ($lang === null) {
                $lang = Lang::getDefaultLang();
            }
            unset($params['lang_id']);
        } else {
            $lang = Lang::getCurrent();
			$default_lang = Lang::getDefaultLang();
        }
        
        $url = parent::createUrl($params);
        $url = str_replace('//', '/', $url);
        
		if ($default_lang->url !== $lang->url) {
			if ($url == '/') {
				$url = '/'.$lang->url.'/';
			} else {
				$url = '/'.$lang->url.$url.'/';
			}
		}

        $url = str_replace('//', '/', $url);
        $url = (strpos($url, '?') === false)?$url:rtrim($url, '/');

        return $url;
    }
}