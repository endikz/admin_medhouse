<?php

namespace app\components;

use Yii;
use app\modules\admin\models\Index;
use yii\helpers\ArrayHelper;

class In
{
	private static $index = null;

	public static function t($ind) {
		if (self::$index === null) {
			self::$index = ArrayHelper::map(Index::find()->multilingual()->all(), 'ind', 'text');
		}
			
		return isset(self::$index[$ind])?self::$index[$ind]:$ind;
	}
}