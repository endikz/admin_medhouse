<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/admin_assets';
    public $css = [
        'css/theme-default/bootstrap.css?1422792965',
        'css/theme-default/materialadmin.css?1425466319',
        'css/theme-default/font-awesome.min.css?1422529194',
        'css/theme-default/material-design-iconic-font.min.css?1421434286',
    ];
    public $js = [
        'js/libs/bootstrap/bootstrap.min.js',
        'js/libs/spin.js/spin.min.js',
        'js/libs/autosize/jquery.autosize.min.js',
        'js/libs/nestable/jquery.nestable.js',
        'js/libs/nanoscroller/jquery.nanoscroller.min.js',
        'js/core/source/App.js',
        'js/core/source/AppNavigation.js',
        'js/core/source/AppOffcanvas.js',
        'js/core/source/AppCard.js',
        'js/core/source/AppForm.js',
        'js/core/source/AppNavSearch.js',
        'js/core/source/AppVendor.js',
        'js/core/demo/Demo.js',
        'js/core/demo/DemoUILists.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
